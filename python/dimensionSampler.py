
import math

from scipy.stats import chisquare

from sampling import *


class UniformDimensionOfLengthSampler:

    def __init__(self, dimensionCount):
        self._dimensionCount = dimensionCount

        print ("[UniformDimensionOfLengthSampler]: dimensionCount: %d" %(self._dimensionCount))

    def sample(self):
        return uniformSubsetWithMaxLength(range(0, self._dimensionCount), max([int(math.sqrt(self._dimensionCount)), 2]))


class UniformDimensionSampler:

    def __init__(self, dimensionCount, sampleDimCount):
        self._dimensionCount = dimensionCount
        self._sampleDimCount = sampleDimCount

        print ("[UniformDimensionSampler]: dimensionCount: %d" %(self._dimensionCount))

        self._weights = [1. / self._dimensionCount for i in range(0, self._dimensionCount)]

    def sample(self):
        return weightedSubsetWithLength(range(0, self._dimensionCount), self._weights, self._sampleDimCount)


class Weighted1DimensionSampler:

    def __init__(self, rows, sampleDimCount, binCount=10):
        self._rows = rows
        self._dimensionCount = self._rows.shape[1]
        self._sampleDimCount = sampleDimCount
        self._binCount = binCount

        print ("[Weighted1DimensionSampler]: dimensionCount: %d, sampleDimCount: %d, " %(self._dimensionCount, self._sampleDimCount))

        self._weights = self._computeWeights()

    def _computeWeights(self):
        weights = []

        for j in range(0, self._dimensionCount):
            weights.append(chisquare(numpy.histogram(self._rows[[j]])[0], self._binCount)[0])

        weights = [(1. if weight < 10 else 2.) for weight in weights]

        theSum = sum(weights)

        return [weight / theSum for weight in weights]

    def sample(self):
        return weightedSubsetWithLength(range(0, self._dimensionCount), self._weights, self._sampleDimCount)


class Weighted2DimensionSampler:

    def __init__(self, rows, sampleDimCount, binCount=10):
        self._rows = rows
        self._dimensionCount = self._rows.shape[1]
        self._sampleDimCount = sampleDimCount
        self._binCount = binCount

        print ("[Weighted2DimensionSampler]: dimensionCount: %d, sampleDimCount: %d, " %(self._dimensionCount, self._sampleDimCount))

        self._weights = self._computeWeights()

    def _computeWeights(self):
        weights = []

        avgSize = 1. * self._rows.shape[0] / self._binCount

        for j in range(0, self._dimensionCount):
            hist = numpy.histogram(self._rows[[j]], self._binCount)
            weights.append(sum([1. for v in hist[0] if v <= avgSize]))

        theSum = sum(weights)

        return [weight / theSum for weight in weights]

    def sample(self):
        return weightedSubsetWithLength(range(0, self._dimensionCount), self._weights, self._sampleDimCount)

class Weighted2aDimensionSampler:

    def __init__(self, rows, sampleDimCount, binCount=10):
        self._rows = rows
        self._dimensionCount = self._rows.shape[1]
        self._sampleDimCount = sampleDimCount
        self._binCount = binCount

        print ("[Weighted2aDimensionSampler]: dimensionCount: %d, sampleDimCount: %d, " % (
        self._dimensionCount, self._sampleDimCount))

        self._weights = self._computeWeights()

    def _computeWeights(self):
        weights = []

        avgSize = 1. * self._rows.shape[0] / self._binCount

        for j in range(0, self._dimensionCount):
            hist = numpy.histogram(self._rows[[j]], self._binCount)
            weights.append(math.sqrt(sum([1. for v in hist[0] if v <= avgSize])))

        theSum = sum(weights)

        return [weight / theSum for weight in weights]

    def sample(self):
        return weightedSubsetWithLength(range(0, self._dimensionCount), self._weights, self._sampleDimCount)


class WeightedFDDimensionSampler:

    def __init__(self, rows, sampleDimCount):
        self._rows = rows
        self._dimensionCount = self._rows.shape[1]
        self._sampleDimCount = sampleDimCount

        print ("[WeightedFDDimensionSampler]: dimensionCount: %d, sampleDimCount: %d, " % (
        self._dimensionCount, self._sampleDimCount))

        self._weights = self._computeWeights()

    def _computeWeights(self):
        weights = []


        for j in range(0, self._dimensionCount):
            hist = numpy.histogram(self._rows[[j]], bins="fd")
            avgSize = 1. * self._rows.shape[0] / len(hist[0])
            weights.append(math.sqrt(sum([1. for v in hist[0] if v <= avgSize]) / len(hist[0])))

        theSum = sum(weights)

        return [weight / theSum for weight in weights]

    def sample(self):
        return weightedSubsetWithLength(range(0, self._dimensionCount), self._weights, self._sampleDimCount)

def dimensionSampler(rows, dimensionSampler="uniform|2"):
    sp = dimensionSampler.split("|")
    strategy = sp[0]

    if len(sp) > 1:
        sampleDimCount = int(sp[1])

    if strategy == "variable":
        return UniformDimensionOfLengthSampler(rows.shape[1])
    elif strategy == "uniform":
        return UniformDimensionSampler(rows.shape[1], sampleDimCount)
    elif strategy == "weighted1":
        return Weighted1DimensionSampler(rows, sampleDimCount)
    elif strategy == "weighted2":
        return Weighted2DimensionSampler(rows, sampleDimCount)
    elif strategy == "weighted2a":
        return Weighted2aDimensionSampler(rows, sampleDimCount)
    elif strategy == "weightedFD":
        return WeightedFDDimensionSampler(rows, sampleDimCount)

    return UniformDimensionSampler(rows.shape[1], 2)
