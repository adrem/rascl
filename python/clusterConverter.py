
import numpy as np

from clustering import *
from dimensionStrategy import *

def normalizeRows(rows):
    nrows = []

    minMax = [[float("inf"), float("-inf")] for i in range(0, len(rows[0]))]

    for row in rows:
        for i in range(0, len(row)):
            minMax[i][0] = min(minMax[i][0], row[i])
            minMax[i][1] = max(minMax[i][1], row[i])

    for row in rows:
        nrows.append([(1. * v - minMax[i][0]) / (minMax[i][1] - minMax[i][0]) for i, v in enumerate(row)])

    return nrows

class ClusterConverter:

    def __init__(self, rows, parameters):
        self._rows = rows
        self._prune = parameters["prune"].startswith("True") if "prune" in parameters else True
        self._beta = float(parameters["prune"].split("_")[1]) if "prune" in parameters and self._prune else 1.5
        self._recomputeDimensions = parameters["recomputeDimensions"] == "True" if "recomputeDimensions" in parameters else False
        self._dimensionsAlg = parameters["dimensionsAlg"] if "dimensionsAlg" in parameters else ALG_STDEV2_0_03

        print ("[ClusterConverter]: Prune: %s, recomputeDimensions: %s, dimensionsAlg: %s" %(str(self._prune), str(self._recomputeDimensions), self._dimensionsAlg))

        if "stdev_based2" in self._dimensionsAlg:
            self._theRows = normalizeRows(rows)
        else:
            self._theRows = self._rows

    def _pruneFimm(self, fimm, dims, dps):
        dimsDps = [[dp[dim] for dim in dims]for dp in dps]

        stdev = np.std(dimsDps, 0)
        avg = np.mean(dimsDps, 0)

        newFimm = []
        newDps = []

        for i, f in enumerate(fimm):
            add = True

            for j in range(0, len(dims)):
                add &= (np.abs(avg[j] - dps[i][dims[j]]) <= stdev[j] * self._beta)

            if add:
                newFimm.append(f)
                newDps.append(dps[i])

        return newFimm, newDps

    def convert(self, fimm, dims):
        # dps = [self._theRows[int(f)] for f in fimm]
        dps = self._theRows.iloc[fimm,:]

        ddims = infer_dimensions(self._dimensionsAlg, fimm, dims, dps)

        if self._prune:
            newFimm, newDps = self._pruneFimm(fimm, ddims, dps)

            if self._recomputeDimensions:
                return SubspaceCluster(set(newFimm), set(infer_dimensions(self._dimensionsAlg, newFimm, dims, newDps)))

            return SubspaceCluster(set(newFimm), set(ddims))

        return SubspaceCluster(set(fimm), set(ddims))