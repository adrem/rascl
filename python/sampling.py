import math, numpy, random

def uniformSubsetWithMaxLength(elements, maxLength):
    nFact = math.factorial(len(elements))
    srange = [(i, nFact / (math.factorial(i) * math.factorial(len(elements) - i))) for i in range(1, maxLength + 1)]

    srsum = sum([v[1] for v in srange])

    value = random.randint(1, srsum)

    ix = 0
    curr = 0

    for j in srange:
        curr += j[1]

        if value <= curr:
            break

        ix += 1

    subsetSize = srange[ix][0]

    subset = set()
    while len(subset) < subsetSize:
        subset.add(elements[random.randint(0, len(elements) - 1)])

    return sorted([d for d in subset])

def weightedSubsetWithLength(elements, weights, length):
    return sorted(numpy.random.choice(elements, p=weights, size=length))
