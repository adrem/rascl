#!/usr/bin/env python

"""
    Wrapper class for running CartiClus.

    Original implementation of CartiClus is provided by E. Aksehirli and is written in Java. This module provides a
    simple interface to run the algorithm and read the output as a SubspaceCluster.
"""

import os, sys, tempfile

from subprocess import call

from clustering import print_clusters, SubspaceCluster

JAR_FILE = "bin/carticlus.jar"

K = 10
MIN_SUP = 10

def parse_cluster(line):
    dims = [i+1 for i, d in enumerate(line.split(" [")[0].split(" ")) if d == "1"]
    objects = [int(v) for v in line.split("] ")[1].split(" ")]

    return SubspaceCluster(set(objects), set(dims))

def carti_clus(file, num_dim, k=K, min_sup=MIN_SUP, min_size=None):
    cartLogFile = "/tmp/cartLog"
    outputFile = tempfile.NamedTemporaryFile().name

    if min_size == None:
        call(["java", "-jar", JAR_FILE, file, "%d" %(k), "%d" %(min_sup), "%d" %(num_dim), cartLogFile, outputFile])
    else:
        call(["java", "-jar", JAR_FILE, file, "%d" % (k), "%d" % (min_sup), "%d" % (num_dim), "%d" %(min_size), cartLogFile, outputFile])

    with open(outputFile) as r:
        clusters = [parse_cluster(line.strip()) for line in r]

    os.remove(outputFile)

    return clusters

def carti_clus_parameters(file, parameters={}):
    k = parameters["k"] if "k" in parameters else K
    min_sup = parameters["min_sup"] if "min_sup" in parameters else MIN_SUP
    num_dim = parameters["num_dim"] if "num_dim" in parameters else 2
    min_size = parameters["min_size"] if "min_size" in parameters else k / 2

    return carti_clus(file, num_dim, k, min_sup, min_size)

# CL #########################################################

def _usage():
    print("""
COMMAND
    $ python carticlus.py <-f file> (-k k) (-s min_sup) (-d num_dim) (-m min_size)

OPTIONS
    - k: number of clusters to find (default: %d)
    
    - min_sup: minimum support value used for maximal frequent itemset mining (default: %d)
    
    - num_dim: number of dimensions (default: 2)
    
    - min_size: minimum size of a cluster (default: k/2)
""" %(K, MIN_SUP), file=sys.stderr)


if __name__ == "__main__":
    import getopt

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:k:s:d:m:", ['help'])
    except getopt.GetoptError:
        _usage()
        sys.exit(2)

    file = None
    k = K
    min_sup = MIN_SUP
    num_dim = 2
    min_size = None

    for o, a in opts:
        if o in ('-h', '--help'):
            _usage()
            sys.exit()
        elif o == '-f':
            file = a
        elif o == '-k':
            k = int(a)
        elif o == '-s':
            min_sup = int(a)
        elif o == '-d':
            num_dim = int(a)
        elif o == '-m':
            min_size = int(a)

    if file == None:
        print("""Please specify an input file using -f""", file=sys.stderr)
        _usage()
        sys.exit(2)

    scs = carti_clus(file, k, min_sup, num_dim, min_size)

    print_clusters(scs)
