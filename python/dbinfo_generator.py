
import numpy, os

from clustering import SubspaceCluster
from dataUtils import getTransactions

def get_cluster(row):
    sp = row.rstrip("\n").split("|")
    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v)-1 for v in sp[1].split(" ")]))

def get_info(data_name):
    data = getTransactions(os.path.join("data", data_name, "%s.raw.txt" %(data_name)))
    clus = [get_cluster(row) for row in open(os.path.join("data", data_name, "%s.clusters.txt" %(data_name)))]

    row_count = len(data)
    col_count = len(data[0])
    clu_count = len(clus)
    avg_clu_obj_size = numpy.average([len(clu.objects) for clu in clus])
    avg_clu_dim_size = numpy.average([len(clu.dimensions) for clu in clus])

    print("%s\t%d\t%d\t%d\t%.5f\t%.5f" %(data_name, row_count, col_count, clu_count, avg_clu_obj_size, avg_clu_dim_size))

if __name__ == '__main__':
    for data_name in [
        "dbsizescale_s1500", "dbsizescale_s2500", "dbsizescale_s3500", "dbsizescale_s4500", "dbsizescale_s5500",
        "dimscale_d05", "dimscale_d10", "dimscale_d15", "dimscale_d20", "dimscale_d25", "dimscale_d50", "dimscale_d75",
        "noisescale_n10", "noisescale_n30", "noisescale_n50", "noisescale_n70"]:
        get_info(data_name)
