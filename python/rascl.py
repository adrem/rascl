#!/usr/bin/env python

"""
    Implements Randomised Subspace Clustering algorithm by S.Moens et al.
"""

import os, pickle, shutil, sys, time

import pandas as pd

from functools import reduce
from matplotlib import pyplot as plt
from sklearn import cluster

from clustering import print_clusters, SubspaceCluster
from dimensionStrategy import infer_dimensions
from patternSortingStrategy import sort_patterns
from rascl_utils import draw_centers, draw_labeled, draw_fis, draw_samples, expand_fimm_using_drop, generate_new_samples, get_fis, top_k_clusters, transactions_to_items

OUTPUT_DIR = os.path.join("output", "rascl")


'''
Cleans up the temporary files and creates a new tmp directory
'''
def cleanup_files(data_key):
    if os.path.isdir("tmp"):
        shutil.rmtree("tmp")

    outputDir = os.path.join(OUTPUT_DIR, data_key)

    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)


'''
Generates n samples.

A sample contains a set of k data points and a list of dimensions.
The number of dimensions is between 1 and sqrt(dimensions).

This is a wrapper function that can be used to cache the generation of samples.
'''
def generate_samples(data_key, rows, n, k, sample_dim_count, run=1, use_cache=True):
    print ("Getting samples [n=%d, k=%d, sampleDimCount=%s]" % (n, k, sample_dim_count if type(sample_dim_count) == str or type(sample_dim_count) == unicode else "%d" % (sample_dim_count)))

    if use_cache:
        output_dir = "%s/%s" %(OUTPUT_DIR, data_key)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        pickle_file = "%s/%s/run%d_samples_n%d_k%d_sampleDimCount%s.pickle" % (OUTPUT_DIR, data_key, run, n, k, sample_dim_count)

        if os.path.isfile(pickle_file):
            print ("Pickle file exists, reading samples from pickle")
            samples = pickle.load(open(pickle_file))
        else:
            samples = generate_new_samples(rows, n, k, sample_dim_count)

            pickle.dump(samples, open(pickle_file, "w"), pickle.HIGHEST_PROTOCOL)
    else:
        samples = generate_new_samples(rows, n, k, sample_dim_count)

    return samples


'''
Generates transactions from the samples, by assigning original datapoints to the closest
data point in the sample given the dimensions, producing n*k transactions. Where n is the
number of samples and k is the number of data points per sample
'''
def generate_new_transactions(samples, rows, kk, draw_new_centers=False):
    start = time.time()

    transactions = []

    for j, sample in enumerate(samples):
        reduced_sample_points = rows.iloc[sample[0], sample[1]].values

        kmeans = cluster.KMeans(n_clusters=kk, init="k-means++", n_init=1).fit(reduced_sample_points)

        reduced_data_points = rows.iloc[:, sample[1]].values

        cluster_assignments = kmeans.predict(reduced_data_points)

        ts = [[] for i in range(0, kk)]

        for i, region in enumerate(cluster_assignments):
            ts[region].append(i)

        transactions.extend(ts)

        if draw_new_centers and j < 5:
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,5))

            draw_centers(rows, sample[0], kmeans.cluster_centers_, sample[1], ax1)
            draw_labeled(reduced_data_points, cluster_assignments, sample[1], ax2)

            plt.show()

    print("Time for generating transactions: %d" %(time.time() - start))

    return transactions


'''
Generates transactions from the samples, by assigning original datapoints to the closest
data point in the sample given the dimensions, producing n*k transactions. Where n is the
number of samples and k is the number of data points per sample.

        else:
            samples = generate_new_samples(rows, n, k, sample_dim_count)
'''
def generate_transactions(data_key, rows, samples, n, k, sample_dim_count, kk, run=1, draw_new_centers=True, use_cache=True):
    print ("Generating transactions [kk=%d]" %(kk))

    if use_cache:
        txt_file = "%s/%s/run%d_transactions_n%d_k%d_kk%d_sampleDimCount%s.txt" % (OUTPUT_DIR, data_key, run, n, k, kk, sample_dim_count)

        if os.path.isfile(txt_file):
            print ("Cache file exists, reading transactions from cache")

            with open(txt_file) as r:
                transactions = [[int(v) for v in row.strip().split(" ")] for row in r if row != "\n"]
        else:
            transactions = generate_new_transactions(samples, rows, kk, draw_new_centers=draw_new_centers)

            with open(txt_file, "w") as w:
                for transaction in transactions:
                    w.write(" ".join(["%d" % (i) for i in transaction]))
                    w.write("\n")
    else:
        transactions = generate_new_transactions(samples, rows, kk, draw_new_centers=draw_new_centers)

    return transactions


def get_fis_and_dims(samples, items, fis, kk, expand):
    fis2 = []
    dims = []

    for fi in fis:
        fis2.append(expand_fimm_using_drop(items, fi, float(expand.split("|")[1])) if expand.startswith("drop") else fi)

        intersect = reduce(set.intersection, map(set, [items[item] for item in fi]))
        dims.append([samples[tid / kk][1] for tid in intersect])

    return fis2, dims


def sc_parameters(file_name, parameters={}, run=1, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=True):
    data_key = file_name.split("/")[-1].split(".")[0]

    cleanup_files(data_key)

    n = parameters["n"] if "n" in parameters else 1000
    k = parameters["k"] if "k" in parameters else 100
    kk = parameters["kk"] if "kk" in parameters else 10
    s = parameters["s"] if "s" in parameters else "rmis|100"
    m = parameters["m"] if "m" in parameters else 100
    expand = parameters["expand"] if "expand" in parameters else "none"
    sample_dim_count = parameters["sampleDimCount"] if "sampleDimCount" in parameters else "weightedFD|2"
    print (parameters)

    return sc_arguments(file_name, n=n, k=k, sample_dim_count=sample_dim_count, kk=kk, s=s, m=m, expand=expand, run=run, draw=draw, draw_new_samples=draw_new_samples, draw_new_centers=draw_new_centers, use_cache=use_cache)


def sc_arguments(file_name, n, k, sample_dim_count, kk, s, m, expand="none", run=1, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=True):
    rows = pd.read_csv(file_name, delimiter=",", header=None, index_col=False)

    return sc(file_name, rows=rows, n=n, k=k, sample_dim_count=sample_dim_count, kk=kk, s=s, m=m, expand=expand, run=run, draw=draw, draw_new_samples=draw_new_samples, draw_new_centers=draw_new_centers, use_cache=use_cache)


def sc_arguments_top_k(file_name, n, k, sample_dim_count, kk, s, m, expand="none", top_k=10, run=1, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=True):
    rows = pd.read_csv(file_name, delimiter=",", header=None, index_col=False)

    samples, transactions, fis, dims = sc(file_name, rows=rows, n=n, k=k, sample_dim_count=sample_dim_count, kk=kk, s=s, m=m, expand=expand, run=run, draw=draw, draw_new_samples=draw_new_samples, draw_new_centers=draw_new_centers, use_cache=use_cache)

    fisPlusDims = [(fi, dims[i]) for i, fi in enumerate(fis)]

    fisPlusDims = sort_patterns("object_count", fisPlusDims)

    scs = [SubspaceCluster(set(fiPlusDim[0]), set(infer_dimensions("alpha|0", fiPlusDim[0], fiPlusDim[1], rows))) for fiPlusDim in fisPlusDims]

    return top_k_clusters(scs, top_k, 0.25)


def sc(file_name, rows, n, k, sample_dim_count, kk, s, m, expand="none",  run=1, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=True):
    print ("rC: %d, dC: %d, N: %d, sampleDimCount: %s, K: %d, s: %s, m: %d, expand: %s, run: %d" % (rows.shape[0], rows.shape[1], n, sample_dim_count, k, str(s), m, expand, run))

    data_key = file_name.split("/")[-1].split(".")[0]

    samples = generate_samples(data_key, rows, n=n, k=k, sample_dim_count=sample_dim_count, run=run, use_cache=use_cache)

    if draw_new_samples:
        draw_samples(rows, samples)

    transactions = generate_transactions(data_key, rows, samples, n=n, k=k, kk=kk, sample_dim_count=sample_dim_count, run=run, draw_new_centers=draw_new_centers, use_cache=use_cache)

    items = transactions_to_items(transactions)

    fis = get_fis(transactions, s=s, m=m)

    fis, dims = get_fis_and_dims(samples, items, fis, kk, expand)

    if draw:
        draw_fis(rows, fis)

    return samples, transactions, fis, dims


# CL #########################################################

def _usage():
    print("""
COMMAND
    $ python rascl.py <-f file> (-n n) (-k k) (-d sample_dim_count) (-K K) (-s s) (-m m) (-e expand) (-r topk) (-g draw) (-c use_cache)

OPTIONS
    - n: number of database samples to use (default: 1000)

    - k: number of data points per sample (default: 100)

    - sample_dim_count: dimension sampling strategy. Can be either 'uniform|X' or 'weightedFD|X', where
        X is a positive integer smaller than or equal to the number of dimensions in the data
        (default: weightedFD|2) 

    - K: number of clusters for K-Means clustering (default: 10)
    
    - s: algorithm to use for sampling itemsets. Can be either "rmis|X" where X is a positive
        integer and is the minimal support, or can be "rts" (default: rmis|100)
        
    - m: number of samples itemsets to sample (default: 100)
    
    - expand: strategy to use to expand sampled itemsets. Can be either "none" or "drop|X"
        where X is a real between 0 and 1
        
    - topk: number of clusters to return (default: 10)
        
    - g: if set, the first 5 samples, centers and itemsets are drawn
    
    - c: if set, caching is enabled. Running with the same parameters will result in cached data to be used
""", file=sys.stderr)


if __name__ == '__main__':
    import getopt

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:n:k:d:K:s:m:e:r:gc", ['help'])
    except getopt.GetoptError:
        _usage()
        sys.exit(2)

    file_name = None
    n = 1000
    k = 100
    sample_dim_count = "weightedFD|2"
    kk = 10
    s = "rmis|100"
    m = 100
    expand = "drop|0.05"
    top_k = 10
    draw = False
    use_cache = False

    for o, a in opts:
        if o in ('-h', '--help'):
            _usage()
            sys.exit()
        elif o == '-f':
            file_name = a
        elif o == '-n':
            n = int(a)
        elif o == '-k':
            k = int(a)
        elif o == '-d':
            sample_dim_count = a
        elif o == '-K':
            kk = int(a)
        elif o == '-s':
            s = a
        elif o == '-m':
            m = int(a)
        elif o == '-e':
            expand = a
        elif o == '-r':
            top_k = int(a)
        elif o == '-g':
            draw = True
        elif o == '-c':
            use_cache = True

    if file_name is None:
        print("""Please specify an input file using -f""", file=sys.stderr)
        _usage()
        sys.exit(2)

    scs = sc_arguments_top_k(file_name, n=n, k=k, sample_dim_count=sample_dim_count, kk=kk, s=s, m=m, expand=expand, top_k=top_k, run=1, draw_new_samples=draw, draw_new_centers=draw, draw=draw, use_cache=use_cache)

    print_clusters(scs)
