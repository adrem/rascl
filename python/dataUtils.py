
def getTransactions(fileName, delimiter=","):
    return [[float(v) for v in row.rstrip().split(delimiter) if v != ""] for row in open(fileName)]
