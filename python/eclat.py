
import os, tempfile

from subprocess import call

ECLAT_EXE = "bin/eclat"

TYPE_FREQUENT = "FREQUENT"
TYPE_CLOSED = "CLOSED"
TYPE_MAXIMAL = "MAXIMAL"



def getTypeParameter(type):
    if type == TYPE_FREQUENT:
        return "s"
    elif type == TYPE_CLOSED:
        return "c"
    elif type == TYPE_MAXIMAL:
        return "m"
    return "s"

'''
Finds itemsets with given parameters
'''
def getFimms(transactions, s, t=TYPE_FREQUENT):
    print("Mining FIMM [s=%d, t=%s]" %(s, t))

    transactionsFile = tempfile.NamedTemporaryFile().name

    with open(transactionsFile, "w") as f:
        for transaction in transactions:
            if type(transaction) is list:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction])))
            else:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction[0]])))

    fimmFile = tempfile.NamedTemporaryFile().name

    call([ECLAT_EXE, "-t%s" %(getTypeParameter(t)), "-s-%d" %(s), transactionsFile, fimmFile])

    fimms = [[int(item) for item in line.split(" (")[0].split(" ")] for line in open(fimmFile) if line.rstrip() != ""]

    os.remove(transactionsFile)
    os.remove(fimmFile)

    return fimms
