
import numpy as np

from collections import Counter

INDICATOR_OBJECT_COUNT = "object_count"
INDICATOR_DIMENSION_COUNT = "dimension_count"
INDICATOR_DISTINCT_DIMENSION_COUNT = "distinct_dimension_count"
INDICATOR_DISTINCT_DIMENSION_COUNT_DEVIATION = "distinct_dimension_count_deviation"
INDICATOR_RANKING = "ranking"
INDICATOR_RANKING2 = "ranking2"

def _sorter(d):
    if d == INDICATOR_OBJECT_COUNT:
        return indicator_object_count
    elif d == INDICATOR_DIMENSION_COUNT:
        return indicator_dimension_count
    elif d == INDICATOR_DISTINCT_DIMENSION_COUNT:
        return indicator_distinct_dimension_count
    elif d == INDICATOR_DISTINCT_DIMENSION_COUNT_DEVIATION:
        return indicator_distinct_dimension_count_deviation
    elif d == INDICATOR_RANKING:
        return indicator_ranking
    elif d == INDICATOR_RANKING2:
        return indicator_ranking2
    return indicator_object_count

cnt = Counter()

def sort_patterns(d, fimmsPlusDims):
    sorter = _sorter(d)

    if d == INDICATOR_RANKING2:
        for fimmPlusDims in fimmsPlusDims:
            for v in fimmPlusDims[0]:
                cnt[v] += 1

    return sorted(fimmsPlusDims, key=lambda tuple: sorter(tuple[0], tuple[1]), reverse=True)

def indicator_object_count(fimm, dims):
    return len(fimm)

def indicator_dimension_count(fimm, dims):
    return len([item for sublist in dims for item in sublist])

def indicator_distinct_dimension_count(fimm, dims):
    return len(set(["_".join(["%d" %(v) for v in sorted(d)]) for d in dims]))

def indicator_distinct_dimension_count_deviation(fimm, dims):
    m = {}

    for d in ["_".join(["%d" %(v) for v in sorted(d)]) for d in dims]:
        if d not in m: m[d] = 0
        m[d] += 1

    s = np.std(m.values())

    return s

def indicator_ranking(fimm, dims):
    fRow = list(filter(lambda x: cnt[x] > 10, fimm))

    return len(fRow)

def indicator_ranking2(fimm, dims):
    fRow = list(filter(lambda x: cnt[x] > 10, fimm))

    return sum([cnt[v] for v in fRow])