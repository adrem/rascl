
import json, math, os.path, pickle, random, sys

import numpy as np

from collections import Counter
from matplotlib import pyplot as plt

import dimensionStrategy
from clustering import *
from dataStore import *
from dataUtils import *
from dimensionStrategy import *
from measures import *

KEY_FIMMS = "fimms"

VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

MEASURES = [
    ("dimension_f1", dimension_f1)
]

INDICATORS = [
    ALG_VOTING,
    ALG_ALPHA_25PCT,
    ALG_ALPHA_40PCT,
    ALG_ALPHA_50PCT,
    ALG_STDEV_0_07,
    ALG_STDEV2_0_04,
    ALG_DENSITY
]

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")]))

def exp(sc, experiment, parameters, topk, indicator, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped/dimensions_quality" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    measures = {i: {} for i, cluster in enumerate(clusters)}
    for i, cluster in enumerate(clusters):
        for measure in MEASURES:
            measures[i][measure[0]] = []

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    nrows = []

    minMax = [[1000000, -1000000] for i in range(0, len(rows[0]))]

    for row in rows:
        for i in range(0, len(row)):
            minMax[i][0] = min(minMax[i][0], row[i])
            minMax[i][1] = max(minMax[i][1], row[i])

    for row in rows:
        nrows.append([(1. * v - minMax[i][0]) / (minMax[i][1] - minMax[i][0]) for i, v in enumerate(row)])

    for measure in MEASURES:

        evaluatedClusters = []

        for ix, fimm in enumerate(fimms):
            if len(fimm) < 20:
                continue

            dps = [rows[int(f)] for f in fimm]
            ndps = [nrows[int(f)] for f in fimm]

            fimm = SubspaceCluster(set(fimm), set(infer_dimensions(indicator, fimm, dims[ix], ndps)))

            values = [(i, precision(fimm, cluster)) for i, cluster in enumerate(clusters)]

            values = sorted(values, key=lambda tuple: tuple[1], reverse=True)

            closest = values[0]

            evaluatedClusters.append((fimm, closest, precision(fimm, clusters[closest[0]] )))

        evaluatedClusters = sorted(evaluatedClusters, key=lambda tuple: tuple[1][1], reverse=True)

        for evaluatedCluster in evaluatedClusters:
            measures[evaluatedCluster[1][0]][measure[0]].append(measure[1](evaluatedCluster[0], clusters[evaluatedCluster[1][0]]))

        for i, cluster in enumerate(clusters):
            for measure in MEASURES:
                measures[i][measure[0]] = measures[i][measure[0]][0:min(len(measures[i][measure[0]]), topk)]

    return measures

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key, topk, indicator):
    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped/dimensions_quality" % (expDir)

    plotFile = "%s/test_%s_diminference_%s_%s.pdf" %(figGroupedDir, infoArr[0]["test"], indicator, key)

    for ic in range(0, len(infoArr[0]["data"])):
        x = [i["value"] for i in infoArr]

        y = []
        for ix, i in enumerate(x):
            y.append(infoArr[ix]["data"][ic][key])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ic * 0.3, widths=0.25)
        p = plt.plot([], label="%d" %(ic))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()

def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments, topk, indicator):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2_ckdtree as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            infoArr.append({"test": rangedParam["name"], "value": v, "data": exp(sc, experiment, parameters, topk, indicator)})

        for measure in MEASURES:
            drawInfoGroupedArr(experiment, infoArr, measure[0], topk, indicator)

# Main

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    for k in [50]:
        for indicator in INDICATORS:
            experiments(json.load(open(sys.argv[1])), k, indicator)
