
import json, math, os.path, pickle, random, sys

import numpy as np

from collections import Counter
from matplotlib import pyplot as plt

from clustering import *
from dataStore import *
from dataUtils import *
from measures import *

KEY_FIMMS = "fimms"

VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

MEASURES = [
    ("dimension_f1", dimension_f1)
]

def voting(dims):
    if len(dims) == 0:
        return []

    counts = Counter(["_".join("%d" % (d) for d in sorted(dd)) for dd in dims])

    maxValue = counts.most_common()[0][1]

    maxDims = [[int(d) for d in mc[0].split("_")] for mc in counts.most_common() if mc[1] == maxValue]

    if len(maxDims) == 1:
        return maxDims[0]

    return maxDims[random.randint(0, len(maxDims) - 1)]

def alpha_occurrence(dims, alpha=0.5):
    if len(dims) == 0:
        return []

    counts = {}

    for dim in dims:
        for d in dim:
            if d not in counts: counts[d] = 0
            counts[d] += 1

    minValue = len(dims) * alpha

    return [d for d in counts.keys() if counts[d] >= minValue]

def alpha_occurrence_25pct(dims):
    return alpha_occurrence(dims, alpha=0.25)

def alpha_occurrence_40pct(dims):
    return alpha_occurrence(dims, alpha=0.40)

def alpha_occurrence_50pct(dims):
    return alpha_occurrence(dims, alpha=0.5)

def alpha_occurrence_75pct(dims):
    return alpha_occurrence(dims, alpha=0.75)

def dimension_expansion(dims):

    return 0

INDICATORS = [
    ("voting", voting),
    ("alpha_occurrence_25pct", alpha_occurrence_25pct),
    ("alpha_occurrence_40pct", alpha_occurrence_40pct),
    ("alpha_occurrence_50pct", alpha_occurrence_50pct),
    ("alpha_occurrence_75pct", alpha_occurrence_75pct),
    # ("dimension_expansion", dimension_expansion)
]

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")]))

def smallOverlap(newCluster, existingClusters, maxOverlap=0.2):
    for existingCluster in existingClusters:
        if precision(newCluster[0], existingCluster[0]) > maxOverlap:
            return False

    return True

def exp(sc, experiment, parameters, topk, indicator, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped/dimensions_top_quality" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    measures = {i: {} for i, cluster in enumerate(clusters)}
    for i, cluster in enumerate(clusters):
        for measure in MEASURES:
            measures[i][measure[0]] = []

    for measure in MEASURES:

        evaluatedClusters = []

        for ix, fimm in enumerate(fimms):
            fimm = SubspaceCluster(set(fimm), set(indicator[1](dims[ix])))

            values = [(i, precision(fimm, cluster)) for i, cluster in enumerate(clusters)]

            values = sorted(values, key=lambda tuple: tuple[1], reverse=True)

            closest = values[0]

            evaluatedClusters.append((fimm, closest, precision(fimm, clusters[closest[0]] )))

        evaluatedClusters = sorted(evaluatedClusters, key=lambda tuple: (tuple[1][1], len(tuple[0].objects)), reverse=True)

        smallOverlapClusters = []

        for evaluatedCluster in evaluatedClusters:
            if smallOverlap(evaluatedCluster, smallOverlapClusters):
                smallOverlapClusters.append(evaluatedCluster)

            if len(smallOverlapClusters) == topk:
                break

        for evaluatedCluster in evaluatedClusters:
            measures[evaluatedCluster[1][0]][measure[0]].append(measure[1](evaluatedCluster[0], clusters[evaluatedCluster[1][0]]))

        # for i, cluster in enumerate(clusters):
        #     for measure in MEASURES:
        #         measures[i][measure[0]] = measures[i][measure[0]][0:min(len(measures[i][measure[0]]), topk)]

    return measures

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key, topk, indicator):
    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped/dimensions_top_quality" % (expDir)

    plotFile = "%s/test_%s_topk%d_mechanism_%s_%s_small_overlap.pdf" %(figGroupedDir, infoArr[0]["test"], topk, indicator[0], key)

    for ic in range(0, len(infoArr[0]["data"])):
        x = [i["value"] for i in infoArr]

        y = []
        for ix, i in enumerate(x):
            y.append(infoArr[ix]["data"][ic][key])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ic * 0.3, widths=0.25)
        p = plt.plot([], label="%d" %(ic))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()

def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments, topk, indicator):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2 as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            infoArr.append({"test": rangedParam["name"], "value": v, "data": exp(sc, experiment, parameters, topk, indicator)})

        for measure in MEASURES:
            drawInfoGroupedArr(experiment, infoArr, measure[0], topk, indicator)

# Tests

def votingTest():
    if voting([]) != []:
        print("[voting] Empty list test failed!!")
        exit()

    if voting([[0, 1]]) != [0,1]:
        print("[voting] Single list test failed")
        exit()

    if voting([[0,1], [0,1], [0], [2,3], [2], [3], [4]]) != [0,1]:
        print("[voting] Multi list test failed")
        exit()

    for i in range(0, 50):
        maxDim = voting([[0,1], [0,1], [0], [2,3], [2,3], [2], [3], [4]])

        if maxDim != [2,3] and maxDim != [0,1]:
            print("[voting] Multi list same occurrence test failed")
            exit()

def alphaOccurrenceTest():
    if alpha_occurrence_25pct([]) != []:
        print("[alpha_occurrence_25] Empty list test failed!!")
        exit()

    if alpha_occurrence_25pct([[0,1]]) != [0,1]:
        print("[alpha_occurrence_25] Single list test failed")
        exit()

    if alpha_occurrence_25pct([[0,1]]) != [0,1]:
        print("[alpha_occurrence_25pct] Single list test failed")
        exit()

    if alpha_occurrence_25pct([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]]) != [0,1]:
        print("[alpha_occurrence_25pct] Multi list1 test failed")
        exit()

    if alpha_occurrence_25pct([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]]) != [0,1]:
        print("[alpha_occurrence_25pct] Multi list2 test failed")
        exit()

    if alpha_occurrence_25pct([[0,1], [0,1], [0], [1], [2,3], [2,3], [3], [4], [4,5], [6,7]]) != [0,1,3]:
        print("[alpha_occurrence_25pct] Multi list3 test failed")
        exit()


    if alpha_occurrence_50pct([]) != []:
        print("[alpha_occurrence_25] Empty list test failed!!")
        exit()

    if alpha_occurrence_50pct([[0, 1]]) != [0, 1]:
        print("[alpha_occurrence_50] Single list test failed")
        exit()

    if alpha_occurrence_50pct([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]]) != [0,1]:
        print("[alpha_occurrence_50pct] Multi list1 test failed")
        exit()

    if alpha_occurrence_50pct([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]]) != [1]:
        print("[alpha_occurrence_50pct] Multi list2 test failed")
        exit()

    if alpha_occurrence_50pct([[0,1], [0,1], [0], [1], [2, 3], [2,3], [3], [4], [4,5], [6,7]]) != []:
        print("[alpha_occurrence_50pct] Multi list3 test failed")
        exit()


    if alpha_occurrence_75pct([]) != []:
        print("[alpha_occurrence_75] Empty list test failed!!")
        exit()

    if alpha_occurrence_75pct([[0, 1]]) != [0, 1]:
        print("[alpha_occurrence_75 Single list test failed")
        exit()

    if alpha_occurrence_75pct([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]]) != [0]:
        print("[alpha_occurrence_75pct] Multi list1 test failed")
        exit()

    if alpha_occurrence_75pct([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]]) != []:
        print("[alpha_occurrence_75pct] Multi list2 test failed")
        exit()

    if alpha_occurrence_75pct([[0,1], [0,1], [0], [1], [2, 3], [2,3], [3], [4], [4,5], [6,7]]) != []:
        print("[alpha_occurrence_75pct] Multi list3 test failed")
        exit()

if True:
    votingTest()
    alphaOccurrenceTest()


# Main

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    for k in [10, 50, 100]:
        for indicator in INDICATORS:
            experiments(json.load(open(sys.argv[1])), k, indicator)
