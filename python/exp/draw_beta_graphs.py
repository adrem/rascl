
import itertools, os

import numpy as np
import matplotlib.pyplot as plt

DIR = "experiment_results_sc2/beta"
DIRALPHA = "experiment_results_sc2/alpha_old"
DIRFIG = "experiment_results_sc2_fig"

DATA = {}

KEYS = ["pruneFalse", "beta_0.5", "beta_1.0", "beta_1.5", "beta_2.0", "beta_2.5", "beta_3.0"]
KEYS = ["pruneFalse", "beta_0.5", "beta_1.5", "beta_2.5"]

def iterateAll():
    for p in itertools.product(
            ["topk10"],
            ["patternSorterobject_count", "patternSorterdimension_count"],
            ["dimension_precision", "dimension_recall", "object_precision", "object_recall"]
    ):
        yield p

def drawScaleGraphs(name, fileKeys):
    width = 0.9 / len(KEYS)

    for p in iterateAll():
        fig, ax = plt.subplots()

        rects = []
        for i, key in enumerate(KEYS):
            d = []
            for fileKey in fileKeys:
                try:
                    d.append(DATA[fileKey][p[0]][p[1]][p[2]][key])
                except KeyError:
                    True


            print(d)
            ind = np.arange(len(d))

            rects.append(ax.bar(ind + (i * width), d, width))

        ax.set_ylabel("Score")
        # ax.set_title(("%s_%s_%s_%s" %(name, p[0], p[1], p[2])).replace("patternSorter", ""))
        ax.set_xticks(ind + width / 2)
        ax.set_xticklabels([fileKey.split("_")[1] for fileKey in fileKeys])

        if all([len(rect) > 0 for rect in rects]):
            # ax.legend([rect[0] for rect in rects], KEYS, loc='center left', bbox_to_anchor=(1, 0.5))
            ax.legend([rect[0] for rect in rects], [key.replace("beta", "b") for key in KEYS], loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4)

        plt.ylim((0,1))
        plt.subplots_adjust(bottom=0.3)
        plt.savefig("%s/beta/%s/%s/%s_%s_%s.pdf" %(DIRFIG, name, p[0], p[0], p[1], p[2]))
        plt.close()

def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z

def readData(dir=DIR):
    global DATA

    for file in os.listdir(dir):
        if file.endswith("xlsx") or file.startswith("."):
            continue

        data = {}

        with open("%s/%s" %(dir, file)) as f:
            for line in f:
                sp = line.strip().split(" ")
                sp1 = sp[0].strip(".pdf").split("_")

                value = float(sp[1])

                ps = "%s_%s" %(sp1[5], sp1[6])
                me = "%s_%s" %(sp1[2], sp1[3])
                dd = "beta_%s" %(sp1[9]) if sp1[8] == "pruneTrue" else "pruneFalse"

                if sp1[4] not in data: data[sp1[4]] = {}
                if ps not in data[sp1[4]]: data[sp1[4]][ps] = {}
                if me not in data[sp1[4]][ps]: data[sp1[4]][ps][me] = {}

                data[sp1[4]][ps][me][dd] = value

        DATA[file] = data

    for file in os.listdir(DIRALPHA):
        if file.endswith("xlsx") or file.startswith("."):
            continue

        if file not in DATA:
            continue

        data = DATA[file]

        with open("%s/%s" %(DIRALPHA, file)) as f:
            for line in f:
                sp = line.strip().split(" ")
                sp1 = sp[0].strip(".pdf").split("_")

                if sp1[8] != "pruneFalse":
                    continue

                value = float(sp[1])

                ps = "%s_%s" %(sp1[5], sp1[6])
                me = "%s_%s" %(sp1[2], sp1[3])
                dd =  "pruneFalse"

                if sp1[4] not in data: data[sp1[4]] = {}
                if ps not in data[sp1[4]]: data[sp1[4]][ps] = {}
                if me not in data[sp1[4]][ps]: data[sp1[4]][ps][me] = {}

                data[sp1[4]][ps][me][dd] = value

def createDirs():
    for d in [
        "%s/beta/dbsizescale/topk10" %(DIRFIG),
        # "%s/beta/dbsizescale/topk25" %(DIRFIG),
        "%s/beta/dimscale/topk10" %(DIRFIG),
        # "%s/beta/dimscale/topk25" %(DIRFIG),
        "%s/beta/noisescale/topk10" %(DIRFIG),
        # "%s/beta/noisescale/topk25" %(DIRFIG),
    ]:
        if not os.path.exists(d):
            os.makedirs(d)

def drawGraphs():
    drawScaleGraphs("dbsizescale", ["dbsizescale_s1500", "dbsizescale_s2500", "dbsizescale_s3500", "dbsizescale_s4500", "dbsizescale_s5500"])
    drawScaleGraphs("dimscale", ["dimscale_d05", "dimscale_d10", "dimscale_d15", "dimscale_d20", "dimscale_d25", "dimscale_d50", "dimscale_d75"])
    drawScaleGraphs("noisescale", ["noisescale_n10", "noisescale_n30", "noisescale_n50", "noisescale_n70"])

if __name__ == '__main__':
    readData()
    createDirs()
    drawGraphs()