#!/usr/bin/env python

"""
    Experiment runner for RaSCl algorithm.

    Implements json based runner class that can run multiple experiments and write results to a single file.
"""

import itertools, json, numpy, os, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import fill_default_parameters_cc, fill_default_parameters_ex, fill_default_parameters_rascl, flatten_parameters, run_exp, split_parameters

import rascl as sc

strip_first_ellipsis = os.getcwd().endswith("exp")

if strip_first_ellipsis:
    os.chdir("..")

EXP_DIR = "experiments"
OUTPUTDIR = os.path.join("experiment_results", "rascl_sweep")


def experiments(experiments):
    global OUTPUTDIR

    output_dir =  experiments["outputDir"] if "outputDir" in experiments else OUTPUTDIR

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for expKey in experiments["subspaceCluster"]:
        data_key = experiments["subspaceCluster"][expKey]["dataFile"].split("/")[-1].split(".")[0]

        file = os.path.join(output_dir, "%s%s" %(data_key, "_prune" if "prune" in experiments["clusterConverter"]["parameters"] and experiments["clusterConverter"]["parameters"]["prune"] == "True" else ""))

        if os.path.isfile(file):
            print("File exists. Skipping. (%s)" %(file))
            continue

        w = open(file, "w")

        rangedParametersEX, fixedParametersEX = split_parameters(experiments["experiment"]["parameters"])
        rangedParametersCC, fixedParametersCC = split_parameters(experiments["clusterConverter"]["parameters"])

        for evaluation in experiments["evaluation"]:

            experiment = experiments["subspaceCluster"][expKey]

            for parametersEX in flatten_parameters(rangedParametersEX, fixedParametersEX):
                fill_default_parameters_ex(parametersEX)

                for parametersCC in flatten_parameters(rangedParametersCC, fixedParametersCC):
                    fill_default_parameters_cc(parametersCC)

                    rangedParametersSC, fixedParametersSC = split_parameters(experiment["parameters"])

                    rangedParams = [[(rangedParam["name"], value) for value in rangedParam["values"]] for rangedParam in rangedParametersSC]

                    for p in itertools.product(*rangedParams):
                        parametersSC = {pp[0]:pp[1] for pp in p}

                        for param in fixedParametersSC:
                            parametersSC[param["name"]] = param["value"]

                        print(parametersSC)
                        fill_default_parameters_rascl(parametersSC)

                        row = [evaluation] + ["%s_%s" %(pp[0], str(pp[1])) for pp in p] + ["%s" %(" ".join(["%.5f" %(numpy.average(e["all"])) for e in run_exp(sc, "RASCL", EXP_DIR, experiment, parametersEX, parametersSC, parametersCC, evaluation)]))]

                        print(row)
                        w.write(";".join(row))
                        w.write("\n")
                        w.flush()
        w.close()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    if strip_first_ellipsis:
        experiments(json.load(open(sys.argv[1][3:])))
    else:
        experiments(json.load(open(sys.argv[1])))
