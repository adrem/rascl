#!/usr/bin/env python

"""
    Experiment with CartiClus on synthetic data.

    Runs scaling experiments on synthetic data using CartiClus. Three different types of synthetic data are used:
    1) increasing data size, 2) increasing dimension size, 3) increasing noise level. The chosen parameters are provided
    by the E. Aksehirli (author of CartiClus).
"""

import os, pickle, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from carticlus import carti_clus_parameters
from exp_runner_utils import get_cluster
from measures import *

if os.getcwd().endswith("exp"):
    os.chdir("..")

OUTPUTDIR = os.path.join("experiment_results", "comparisonQuality", "carticlus")
CACHEDIR = os.path.join(OUTPUTDIR, "cache")

EVALUATORS = [
    "object_precision_res",
    "object_recall_res",
    "dimension_precision_res",
    "dimension_recall_res",
    "sc_f1P_res",
    "sc_f1R_res",
    "e4sc_res"
]


# CL #########################################################

if __name__ == '__main__':
    if not os.path.exists(OUTPUTDIR):
        os.makedirs(OUTPUTDIR)
        os.makedirs(CACHEDIR)

    print ("RUNNING CARTICLUS EXPERIMENT")

    for file in [
        ["dbsizescale_s1500", 262 ,1350, 75],
        ["dbsizescale_s2500", 437, 2250, 125],
        ["dbsizescale_s3500", 612, 3150, 175],
        ["dbsizescale_s3500", 787, 4050, 225],
        ["dbsizescale_s3500", 962, 4950, 275],
        ["dimscale_d05", 300, 600, 100],
        ["dimscale_d10", 300, 1200, 100],
        ["dimscale_d15", 300, 1800, 100],
        ["dimscale_d25", 300, 2550, 100],
        ["dimscale_d50", 300, 5250, 100],
        ["dimscale_d75", 300, 6000, 100 ],
        ["noisescale_n10", 225, 1350, 75],
        ["noisescale_n30", 262, 1350, 75],
        ["noisescale_n50", 300, 1350, 75],
        ["noisescale_n70", 375, 1350, 75],
    ]:

        f = os.path.join(OUTPUTDIR, file[0])

        if os.path.isfile(f):
            print("File exists. Skipping. (%s)" % (f))
            continue

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file[0], file[0]))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file[0], file[0]))

        num_dim = len(open(data_file).readline().split(","))

        scoreMap = {evaluator: [] for evaluator in EVALUATORS}

        for run in range(1, 11):
            pickleFile = os.path.join(CACHEDIR, "%s_run%d_k%d_minSup%d_minSize%d.pickle" %(file[0], run, file[1], file[2], file[3]))

            if os.path.isfile(pickleFile):
                cclusters = pickle.load(open(pickleFile))
            else:
                cclusters = carti_clus_parameters(data_file, {"k": file[1], "min_sup": file[2], "num_dim": num_dim, "min_size": file[3]})

                pickle.dump(cclusters, open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

            clusters = [get_cluster(row) for row in open(clusters_file)]

            for evaluator in EVALUATORS:
                scoreMap[evaluator].append(MEASURES[evaluator](cclusters, clusters))

        with open(f, "w") as w:

            for evaluator in EVALUATORS:
                w.write("%s %s\n" %(evaluator, " ".join(["%.5f" %(score) for score in scoreMap[evaluator]])))
