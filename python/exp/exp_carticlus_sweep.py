#!/usr/bin/env python

"""
    Grid search experiment with CartiClus on synthetic data.

    Runs scaling experiments on synthetic data using CartiClus. Three different types of synthetic data are used:
    1) increasing data size, 2) increasing dimension size, 3) increasing noise level. A grid around the optimal
    parameter settings (as provided by the E. Aksehirli (author of CartiClus)) is used.
"""

import itertools, os, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import get_cluster
from measures import *

from carticlus import *

if os.getcwd().endswith("exp"):
    os.chdir("..")

OUTPUTDIR = os.path.join("experiment_results", "sweepExperiment", "carticlus_sweep")


def writeScores(w, mclusters, clusters, ps):
    w.write("object_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], object_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], object_f1R_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_precision;%d;%d;%.5f\n" % (ps[0], ps[1], object_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_recall;%d;%d;%.5f\n" % (ps[0], ps[1], object_recall_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_f1R_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_precision;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_recall;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_recall_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], sc_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], sc_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_precision;%d;%d;%.5f\n" % (ps[0], ps[1], sc_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_recall;%d;%d;%.5f\n" % (ps[0], ps[1], sc_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("e4sc_res;%d;%d;%.5f\n" % (ps[0], ps[1], e4sc_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.flush()

# CL #########################################################

if __name__ == '__main__':
    if not os.path.exists(OUTPUTDIR):
        os.makedirs(OUTPUTDIR)

    print ("RUNNING CARTICLUS SWEEP EXPERIMENT")

    for file in [
        ["dbsizescale_s1500", [50, 501, 50], [800, 1801, 100]],     #262, 1350
        ["dbsizescale_s2500", [200, 651, 50], [1700, 2701, 100]],   #437, 2250
        ["dbsizescale_s3500", [400, 851, 50], [2600, 3601, 100]],   #612, 3150
        ["dimscale_d05", [100, 551, 50], [100, 1101, 100]],         #300, 600
        ["dimscale_d10", [100, 551, 50], [700, 1701, 100]],         #300, 1200
        ["dimscale_d15", [100, 551, 50], [1300, 2301, 100]],        #300, 1800
        ["dimscale_d25", [100, 551, 50], [2000, 3001, 100]],        #300, 2550
        ["dimscale_d50", [100, 551, 50], [4700, 5701, 100]],        #300, 5250
        ["dimscale_d75", [100, 551, 50], [5500, 6501, 100]],        #300, 6000
        ["noisescale_n10", [50, 501, 50], [800, 1801, 100]],        #225, 1350
        ["noisescale_n30", [50, 501, 50], [800, 1801, 100]],        #262, 1350
        ["noisescale_n50", [100, 551, 50], [800, 1801, 100]],       #300, 1350
        ["noisescale_n70", [150, 601, 50], [800, 1801, 100]],       #375, 1350
    ]:
        ks = range(file[1][0], file[1][1], file[1][2])
        ms = range(file[2][0], file[2][1], file[2][2])

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file[0], file[0]))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file[0], file[0]))

        with open(os.path.join(OUTPUTDIR, file[0]), "w") as w:

            for ps in itertools.product(ks, ms):
                print(ps)

                num_dim = len(open(data_file).readline().split(","))

                mclusters = carti_clus_parameters(data_file, {"k": ps[0], "min_sup": ps[1], "num_dim": num_dim})

                gclusters = [get_cluster(row) for row in open(clusters_file)]

                writeScores(w, mclusters, gclusters, ps)

