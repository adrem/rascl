
"""
    Implements utility functions for RaSCl and RaSClR experiment runners
"""

import itertools, os, pickle

import pandas as pd

from clusterConverter import ClusterConverter, SubspaceCluster
from measures import MEASURES, sc_precision
from patternSortingStrategy import sort_patterns

def get_cluster(row):
    sp = row.rstrip("\n").split("|")
    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v)-1 for v in sp[1].split(" ")]))


def smallOverlap(newCluster, existingClusters, maxOverlap=0.25):
    for existingCluster in existingClusters:
        if sc_precision(newCluster[0], existingCluster[0]) > maxOverlap:
            return False

    return True


def run_exp(sc, sc_name, exp_dir, experiment, parametersEX, parametersSC, parametersCC, evaluation, draw=False, redo=True, useCache=True):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]
    name = experiment["name"]

    topk = parametersEX["topk"] if "topk" in parametersEX else 10

    print("[EXPERIMENT]:", "Datafile: %s" %(experiment["dataFile"]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersEX[param]) if isinstance(parametersEX[param], int) else parametersEX[param]) for param in sorted(parametersEX.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted(parametersSC.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersCC[param]) if isinstance(parametersCC[param], int) else parametersCC[param]) for param in sorted(parametersCC.keys())]))

    exp_dir = os.path.join(exp_dir, data_key, name)
    pickle_dir = os.path.join(exp_dir, "pickles")

    evaluator = MEASURES[evaluation]

    data = []

    for run in range(1, parametersEX["runCount"] + 1):

        pickle_file = os.path.join(pickle_dir, "run%d_fimmsdims_%s.pickle" %(run, "_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])])))

        for d in [exp_dir, pickle_dir]:
            if not os.path.exists(d):
                os.makedirs(d)

        clusters = [get_cluster(row) for row in open(experiment["clustersFile"])]

        if useCache and os.path.isfile(pickle_file):
            print("Pickle file found!")
            print(pickle_file)

            if not redo:
                return

            object = pickle.load(open(pickle_file))

            fimms = object[0]
            dims = object[1]
        else:
            print("Running %s" %(sc_name))

            samples, transactions, fimms, dims = sc.sc_parameters(experiment["dataFile"], parameters=parametersSC, run=run, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=useCache)

            if useCache:
                pickle.dump([fimms, dims], open(pickle_file, "w"), pickle.HIGHEST_PROTOCOL)

        rows = pd.read_csv(experiment["dataFile"], delimiter=",", header=None, index_col=False)

        clusterConverter = ClusterConverter(rows, parametersCC)

        evaluatedClusters = []

        fimmsPlusDims = [(fimm, dims[i]) for i, fimm in enumerate(fimms)]

        fimmsPlusDims = sort_patterns(parametersEX["patternSorter"], fimmsPlusDims)

        clusterAssigner = MEASURES[parametersEX["clusterAssigner"]]

        for fimmPlusDim in fimmsPlusDims:
            fimm = fimmPlusDim[0]
            dims = fimmPlusDim[1]

            fimm = clusterConverter.convert(fimm, dims)

            vv = [(i, clusterAssigner(fimm, cluster)) for i, cluster in enumerate(clusters)]
            vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

            closest = vv[0]

            evaluatedClusters.append((fimm, closest))

        topKClusters = []

        for evaluatedCluster in evaluatedClusters:
            if smallOverlap(evaluatedCluster, topKClusters, maxOverlap=parametersEX["overlap"]):
                if len(evaluatedCluster[0].objects) > 10:
                    topKClusters.append(evaluatedCluster)

            if len(topKClusters) == topk:
                break

        cRs = [cluster[0] for cluster in topKClusters]
        cGs = [clusters[cluster[1][0]] for cluster in topKClusters]

        data.append({"all": evaluator(cRs, clusters) if len(cRs) > 0 else -1})

    return data


def fill_default_parameters_cc(parameters):
    if "prune" not in parameters: parameters["prune"] = "True"
    if "recomputeDimensions" not in parameters: parameters["recomputeDimensions"] = "True"
    if "dimensionsAlg" not in parameters: parameters["dimensionsAlg"] = "alpha|0"


def fill_default_parameters_ex(parameters):
    if "topk" not in parameters: parameters["topk"] = 10
    if "patternSorter" not in parameters: parameters["patternSorter"] = "object_count"
    if "overlap" not in parameters: parameters["overlap"] = 0.25
    if "runCount" not in parameters: parameters["runCount"] = 10


def fill_default_parameters_rascl(parameters):
    if "n" not in parameters: parameters["n"] = 1000
    if "k" not in parameters: parameters["k"] = 200
    if "kk" not in parameters: parameters["kk"] = 5
    if "s" not in parameters: parameters["s"] = "rmis|100"
    if "m" not in parameters: parameters["m"] = 100
    if "sampleDimCount" not in parameters: parameters["sampleDimCount"] = "weightedFD|2"
    if "expand" not in parameters: parameters["expand"] = "none"


def fill_default_parameters_rasclr(parameters):
    if "n" not in parameters: parameters["n"] = 1000
    if "k" not in parameters: parameters["k"] = 5
    if "s" not in parameters: parameters["s"] = "rmis|100"
    if "m" not in parameters: parameters["m"] = 100
    if "sampleDimCount" not in parameters: parameters["sampleDimCount"] = "weightedFD|2"
    if "expand" not in parameters: parameters["expand"] = "none"



def flatten_parameters(rangedParameters, fixedParameters):
    parametersList = [[(rangedParameter["name"], value) for value in rangedParameter["values"]] for rangedParameter in rangedParameters]
    parametersList.extend([[(fixedParameter["name"], fixedParameter["value"])] for fixedParameter in fixedParameters])

    for parameters in itertools.product(*parametersList):
        yield {parameter[0]: parameter[1] for parameter in parameters}


def split_parameters(parameters):
    rangedParameters = []
    fixedParameters = []

    for parameter in parameters:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
            parameter["values"] = range(parameter["range"]["start"], parameter["range"]["end"], parameter["range"]["step"])
        elif parameter["type"] == "values":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters
