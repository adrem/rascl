#!/usr/bin/env python

"""
    Experiment runner for RaSCl-R algorithm.

    Implements json based runner class that can run multiple experiments and write results to a single file.
"""

import itertools, json, numpy, os, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import fill_default_parameters_cc, fill_default_parameters_ex, fill_default_parameters_rasclr, flatten_parameters, run_exp, split_parameters

import rasclr as sc

strip_first_ellipsis = os.getcwd().endswith("exp")

if strip_first_ellipsis:
    os.chdir("..")

EXP_DIR = "experiments"
OUTPUTDIR = os.path.join("experiment_results", "rasclr")


def write_info(w, infoArr, parametersEX, parametersSC, parametersCC, evaluation):
    for i in range(0, len(infoArr)):
        w.write("test_%s%s_%s_topk%d_patternSorter%s_overlap%.2f_prune%s_recomputeDimensions%s_dimensionsAlg%s_n%d_k%d_s%s_sampleDimCount%s_m%d.pdf" % (infoArr[i]["test"], "%d" %(infoArr[i]["value"]) if isinstance(infoArr[i]["value"], int) else infoArr[i]["value"], evaluation.replace("_", "-"), parametersEX["topk"], parametersEX["patternSorter"], parametersEX["overlap"], str(parametersCC["prune"]), str(parametersCC["recomputeDimensions"]), parametersCC["dimensionsAlg"], parametersSC["n"], parametersSC["k"], parametersSC["s"], parametersSC["sampleDimCount"], parametersSC["m"]))
        w.write(" ")
        w.write(" ".join(["%.2f" %(numpy.average(arr["all"])) for arr in infoArr[i]["data"]]))
        w.write("\n")
        w.flush()


def _shouldPrune(parameters):
    for parameter in parameters:
        if parameter["name"] == "prune" and parameter["value"] == "True":
            return True

    return False


def experiments(experiments):
    global OUTPUTDIR

    output_dir =  experiments["outputDir"] if "outputDir" in experiments else OUTPUTDIR

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for expKey in experiments["subspaceCluster"]:
        data_key = experiments["subspaceCluster"][expKey]["dataFile"].split("/")[-1].split(".")[0]

        file = os.path.join(output_dir, "%s%s" %(data_key, "_prune" if "prune" in experiments["clusterConverter"]["parameters"] and experiments["clusterConverter"]["parameters"]["prune"] == "True" else ""))

        if os.path.isfile(file):
            print("File exists. Skipping. (%s)" % (file))
            continue

        w = open(file, "w")

        rangedParametersEX, fixedParametersEX = split_parameters(experiments["experiment"]["parameters"])
        rangedParametersCC, fixedParametersCC = split_parameters(experiments["clusterConverter"]["parameters"])

        for evaluation in experiments["evaluation"]:

            experiment = experiments["subspaceCluster"][expKey]

            for parametersEX in flatten_parameters(rangedParametersEX, fixedParametersEX):
                fill_default_parameters_ex(parametersEX)

                for parametersCC in flatten_parameters(rangedParametersCC, fixedParametersCC):
                    fill_default_parameters_cc(parametersCC)

                    rangedParametersSC, fixedParametersSC = split_parameters(experiment["parameters"])

                    if len(rangedParametersSC) != 1:
                        print ("Cannot handle ranged params of length %d" %(len(rangedParametersSC)))
                        break

                    rangedParameterSC = rangedParametersSC[0]

                    infoArr = []

                    for v in rangedParameterSC["values"]:
                        parametersSC = {rangedParameterSC["name"]: v}

                        for param in fixedParametersSC:
                            parametersSC[param["name"]] = param["value"]

                        fill_default_parameters_rasclr(parametersSC)

                        infoArr.append({"test": rangedParameterSC["name"], "value": v, "data": run_exp(sc, "RASCLR", EXP_DIR, experiment, parametersEX, parametersSC, parametersCC, evaluation, useCache=True)})

                    write_info(w, infoArr, parametersEX, parametersSC, parametersCC, evaluation)

        w.close()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    if strip_first_ellipsis:
        experiments(json.load(open(sys.argv[1][3:])))
    else:
        experiments(json.load(open(sys.argv[1])))

