
import itertools, os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 16 })

DIR = "experiment_results_dimension_strategy"
DIRFIG = "experiment_results_dimension_strategy_fig"

DATA = None

KEYS = [r"$\alpha=0.03$", r"$\alpha=0.05$", r"$\alpha=0.10$", r"$\alpha=0.15$", r"$\alpha=0.20$"]

FILES = ["dbsizescale_s2500", "dbsizescale_s5500", "dimscale_d25", "dimscale_d75", "noisescale_n30", "noisescale_n70"]

FILEMAP = {
    "dbsizescale_s2500": r"$dbsizescale_{s2500}$",
    "dbsizescale_s5500": r"$dbsizescale_{s5500}$",
    "dimscale_d25": r"$dimscale_{d25}$",
    "dimscale_d75": r"$dimscale_{d75}$",
    "noisescale_n30": r"$noisescale_{n30}$",
    "noisescale_n70": r"$noisescale_{n70}$",
}

SCORES = [
        # "object_f1",
        # "object_precision",
        # "object_recall",
        # "dimension_f1",
        "dimension_precision",
        # "dimension_recall",
        # "e4sc_f1",
        # "e4sc_precision",
        # "e4sc_recall"
    ]

SCOREMAP = {
    "dimension_precision": r"$PRECISION_{DIM}$"
}

def drawGraphs(data):
    for score in SCORES:
        for file in FILES:
            print(file, score, data[file][score])
            y = [data[file][score][key] for key in KEYS]
            print(y)

            plt.plot(range(0, len(y)), y)

        plt.xticks(range(0, len(y)), KEYS)

        plt.ylim((0.6,1))
        plt.ylabel(SCOREMAP[score])

        plt.legend([FILEMAP[file] for file in FILES], loc='upper center', bbox_to_anchor=(0.5, -0.09), ncol=2)

        plt.subplots_adjust(bottom=0.3)
        plt.savefig("%s/%s.pdf" % (DIRFIG, score))
        plt.close()

def readData(dir_sc):
    d = {}

    for file in os.listdir(dir_sc):
        if file.endswith("xlsx") or file.startswith("."):
            continue

        if file == "old":
            continue

        if file.replace("_prune", "") not in d:
            d[file.replace("_prune", "")] = {}

        data = d[file.replace("_prune", "")]

        with open("%s/%s" %(dir_sc, file)) as f:
            for line in f:
                sp = line.strip().split(" ")
                sp1 = sp[0].strip(".pdf").split("_")

                value = float(sp[1])
                me = "%s_%s" %(sp1[2], sp1[3])
                dd = r"$\alpha=%s$" %(sp1[13]) if sp1[8] == "pruneTrue" else r"$\alpha=%s$" %(sp1[12])

                if me not in data: data[me] = {}

                data[me][dd] = value

    return d


if __name__ == '__main__':
    DATA = readData(DIR)
    drawGraphs(DATA)