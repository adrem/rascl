
import json, math, os.path, pickle, sys

import numpy as np

from matplotlib import pyplot as plt

from dataStore import *
from rascl_utils import transactions_to_items

KEY_FIMMS = "fimms"


VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return [set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")])]

def exp(sc, experiment, parameters, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, method)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])
    items = transactions_to_items(transactions)

    intersectionSize = 0
    intersectionCount = 0
    maxIntersection = 0
    minIntersection = 10000

    for i1, fimm1 in enumerate(fimms):
        overlaps = sorted([(i, len(cluster & set(fimm1))) for i, cluster in enumerate(clusters)],
                          key=lambda tuple: tuple[1], reverse=True)

        if overlaps[0][0] != 0:
            continue

        intersect = reduce(set.intersection, map(set, [items[item] for item in fimm1]))

        dimensions = [samples[tid / parameters["k"]][1] for tid in intersect]

        dimCounts = sc.Counter([d for dim in dimensions for d in dim])

        # sc.drawRows(rows, fimm1, dimCounts)

        for i2, fimm2 in enumerate(fimms):
            if i1 >= i2:
                continue

            intersection = set(fimm1) & set(fimm2)
            if len(intersection) > 5:
                intersectionCount += len(intersection)
                intersectionSize += 1
                maxIntersection = max(maxIntersection, len(intersection))
                minIntersection = min(minIntersection, len(intersection))

    print("%d %d %.5f %d %d" %(intersectionCount, intersectionSize, 1. * intersectionCount / intersectionSize if intersectionSize > 0 else 0, maxIntersection, minIntersection))

def drawInfoGrouped(experiment, infoArr, key):
    for ic in range(0, len(infoArr[0]["data"][key])):
        expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["method"])
        figGroupedDir = "%s/figGrouped" % (expDir)

        plotFile = "%s/%s_test_%s_cluster_%d.pdf" %(figGroupedDir, key, infoArr[0]["test"], ic)

        x = [i["value"] for i in infoArr]

        for j in range(4, 11):
            y = []
            for ix, i in enumerate(x):
                y.append(infoArr[ix]["data"][key][ic][j])

            plt.plot(x, y, label="%d" %(j))

        plt.legend()
        plt.savefig(plotFile)
        plt.cla()
        plt.clf()

def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2 as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = bool(experiment["fixedDimensions"])

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            exp(sc, experiment, parameters)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    experiments(json.load(open(sys.argv[1])))
