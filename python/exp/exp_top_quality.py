
import json, math, os.path, pickle, sys

import numpy as np

from collections import Counter
from matplotlib import pyplot as plt

from clustering import *
from dataStore import *
from dataUtils import *
from dimensionStrategy import *
from measures import *

KEY_FIMMS = "fimms"

VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

MEASURES = [
    ("precision", precision),
    ("recall", recall),
    ("f1", f1),
    ("e4sc_precision", e4sc_precision),
    ("e4sc_recall", e4sc_recall),
    ("e4sc_f1", e4sc_f1),
]

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")]))

def exp(sc, experiment, parameters, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]
    topk = parameters["topk"] if "topk" in parameters else 10

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    counts = {}
    lengths = {}
    dimss = {}

    for sample in samples:
        for d in sample[1]:
            if not d in counts: counts[d] = 0
            counts[d] += 1

        l = len(sample[1])
        if not l in lengths: lengths[l] = 0
        lengths[l] += 1

        key = "_".join(["%d" %d for d in sorted(sample[1])])
        if not key in dimss: dimss[key] = 0
        dimss[key] += 1

    measures = {i: {} for i, cluster in enumerate(clusters)}
    for i, cluster in enumerate(clusters):
        for measure in MEASURES:
            measures[i][measure[0]] = []

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    nrows = []

    minMax = [[1000000, -1000000] for i in range(0, len(rows[0]))]

    for row in rows:
        for i in range(0, len(row)):
            minMax[i][0] = min(minMax[i][0], row[i])
            minMax[i][1] = max(minMax[i][1], row[i])

    for row in rows:
        nrows.append([(1. * v - minMax[i][0]) / (minMax[i][1] - minMax[i][0]) for i, v in enumerate(row)])

    for measure in MEASURES:

        evaluatedClusters = []

        for ix, fimm in enumerate(fimms):

            ndps = [nrows[int(f)] for f in fimm]

            fimm = SubspaceCluster(set(fimm), set(infer_dimensions(ALG_STDEV2_0_04, fimm, dims[ix], ndps)))

            values = [(i, measure[1](fimm, cluster)) for i, cluster in enumerate(clusters)]

            values = sorted(values, key=lambda tuple: tuple[1], reverse=True)

            closest = values[0]

            evaluatedClusters.append((fimm, closest))

        evaluatedClusters = sorted(evaluatedClusters, key=lambda tuple: tuple[1][1], reverse=True)[0:topk]

        for evaluatedCluster in evaluatedClusters:
            measures[evaluatedCluster[1][0]][measure[0]].append(evaluatedCluster[1][1])

    return measures

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key, topk):
    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped" % (expDir)

    plotFile = "%s/test_%s_topk%d_%s.pdf" %(figGroupedDir, infoArr[0]["test"], topk, key)

    for ic in range(0, len(infoArr[0]["data"])):
        x = [i["value"] for i in infoArr]

        y = []
        for ix, i in enumerate(x):
            y.append(infoArr[ix]["data"][ic][key])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ic * 0.3, widths=0.25)
        p = plt.plot([], label="%d" %(ic))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()

def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2_ckdtree as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            infoArr.append({"test": rangedParam["name"], "value": v, "data": exp(sc, experiment, parameters)})

        for measure in MEASURES:
            drawInfoGroupedArr(experiment, infoArr, measure[0], 10)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    experiments(json.load(open(sys.argv[1])))
