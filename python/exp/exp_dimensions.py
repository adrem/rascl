
import json, math, os.path, pickle, sys

import numpy as np

from collections import Counter
from matplotlib import pyplot as plt

from dataStore import *
from dataUtils import *
from measures import *

KEY_FIMMS = "fimms"

VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

MEASURES = [
    ("precision", precision),
    ("recall", recall),
    ("f1", f1)
]

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return [set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")])]

def exp(sc, experiment, parameters, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if not os.path.isfile(pickleFile):
        print("Pickle not found. Abort this exp.")
        return None

    print("Pickle file found!")

    if not redo:
        return

    object = pickle.load(open(pickleFile))

    samples = object[0]
    transactions = object[1]
    fimms = object[2]
    dims = object[3]

    m = {}

    alpha = 0.5

    for ix, dim in enumerate(dims):
        #Check why dimensions can be empty

        dimCounts = Counter([d for dd in dim for d in dd])

        most_common = dimCounts.most_common()[0:2]
        most_common = [d for d in dimCounts.most_common() if d[1] >= alpha * len(dim)]

        most_common = sorted(most_common, key=lambda tuple: tuple[0])

        dimdimCounts = Counter(["_".join("%d" %(d) for d in sorted(dd)) for dd in dim])

        if len(dimdimCounts) > 0:
            most_common = dimdimCounts.most_common()[0:2]
        else:
            most_common = []

        if len(most_common) > 0:
            # key = "_".join(["%d" %(d[0]) for d in most_common])

            for mc in most_common:
                key = mc[0]

                if not key in m: m[key] = 0

                m[key] += 1

    print(m)

    values = sorted([(k, m[k]) for k in m], key=lambda tuple: tuple[1], reverse=True)

    for value in values[0:5]:
        print(value)

    return []

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key):
    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped" % (expDir)

    plotFile = "%s/test_%s_%s.pdf" %(figGroupedDir, infoArr[0]["test"], key)

    for ic in range(0, len(infoArr[0]["data"])):
        x = [i["value"] for i in infoArr]

        y = []
        for ix, i in enumerate(x):
            y.append(infoArr[ix]["data"][ic][key])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ic * 0.3, widths=0.25)
        p = plt.plot([], label="%d" %(ic))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()


def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2 as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            print(rangedParam["name"])
            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            result = exp(sc, experiment, parameters)

            if result != None:
                print("Found")
                infoArr.append({"test": rangedParam["name"], "value": v, "data": result})

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    experiments(json.load(open(sys.argv[1])))
