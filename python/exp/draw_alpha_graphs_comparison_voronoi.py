
import itertools, os

import numpy as np
import matplotlib.pyplot as plt

TEST = "dimensions"

DIR_SC1 = "experiment_results_sc1/%s_prune" %(TEST)
DIR_SC2 = "experiment_results_sc2/%s_prune" %(TEST)

DIRFIG = "experiment_results_fig_comparison_voronoi"

DATA_SC1 = {}
DATA_SC2 = {}

KEYS = ["alpha_0.03", "alpha_0.10", "alpha_0.15"]

value = 10
test1 = "k%d" %(value)
test2 = "kk%d" %(value)

def iterateAll():
    for p in itertools.product(
            # ["topk10", "topk25"],
            # ["patternSorterobject_count", "patternSorterdimension_count"],
            ["topk10"],
            ["patternSorterobject_count"],
            # ["e4sc_precision", "e4sc_recall", "e4sc_f1", "object_precision", "object_recall", "object_f1", "dimension_precision", "dimension_recall", "dimension_f1"]
            # ["object_precision", "object_recall", "dimension_precision", "dimension_recall"]
            ["dimension_f1", "dimension_precision", "dimension_recall"]
    ):
        yield p

def drawScaleGraphs(name, fileKeys):
    keys = ["sc1_%s" %(key) for key in KEYS] + ["sc2_%s" %(key) for key in KEYS]

    width = 0.9 / (len(keys))

    for p in iterateAll():
        fig, ax = plt.subplots()

        rects = []

        for i, key in enumerate(KEYS):
            print(fileKeys[0], p)
            print(DATA_SC1["dbsizescale_s1500"])
            # print(DATA_SC1[fileKeys[0]][p[0]][p[1]][p[2]])
            d = [DATA_SC1[fileKey][p[0]][p[1]][p[2]][key] for fileKey in fileKeys]

            ind = np.arange(len(fileKeys))

            rects.append(ax.bar(ind + (i * width), d, width))

        for i, key in enumerate(KEYS):
            d = [DATA_SC2[fileKey][p[0]][p[1]][p[2]][key] for fileKey in fileKeys]

            ind = np.arange(len(fileKeys))

            rects.append(ax.bar(ind + ((len(KEYS)+i) * width), d, width))

        ax.set_ylabel("Score")
        # ax.set_title(("%s_%s_%s_%s" %(name, p[0], p[1], p[2])).replace("patternSorter", ""))
        ax.set_xticks(ind + width / 2)
        ax.set_xticklabels([fileKey.split("_")[1] for fileKey in fileKeys])

        ax.legend([rect[0] for rect in rects], keys, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)

        plt.ylim((0,1))
        plt.subplots_adjust(bottom=0.3)
        plt.savefig("%s/%s/%s/%s/%s_%s_%s.pdf" %(DIRFIG, TEST, name, p[0], test1, p[1], p[2]))
        plt.close()

def readData(dir_sc):
    d = {}

    for file in os.listdir(dir_sc):
        if file.endswith("xlsx") or file.startswith("."):
            continue

        if file.replace("_prune", "") not in d:
            d[file.replace("_prune", "")] = {}

        data = d[file.replace("_prune", "")]

        with open("%s/%s" %(dir_sc, file)) as f:
            for line in f:
                if test1 not in line and test2 not in line:
                    continue

                print(line)

                sp = line.strip().split(" ")
                sp1 = sp[0].strip(".pdf").split("_")

                value = float(sp[1])

                ps = "%s_%s" %(sp1[5], sp1[6])
                me = "%s_%s" %(sp1[2], sp1[3])
                dd = "alpha_%s" %(sp1[13]) if sp1[8] == "pruneTrue" else "alpha_%s" %(sp1[12])

                if sp1[4] not in data: data[sp1[4]] = {}
                if ps not in data[sp1[4]]: data[sp1[4]][ps] = {}
                if me not in data[sp1[4]][ps]: data[sp1[4]][ps][me] = {}

                print(sp1[4])
                data[sp1[4]][ps][me][dd] = value
                print(ps, me, dd)

    print("DD", d)
    return d

def createDirs():
    for d in ["dbsizescale", "dimscale", "noisescale"]:
        dir = "%s/%s/%s/topk10" %(DIRFIG, TEST, d)

        if not os.path.exists(dir):
            os.makedirs(dir)

def drawGraphs():
    drawScaleGraphs("dbsizescale", ["dbsizescale_s1500", "dbsizescale_s2500", "dbsizescale_s3500"])#, "dbsizescale_s4500", "dbsizescale_s5500"])
    drawScaleGraphs("dimscale", ["dimscale_d05", "dimscale_d10", "dimscale_d25", "dimscale_d50", "dimscale_d75"])#, "dimscale_d15", "dimscale_d20", "dimscale_d25", "dimscale_d50", "dimscale_d75"])
    drawScaleGraphs("noisescale", ["noisescale_n10", "noisescale_n30", "noisescale_n50", "noisescale_n70"])

if __name__ == '__main__':
    DATA_SC1 = readData(DIR_SC1)
    DATA_SC2 = readData(DIR_SC2)
    createDirs()
    drawGraphs()