
import json, os.path, pickle, sys

import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 16 })

import pandas as pd

from pandas.plotting import andrews_curves

from sklearn.manifold import TSNE
from sklearn.metrics import silhouette_score, silhouette_samples

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import fill_default_parameters_cc, fill_default_parameters_ex, fill_default_parameters_rascl, flatten_parameters, get_cluster, smallOverlap, split_parameters
from clusterConverter import ClusterConverter
from colors import tableau20
from measures import *
from patternSortingStrategy import *

import rascl as sc

strip_first_ellipsis = os.getcwd().endswith("exp")

if strip_first_ellipsis:
    os.chdir("..")

EXP_DIR = "experiments"
OUTPUTDIR = os.path.join("experiment_results_fig", "clusters")


MAP = {
    "sc_precision": r"$precision_{SC}$",
    "sc_recall": r"$recall_{SC}$",
    "sc_f1": r"$F1_{SC}$"
}


def exp(sc, experiment, exp_dir, parametersEX, parametersSC, parametersCC, draw=False, redo=True, use_cache=True):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]
    name = experiment["name"]

    topk = parametersEX["topk"] if "topk" in parametersEX else 10

    print("[EXPERIMENT]:", "Datafile: %s" %(experiment["dataFile"]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersEX[param]) if isinstance(parametersEX[param], int) else parametersEX[param]) for param in sorted(parametersEX.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted(parametersSC.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersCC[param]) if isinstance(parametersCC[param], int) else parametersCC[param]) for param in sorted(parametersCC.keys())]))

    exp_dir = os.path.join(exp_dir, data_key, name)
    pickle_dir = os.path.join(exp_dir, "pickles")

    data = []

    for run in range(1, parametersEX["runCount"] + 1):
        pickle_file = os.path.join(pickle_dir, "run%d_fimmsdims_%s.pickle" %(run, "_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])])))

        for d in [exp_dir, pickle_dir]:
            if not os.path.exists(d):
                os.makedirs(d)

        clusters = [get_cluster(row) for row in open(experiment["clustersFile"])]

        if os.path.isfile(pickle_file):
            print("Pickle file found!")

            if not redo:
                return

            object = pickle.load(open(pickle_file))

            fimms = object[0]
            dims = object[1]
        else:
            print("Running RASCL")

            samples, transactions, fimms, dims = sc.sc_parameters(experiment["dataFile"], parameters=parametersSC, run=run, draw=draw, draw_new_samples=draw, draw_new_centers=draw, use_cache=use_cache)

            if use_cache:
                pickle.dump([fimms, dims], open(pickle_file, "w"), pickle.HIGHEST_PROTOCOL)

        rows = pd.read_csv(experiment["dataFile"], delimiter=",", header=None, index_col=False)

        clusterConverter = ClusterConverter(rows, parametersCC)

        evaluatedClusters = []

        fimmsPlusDims = [(fimm, dims[i]) for i, fimm in enumerate(fimms)]

        fimmsPlusDims = sort_patterns(parametersEX["patternSorter"], fimmsPlusDims)

        for fimmPlusDim in fimmsPlusDims:
            fimm = fimmPlusDim[0]
            dims = fimmPlusDim[1]

            fimm = clusterConverter.convert(fimm, dims)

            vv = [(i, sc_f1(fimm, cluster)) for i, cluster in enumerate(clusters)]
            vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

            closest = vv[0]

            evaluatedClusters.append((fimm, closest))

        topKClusters = []

        for evaluatedCluster in evaluatedClusters:
            if smallOverlap(evaluatedCluster, topKClusters, maxOverlap=parametersEX["overlap"]):
                if len(evaluatedCluster[0].objects) > 10:
                    topKClusters.append(evaluatedCluster)

            if len(topKClusters) == topk:
                break

        data.append(topKClusters)

    return data


def draw_separate(experiment, data, parametersEX, parametersSC, parametersCC):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]

    raw_data = np.array([[float(v) for v in line.split(",")] for line in open(experiment["dataFile"])])
    raw_data = raw_data / np.linalg.norm(raw_data)

    if not os.path.exists(os.path.join(OUTPUTDIR, data_key)):
        os.makedirs(os.path.join(OUTPUTDIR, data_key))

    for run, ddata in enumerate(data):

        for ix, cluster in enumerate(ddata):
            plotFile = os.path.join(OUTPUTDIR, data_key, "%s_run%d_cluster%d.pdf" %("_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])]), run + 1, ix + 1))

            newdata = [[row[i] for i in cluster[0].dimensions] for row in raw_data]
            labels = [1 if i in cluster[0].objects else 0 for i in range(0, len(raw_data))]

            X = newdata
            cluster_labels = labels

            n_clusters = 2

            # Create a subplot with 1 row and 2 columns
            fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
            fig.set_size_inches(20, 5)

            # The 1st subplot is the silhouette plot
            # The silhouette coefficient can range from -1, 1 but in this example all
            # lie within [-0.1, 1]
            ax1.set_xlim([-1, 1])
            # The (n_clusters+1)*10 is for inserting blank space between silhouette
            # plots of individual clusters, to demarcate them clearly.
            ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

            # The silhouette_score gives the average value for all the samples.
            # This gives a perspective into the density and separation of the formed
            # clusters
            silhouette_avg = silhouette_score(X, cluster_labels)
            print("For n_clusters =", n_clusters,
                  "The average silhouette_score is :", silhouette_avg)

            # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(X, cluster_labels)

            y_lower = 10

            mapping = ["all", "cluster%d" % (ix + 1)]

            for i in range(n_clusters):
                # Aggregate the silhouette scores for samples belonging to
                # cluster i, and sort them
                ith_cluster_silhouette_values = [v for ii, v in enumerate(sample_silhouette_values) if cluster_labels[ii] == i]

                ith_cluster_silhouette_values.sort()

                size_cluster_i = len(ith_cluster_silhouette_values)
                y_upper = y_lower + size_cluster_i

                color = "red" if i == 1 else "blue"

                ax1.fill_betweenx(np.arange(y_lower, y_upper), 0, ith_cluster_silhouette_values, facecolor=color,
                                  edgecolor=color, alpha=0.7)

                # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.5, y_lower + 0.5 * size_cluster_i, mapping[i])

                # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples

            ax1.set_title("Silhouette plot")
            #     ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")

            # The vertical line for average silhouette score of all the values
            # ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1])

            # 2nd Plot showing the actual clusters formed
            X_embedded = TSNE(n_components=2).fit_transform(X)

            colors = ["blue" if label == 0 else "red" for label in cluster_labels]
            ax2.scatter([v[0] for v in X_embedded], [v[1] for v in X_embedded], marker='.', s=30, lw=0, alpha=0.7,
                        c=colors, edgecolor='k')

            ax2.set_title("t-SNE plot with cluster")

            dddata = [X_embedded[i] for i, label in enumerate(cluster_labels) if label == 0]
            colors = ["blue" for ddd in dddata]

            ax3.set_title("t-SNE plot without cluster")
            ax3.scatter([v[0] for v in dddata], [v[1] for v in dddata], marker='.', s=30, lw=0, alpha=0.7,
                        c=colors, edgecolor='k')

            df = pd.DataFrame(X)
            df = df.join(pd.DataFrame(["cluster%d" % (ix + 1) if v == 1 else "all" for v in cluster_labels], columns=["label"]))

            andrews_curves(df.loc[df["label"] == "all"], "label", color=["blue"], ax=ax4)
            andrews_curves(df.loc[df["label"] == "cluster%d" % (ix + 1)], "label", color=["red"], ax=ax4)

            ax4.set_title("Andrews curves")

            plt.savefig(plotFile)


def draw_combined(experiment, data, parametersEX, parametersSC, parametersCC):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]

    raw_data = np.array([[float(v) for v in line.split(",")] for line in open(experiment["dataFile"])])
    raw_data = raw_data / np.linalg.norm(raw_data)

    for run, ddata in enumerate(data):

        plotFile = os.path.join(OUTPUTDIR, data_key, "%s_run%d_all.pdf" % ("_".join( ["%s%s" % (param, "%d" % (parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])]), run + 1))

        cluster_labels = []

        for i in range(0, len(raw_data)):
            added = False
            for ic, cluster in enumerate(ddata):
                if i in cluster[0].objects:
                    cluster_labels.append("cluster%d" % (ic + 1))
                    added = True
                    break

            if not added:
                cluster_labels.append("all")

        n_clusters = len(ddata) + 1

        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(10, 5)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1 but in this example all
        # lie within [-0.1, 1]
        ax1.set_xlim([-1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(raw_data) + (n_clusters + 1) * 100])

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(raw_data, cluster_labels)
        print("For n_clusters =", n_clusters,
              "The average silhouette_score is :", silhouette_avg)

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(raw_data, cluster_labels)

        y_lower = 100

        mapping = ["cluster%d" % (i + 1) for i in range(0, len(ddata))] + ["all"]

        colors = ["red", "green", "orange", "yellow", "blue", "pink", "cyan", "violet", "brown", "black", "gray"]

        for i in range(n_clusters - 1, -1, -1):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = [v for ii, v in enumerate(sample_silhouette_values) if cluster_labels[ii] == mapping[i]]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = len(ith_cluster_silhouette_values)
            y_upper = y_lower + size_cluster_i

            ax1.fill_betweenx(np.arange(y_lower, y_upper), 0, ith_cluster_silhouette_values, facecolor=colors[i],
                              edgecolor="gray", alpha=0.7)

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.5, y_lower + 0.5 * size_cluster_i, mapping[i])

            # Compute the new y_lower for next plot
            y_lower = y_upper + 100  # 10 for the 0 samples

        ax1.set_title("Silhouette plot")
        #     ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        # ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1])

        # 2nd Plot showing the actual clusters formed
        X_embedded = TSNE(n_components=2).fit_transform(raw_data)

        ccolors = ["gray" if label == "all" else colors[int(label.replace("cluster", "")) - 1] for label in cluster_labels]
        ax2.scatter([v[0] for v in X_embedded], [v[1] for v in X_embedded], marker='.', s=30, lw=0, alpha=0.7,
                    c=ccolors, edgecolor='k')

        ax2.set_title("t-SNE plot with clusters")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")

        plt.savefig(plotFile)


def experiments(experiments):
    for expKey in experiments["subspaceCluster"]:
        rangedParametersEX, fixedParametersEX = split_parameters(experiments["experiment"]["parameters"])
        rangedParametersCC, fixedParametersCC = split_parameters(experiments["clusterConverter"]["parameters"])

        experiment = experiments["subspaceCluster"][expKey]

        for parametersEX in flatten_parameters(rangedParametersEX, fixedParametersEX):
            fill_default_parameters_ex(parametersEX)

            for parametersCC in flatten_parameters(rangedParametersCC, fixedParametersCC):
                fill_default_parameters_cc(parametersCC)

                rangedParametersSC, fixedParametersSC = split_parameters(experiment["parameters"])

                if len(rangedParametersSC) != 0:
                    print ("Cannot handle ranged params of length %d" %(len(rangedParametersSC)))
                    break

                parametersSC = {}

                for param in fixedParametersSC:
                    parametersSC[param["name"]] = param["value"]

                fill_default_parameters_rascl(parametersSC)

                data = exp(sc, experiment, EXP_DIR, parametersEX, parametersSC, parametersCC)

                draw_separate(experiment, data, parametersEX, parametersSC, parametersCC)
                draw_combined(experiment, data, parametersEX, parametersSC, parametersCC)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    if strip_first_ellipsis:
        experiments(json.load(open(sys.argv[1][3:])))
    else:
        experiments(json.load(open(sys.argv[1])))

