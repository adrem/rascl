
import json, math, os.path, pickle, sys

import numpy as np

from matplotlib import pyplot as plt

from dataStore import *
from rascl_utils import transactions_to_items

KEY_FIMMS = "fimms"


VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return [set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")])]

def exp(sc, experiment, parameters, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    counts = {}
    lengths = {}
    dims = {}

    for sample in samples:
        for d in sample[1]:
            if not d in counts: counts[d] = 0
            counts[d] += 1

        l = len(sample[1])
        if not l in lengths: lengths[l] = 0
        lengths[l] += 1

        key = "_".join(["%d" %d for d in sorted(sample[1])])
        if not key in dims: dims[key] = 0
        dims[key] += 1

    items = transactions_to_items(transactions)

    clustersCounts = [[0 for i in range(0, 11)] for cluster in clusters]
    sizes = [[0 for i in range(0, 11)] for cluster in clusters]
    sizesArr = [[[] for i in range(0, 11)] for cluster in clusters]
    clusterOverlaps = [[[] for i in range(0, 11)] for cluster in clusters]
    supports = [[0 for i in range(0, 11)] for cluster in clusters]

    for fimm in fimms:
        overlaps = sorted([(i, len(cluster[0] & set(fimm))) for i, cluster in enumerate(clusters)], key=lambda tuple: tuple[1], reverse=True)

        purity = 1. * overlaps[0][1] / len(fimm)

        ix = int(math.floor(purity * 10))

        clustersCounts[overlaps[0][0]][ix]  += 1
        sizes[overlaps[0][0]][ix] += len(fimm)
        sizesArr[overlaps[0][0]][ix].append(len(fimm))
        clusterOverlaps[overlaps[0][0]][ix].append(overlaps[0][1])

        support = set(items[fimm[0]])
        for i in range(1, len(fimm)):
            support = support.intersection(set(items[fimm[i]]))

        supports[overlaps[0][0]][ix] += len(support)

    for i in range(0, len(clusters)):
        for j in range(0, 11):
            if clustersCounts[i][j] != 0:
                sizes[i][j] = 1. * sizes[i][j] / clustersCounts[i][j]
                supports[i][j] = 1. * supports[i][j] / clustersCounts[i][j]

    clustersCountsPct = [[1. * v / parameters["m"] for v in counts] for counts in clustersCounts]
    clusterOverlapsPct = [[1. * sum(cO) / len(cO) / len(clusters[i]) if len(cO) > 0 else 0 for cO in clusterOverlap] for i, clusterOverlap in enumerate(clusterOverlaps)]

    # drawClustersCounts(clustersCounts, plotFile="%s_counts.pdf" %(plotFile), draw=draw)
    # drawClustersCounts(clustersCountsPct, plotFile="%s_counts_pct.pdf" % (plotFile), draw=draw, pct=True)
    # drawClustersCounts(sizes, plotFile="%s_sizes.pdf" %(plotFile), draw=draw)
    # drawClustersCounts(supports, plotFile="%s_supports.pdf" %(plotFile), draw=draw)

    return {"clustersCounts": clustersCounts, "clustersCountsPct": clustersCountsPct, "sizes": sizes, "sizesArr": sizesArr, "clusterOverlapsPct": clusterOverlapsPct, "supports": supports}

def drawInfoGrouped(experiment, infoArr, key):
    for ic in range(0, len(infoArr[0]["data"][key])):
        expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
        figGroupedDir = "%s/figGrouped" % (expDir)

        plotFile = "%s/%s_test_%s_cluster_%d.pdf" %(figGroupedDir, key, infoArr[0]["test"], ic)

        x = [i["value"] for i in infoArr]

        for j in range(4, 11):
            y = []
            for ix, i in enumerate(x):
                y.append(infoArr[ix]["data"][key][ic][j])

            plt.plot(x, y, label="%d" %(j))

        plt.legend()
        plt.savefig(plotFile)
        plt.cla()
        plt.clf()

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key):
    for ic in range(0, len(infoArr[0]["data"][key])):
        expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
        figGroupedDir = "%s/figGrouped" % (expDir)

        plotFile = "%s/%s_test_%s_cluster_%d.pdf" %(figGroupedDir, key, infoArr[0]["test"], ic)

        x = [i["value"] for i in infoArr]

        for ji, j in enumerate(range(4, 11)):
            y = []
            for ix, i in enumerate(x):
                y.append(infoArr[ix]["data"][key][ic][j])

            bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ji * 0.3, widths=0.25)
            p = plt.plot([], label="%d" %(j))
            set_box_color(bp, p[0].get_color())

        plt.xticks(xrange(0, len(x) * 2, 2), x)
        plt.xlim(-1, len(x) * 2 + 1)
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
        plt.savefig(plotFile)
        plt.cla()
        plt.clf()


def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2 as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            infoArr.append({"test": rangedParam["name"], "value": v, "data": exp(sc, experiment, parameters)})

        drawInfoGrouped(experiment, infoArr, "clustersCountsPct")
        drawInfoGrouped(experiment, infoArr, "clusterOverlapsPct")
        drawInfoGrouped(experiment, infoArr, "sizes")
        drawInfoGroupedArr(experiment, infoArr, "sizesArr")


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    experiments(json.load(open(sys.argv[1])))
