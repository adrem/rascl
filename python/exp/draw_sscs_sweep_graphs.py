
import itertools, os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 20 })

DIR = "experiment_results_sscs_sweep"
DIRFIG = "experiment_results_sscs_sweep_fig"

def drawGraph(filename, data):
    for key in data:
        if key.endswith("params"):
            continue

        x = range(0, len(data[key]))
        y = data[key]

        plt.scatter(x, y, s=1)
        plt.ylim((0,1))
        plt.savefig("%s/%s/%s.pdf" % (DIRFIG, key, filename))
        plt.close()

def drawSingleProclusHeatmap(file, data):
    for key in data:
        if key.endswith("params"):
            continue

        xlabels = range(3, 42, 2) if file != "dimscale_d75" else range(3, 78, 4)
        xmlabels = [v for i, v in enumerate(xlabels) if i % 3 == 0]

        ylabels = range(2, 41, 2)
        ymlabels = [v for i, v in enumerate(ylabels) if i % 3 == 0]

        print(data[key])
        d = [data[key][i*20:(i+1)*20] for i in range(0, 20)] if file != "dimscale_d75" else [data[key][i*19:(i+1)*19] for i in range(0, 20)]
        print(d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel("l")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5, minor=True)
        ax.set_xticks(np.arange(len(xmlabels)) * 3 + 0.5, minor=False)
        ax.set_xticklabels(xmlabels, minor=False)

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5, minor=True)
        ax.set_yticks(np.arange(len(ymlabels)) * 3 + 0.5, minor=False)
        ax.set_yticklabels(ymlabels, minor=False)

        fig.tight_layout()
        plt.colorbar(heatmap)

        plt.savefig(os.path.join(DIRFIG, "proclus", key, "2d_%s.pdf" %(file)))
        plt.close()

def drawSingleDocHeatmap(file, data):
    for key in data:
        if key.endswith("params"):
            continue

        xlabels = [1. * v/100 for v in range(5, 100, 5)]
        xlabels[1] = ""
        xlabels[2] = ""
        xlabels[4] = ""
        xlabels[5] = ""
        xlabels[7] = ""
        xlabels[8] = ""
        xlabels[10] = ""
        xlabels[11] = ""
        xlabels[13] = ""
        xlabels[14] = ""
        xlabels[16] = ""
        xlabels[17] = ""
        ylabels = [1. * v/100 for v in range(5, 25, 1)]

        d = [data[key][i * 19:(i + 1) * 19] for i in range(0, 20)]
        print(data["object_precision_params"])
        print(d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel("l")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5)
        ax.set_xticklabels(xlabels)

        ax.set_ylabel("k")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5)
        ax.set_yticklabels(ylabels)

        fig.tight_layout()
        plt.colorbar(heatmap)

        plt.savefig(os.path.join(DIRFIG, "doc", key, "2d_%s.pdf" % (file)))
        plt.close()

def readData(file):
    data = {}

    with open(file) as f:

        for line in f:
            sp = line.strip().split(";")

            pKey = "%s_params" %(sp[0])

            if sp[0] not in data: data[sp[0]] = []
            if pKey not in data: data[pKey] = []

            data[sp[0]].append(float(sp[3]) if sp[3] != "nan" else 0)
            data[pKey].append(sp[1:3])

    return data

def drawGraphs():
    drawProclusGraphs()
    # drawDocGraphs()

def drawProclusGraphs():
    dir = os.path.join(DIR, "proclus")

    for file in os.listdir(dir):
        if file.endswith(".pdf") or file.startswith("."):
            continue

        print(file)

        d = readData(os.path.join(dir, file))

        drawSingleProclusHeatmap(file, d)

def drawDocGraphs():
    dir = os.path.join(DIR, "doc")

    for file in sorted(os.listdir(dir)):
        if file.endswith(".pdf") or file.startswith("."):
            continue

        d = readData(os.path.join(dir, file))

        drawSingleDocHeatmap(file, d)

def createDirs():
    for a in ["clique", "doc", "proclus", "p3c", "subclu"]:
        for d in [
            "object_f1",
            "object_f1P",
            "object_f1R",
            "object_precision",
            "object_recall",
            "dimension_f1",
            "dimension_f1P",
            "dimension_f1R",
            "dimension_precision",
            "dimension_recall",
            "sc_f1",
            "sc_f1P",
            "sc_f1R",
            "sc_precision",
            "sc_recall"
            "sc_recall",
            "e4sc_res"
        ]:
            dir = os.path.join(DIRFIG, a, d)

            if not os.path.exists(dir):
                os.makedirs(dir)

if __name__ == '__main__':
    createDirs()
    drawGraphs()