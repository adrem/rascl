
import pandas as pd

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.model_selection import KFold
from sklearn.tree import DecisionTreeClassifier

import sc2_v3 as sc

from clusterConverter import ClusterConverter
from dataStore import *

def normalClassification(dataKey):
    print ("RUNNING CLASSIFICATION EXPERIMENT")

    raw = pd.read_csv(DATA[dataKey][KEY_RAW_FILE], header=None)
    lab = pd.read_csv(DATA[dataKey][KEY_LABELS_FILE], names=["class"])

    kfold = KFold(n_splits=10, shuffle=True, random_state=100)

    allLabels = []
    allPredictions = []

    i = 0

    for ixTrain, ixTest in kfold.split(raw, lab):
        trainD = raw.iloc[ixTrain, :].values
        trainL = lab.iloc[ixTrain, 0].values

        testD = raw.iloc[ixTest, :].values
        testL = lab.iloc[ixTest, 0].values

        # clf = RandomForestClassifier(n_estimators=250, criterion="gini", max_depth=5, random_state=100)
        clf = DecisionTreeClassifier()

        clf.fit(trainD, trainL)

        predictions = clf.predict(testD)

        allLabels.extend(testL)
        allPredictions.extend(predictions)

        for avg in ["macro", "micro"]:
            print(i, avg, precision_score(testL, predictions, average=avg), recall_score(testL, predictions, average=avg), f1_score(testL, predictions, average=avg))

        i += 1

    print()

    for avg in ["macro", "micro"]:
        print(avg, precision_score(allLabels, allPredictions, average=avg), recall_score(allLabels, allPredictions, average=avg), f1_score(allLabels, allPredictions, average=avg))

def partOfCluster(ssc, data, testData, index):
    dimensions = [d for d in ssc.dimensions]

    d = data.iloc[[o for o in ssc.objects], dimensions]
    drow = testData.iloc[index, dimensions]

    maxs = d.max(0).values
    mins = d.min(0).values
    drowv = drow.values

    for i in range(0, d.shape[1]):
        if drowv[i] < mins[i] or drowv[i] > maxs[i]:
            return False

    return True

def enhancedClassification(dataKey, useOriginal=True):
    print ("RUNNING ENHANCED CLASSIFICATION EXPERIMENT [useOriginal=%s]" %(str(useOriginal)))

    raw = pd.read_csv(DATA[dataKey][KEY_RAW_FILE], header=None)
    lab = pd.read_csv(DATA[dataKey][KEY_LABELS_FILE], names=["class"])

    kfold = KFold(n_splits=10, shuffle=True, random_state=100)

    allLabels = []
    allPredictions = []

    fi = 0
    for ixTrain, ixTest in kfold.split(raw, lab):
        trainD = raw.iloc[ixTrain, :]
        trainL = lab.iloc[ixTrain, 0]

        trainDV = trainD.values

        samples, transactions, fimms, dims = sc.scDat("%s_train%d" %(dataKey, fi), trainD, 1000, 100, "weighted2_2", 10, "rmis|100", 100, None, 1, draw=False)

        cc = ClusterConverter(trainDV, {"prune": "False", "recomputeDimensions": "False", "dimensionsAlg": "alpha_0"})

        cFeatures = []

        sscs = [cc.convert(fimm, dims[i]) for i, fimm in enumerate(fimms)]
        sscs = sorted(sscs, key=lambda tuple: len(tuple.objects), reverse=True)[0:20]

        for i, row in enumerate(trainDV):
            cFeatures.append([1 if i in ssc.objects else 0 for ssc in sscs])

        cFD = pd.DataFrame(cFeatures, index=trainD.index)

        trainDE = pd.concat([trainD, cFD], axis=1) if useOriginal else cFD

        testD = raw.iloc[ixTest, :]
        testL = lab.iloc[ixTest, 0]

        testDV = testD.values

        ccFeatures = []

        for i in range(0, testDV.shape[0]):
            ccFeatures.append([1 if partOfCluster(ssc, trainD, testD, i) else 0 for ssc in sscs])

        ccFD = pd.DataFrame(ccFeatures, index=testD.index)

        testDE = pd.concat([testD, ccFD], axis=1) if useOriginal else ccFD

        # clf = RandomForestClassifier(n_estimators=250, criterion="gini", max_depth=5, random_state=100)
        clf = DecisionTreeClassifier()

        clf.fit(trainDE.values, trainL.values)

        predictions = clf.predict(testDE.values)

        allLabels.extend(testL.values)
        allPredictions.extend(predictions)

        for avg in ["macro", "micro"]:
            print(fi, avg, precision_score(testL.values, predictions, average=avg), recall_score(testL.values, predictions, average=avg), f1_score(testL.values, predictions, average=avg))

        fi += 1

    print()

    for avg in ["macro", "micro"]:
        print(avg, precision_score(allLabels, allPredictions, average=avg), recall_score(allLabels, allPredictions, average=avg), f1_score(allLabels, allPredictions, average=avg))

if __name__ == '__main__':
    dataKey = "pendigits"

    normalClassification(dataKey)
    enhancedClassification(dataKey, useOriginal=False)
    enhancedClassification(dataKey)
