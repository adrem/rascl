#!/usr/bin/env python

import itertools
import os
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams.update({'font.size': 14 })

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from colors import tableau20

strip_first_ellipsis = os.getcwd().endswith("exp")

if strip_first_ellipsis:
    os.chdir("..")

"""
    This script produces object comparison graphs between two algorithm runs
"""

DATASET = ""
DATASETS = []

ALGOS = []
DIR = ""
DIRFIG = ""

TEST = ""
KEY = ""

DATA_ALGOS = []

def reset():
    global DATASET, DATASETS, ALGOS, DIR, DIRFIG, TEST, KEY, DATA_ALGOS
    DATASET = ""
    DATASETS = []

    ALGOS = []
    DIR = ""
    DIRFIG = ""

    TEST = ""
    KEY = ""

    DATA_ALGOS = []

""" Read local constants """
def readConfFile(confFile):
    if os.path.isfile(confFile):
        with open(confFile) as f:
            for l in f:
                line = l.strip()
                if line and line[0] != "#":
                    exec(line, globals())

def readData(dir_sc):
    global DATASET

    d = {}

    for file in os.listdir(dir_sc):
        if not file.startswith(DATASET) or file.endswith("xlsx"):
            continue

        if file not in d:
            d[file] = {}

        data = d[file]

        with open("%s/%s" %(dir_sc, file)) as f:
            for line in f:
                sp = line.strip().split(" ")
                sp1 = sp[0].strip(".pdf").split("_")

                values = [float(value) for value in sp[1:]]
                value = sum(values) / len(values)

                ps = "%s_%s" %(sp1[4], sp1[5])
                me = sp1[2].replace("-", "_").replace("_res", "")
                dd = sp1[10].split("|")[1] if sp1[7] == "pruneTrue" else sp1[9].split("|")[1]

                if sp1[3] not in data: data[sp1[3]] = {}
                if ps not in data[sp1[3]]: data[sp1[3]][ps] = {}
                if me not in data[sp1[3]][ps]: data[sp1[3]][ps][me] = {}

                data[sp1[3]][ps][me][dd] = value

    return d

def readDataCarti(dir_sc):
    global DATASET

    d = {}

    for file in os.listdir(dir_sc):
        if not file.startswith(DATASET) or file.endswith("xlsx"):
            continue

        if file not in d:
            d[file] = {}

        data = d[file]

        with open("%s/%s" %(dir_sc, file)) as f:
            for line in f:
                sp = line.strip().split(" ")

                values = [float(value) for value in sp[1:]]
                value = sum(values) / len(values)

                data[sp[0].replace("_res", "")] = value

    return d

def createDirs():
    global DIRFIG

    if not os.path.exists(DIRFIG):
        os.makedirs(DIRFIG)

def readDataFromDirs():
    global ALGOS, DATA_ALGOS

    for i, algo in enumerate(ALGOS):
        if algo[2] == "sc":
            DATA_ALGOS.append(readData(algo[1]))
        elif algo[2] in ["carticlus", "proclus" ]:
            DATA_ALGOS.append(readDataCarti(algo[1]))
        else:
            DATA_ALGOS.append({})

def iterateAll():
    for p in itertools.product(
            ["topk10"],
            ["patternSorterobject_count"],
            ["object_precision", "object_recall", "dimension_precision", "dimension_recall"]
    ):
        yield p

def drawScaleGraphs(fileKeys):
    global ALGOS, DATA_ALGOS, KEY, KEYS, DIRFIG, TEST

    keys = [algo[0] for algo in ALGOS]

    width = 0.7 / (len(keys))

    for p in iterateAll():
        fig, ax = plt.subplots()

        rects = []
        bar_cycle = (matplotlib.cycler('color', tableau20)[0:16] + matplotlib.cycler('hatch', ['///', '---', '...', '\/\/', 'xxx', '\\\\', '***', '+++', '/', '-', '.', '\/', 'x', '\\', '**', '++']))
        styles = bar_cycle()

        for i, algo in enumerate(ALGOS):
            if algo[2] == "sc":
                d = [(DATA_ALGOS[i][fileKey][p[0]][p[1]][p[2]][KEY] if fileKey in DATA_ALGOS[i] and p[0] in DATA_ALGOS[i][fileKey] and p[1] in DATA_ALGOS[i][fileKey][p[0]] and p[2] in DATA_ALGOS[i][fileKey][p[0]][p[1]] and KEY in DATA_ALGOS[i][fileKey][p[0]][p[1]][p[2]] else 0) for fileKey in fileKeys]
            elif algo[2] in ["carticlus", "proclus"]:
                d = [(DATA_ALGOS[i][fileKey][p[2]] if fileKey in DATA_ALGOS[i] else 0) for fileKey in fileKeys]

            ind = np.arange(len(fileKeys))
            rects.append(ax.bar(ind + i * width, d, width*0.8, **next(styles)))

        ax.set_ylabel("Score")
        ax.set_xticks(ind + (width * (len(keys) - 1) / 2))
        ax.set_xticklabels([fileKey.split("_")[1] for fileKey in fileKeys])

        if len(ALGOS) <= 6:
            ax.legend([rect[0] for rect in rects], keys, loc='upper center', bbox_to_anchor=(0.484, -0.1), ncol=3)
        else:
            ax.legend([rect[0] for rect in rects], keys, loc='upper center', bbox_to_anchor=(0.484, -0.1), ncol=2)

        plt.ylim((0,1))

        if len(ALGOS) <= 6:
            plt.subplots_adjust(bottom=0.3)
        else:
            plt.subplots_adjust(bottom=0.4)
        plt.savefig("%s/%s_%s.pdf" %(DIRFIG, p[1], p[2]))
        plt.close()

def drawGraphs():
    drawScaleGraphs(["dbsizescale_s1500", "dbsizescale_s2500", "dbsizescale_s3500", "dbsizescale_s4500", "dbsizescale_s5500"])
    drawScaleGraphs(["dimscale_d05", "dimscale_d10", "dimscale_d25", "dimscale_d50", "dimscale_d75"])#, "dimscale_d15", "dimscale_d20", "dimscale_d25", "dimscale_d50", "dimscale_d75"])
    drawScaleGraphs(["noisescale_n10", "noisescale_n30", "noisescale_n50", "noisescale_n70"])

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Please specify: (comma separated confFile)")
        exit(1)

    for confFile in sys.argv[1:]:
        print(confFile)
        reset()
        readConfFile(confFile)
        createDirs()
        readDataFromDirs()
        drawScaleGraphs(DATASETS)
        # drawGraphs()