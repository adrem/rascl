
import itertools, json, math, os.path, pickle, sys

from matplotlib import pyplot as plt

from clusterConverter import ClusterConverter
from dataStore import *
from dataUtils import *
from measures import *
from patternSortingStrategy import *

KEY_FIMMS = "fimms"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

OUTPUTDIR = "experiment_results_dimension_strategy"

prune = True

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v)-1 for v in sp[1].split(" ")]))

def smallOverlap(newCluster, existingClusters, maxOverlap=0.25):
    for existingCluster in existingClusters:
        if precision(newCluster[0], existingCluster[0]) > maxOverlap:
            return False

    return True

def cluster_dimension_precision(cluster, clusters):
    print(cluster.dimensions)
    for c in clusters:
        print(c.dimensions, len(c.objects), c.objects)
    return [dimension_precision(c, cluster) for c in clusters]

def drawFimm(rows, fimm):
    ddddimmms = fimm.dimensions

    d = {i: [] for i in range(0, len(rows[0]))}
    for i in ddddimmms:
        d["_%d" % i if i > 10 else "_0%d" % i] = []
    d['_-1'] = []
    d['cluster'] = []

    dc = {i: [] for i in range(0, len(rows[0]))}
    for i in ddddimmms:
        dc["_%d" % i if i > 10 else "_0%d" % i] = []
    dc['_-1'] = []
    dc['cluster'] = []

    for i, row in enumerate(rows):
        for dim in ddddimmms:
            if i in fimm.objects:
                dc["_%d" % dim if dim > 10 else "_0%d" % dim].append(row[dim])
            else:
                d["_%d" % dim if dim > 10 else "_0%d" % dim].append(row[dim])
        for dim in range(0, len(rows[0])):
            if i in fimm.objects:
                dc[dim].append(row[dim])
            else:
                d[dim].append(row[dim])

        if i in fimm.objects:
            dc["_-1"].append(0)
            dc['cluster'].append('yes')
        else:
            d["_-1"].append(0)
            d['cluster'].append('no')

    import pandas as pd
    from pandas.plotting import parallel_coordinates

    parallel_coordinates(pd.DataFrame(data=d), 'cluster', color=['b'])
    parallel_coordinates(pd.DataFrame(data=dc), 'cluster', color=['r'])

    plt.show()

def exp(experiment, parametersCC, evaluation):
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    clusterConverter = ClusterConverter(rows, parametersCC)

    evaluatedClusters = []

    for cluster in clusters:
        fimm = clusterConverter.convert(cluster.objects, [])

        vv = [(i, e4sc_precision(fimm, cluster)) for i, cluster in enumerate(clusters)]
        vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

        closest = vv[0]

        print(vv)

        evaluatedClusters.append((fimm, closest))

    topKClustersGrouped = {i: [] for i in range(0, len(clusters))}

    for cluster in evaluatedClusters:
        topKClustersGrouped[cluster[1][0]].append(cluster[0])

    data = {i: [] for i, cluster in enumerate(clusters)}
    data["all"] = []

    evaluator = MEASURES[evaluation]

    for key in topKClustersGrouped:
        for cluster in topKClustersGrouped[key]:
            eval = evaluator(cluster, clusters[key])
            print(cluster.dimensions)
            print(clusters[key].dimensions)
            print(evaluation, eval)
            data[key].append(eval)
            data["all"].append(eval)

    return data

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

import numpy
w = None

def drawInfoGroupedArr(experiment, infoArr, parametersEX, parametersSC, parametersCC, evaluation):
    global w

    for i in range(0, len(infoArr)):
        w.write("test_%s%s_%s_topk%d_patternSorter%s_overlap%.2f_prune%s_recomputeDimensions%s_dimensionsAlg%s.pdf" % (infoArr[i]["test"], "%d" %(infoArr[i]["value"]) if isinstance(infoArr[i]["value"], int) else infoArr[i]["value"], evaluation, parametersEX["topk"], parametersEX["patternSorter"], parametersEX["overlap"], str(parametersCC["prune"]), str(parametersCC["recomputeDimensions"]), parametersCC["dimensionsAlg"]))
        w.write(" ")
        w.write("%.2f" %(numpy.average(infoArr[i]["data"]["all"])))
        w.write("\n")
        w.flush()

    # expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    # figGroupedDir = "%s/figGrouped" % (expDir)
    #
    # plotFile = "%s/test_%s_%s_topk%d_patternSorter%s_overlap%.2f_prune%s_recomputeDimensions%s_dimensionsAlg%s.pdf" %(figGroupedDir, infoArr[0]["test"], evaluation, parametersEX["topk"], parametersEX["patternSorter"], parametersEX["overlap"], str(parametersCC["prune"]), str(parametersCC["recomputeDimensions"]), parametersCC["dimensionsAlg"])
    #
    # x = [i["value"] for i in infoArr]
    # print infoArr
    # print x
    # for ic, key in enumerate(sorted(infoArr[0]["data"].keys())):
    #     y = []
    #     for ix, i in enumerate(x):
    #         y.append(infoArr[ix]["data"][key] if key in infoArr[ix]["data"] else [])
    #
    #     bp = plt.boxplot(y, positions=np.array(xrange(len(y))) * 2.0 + 1. * ic / (len(y)+1) + 0.1, widths=0.9/(len(y)+1))
    #     p = plt.plot([], label="%s" %("%d" %key if isinstance(key, int) else key))
    #     set_box_color(bp, p[0].get_color())
    #
    # plt.xticks(xrange(0, len(x) * 2, 2), x)
    # plt.xlim(-1, len(x) * 2 + 1)
    # if evaluation == "object_size":
    #     plt.ylim(0, 405)
    # else:
    #     plt.ylim(0, 1.05)
    # plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    # plt.savefig(plotFile)
    # plt.cla()
    # plt.clf()

def _splitParameters(parameters):
    rangedParameters = []
    fixedParameters = []

    for parameter in parameters:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
            parameter["values"] = range(parameter["range"]["start"], parameter["range"]["end"], parameter["range"]["step"])
        elif parameter["type"] == "values":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def _flattenParameters(rangedParameters, fixedParameters):
    parametersList = [[(rangedParameter["name"], value) for value in rangedParameter["values"]] for rangedParameter in rangedParameters]
    parametersList.extend([[(fixedParameter["name"], fixedParameter["value"])] for fixedParameter in fixedParameters])

    for parameters in itertools.product(*parametersList):
        yield {parameter[0]: parameter[1] for parameter in parameters}

def _fillDefaultParametersEX(parameters):
    if "topk" not in parameters: parameters["topk"] = 10
    if "patternSorter" not in parameters: parameters["patternSorter"] = "object_count"
    if "overlap" not in parameters: parameters["overlap"] = 0.25

def _fillDefaultParametersSC(parameters):
    if "n" not in parameters: parameters["n"] = 1000
    if "k" not in parameters: parameters["k"] = 200
    if "kk" not in parameters: parameters["kk"] = 5
    if "s" not in parameters: parameters["s"] = 100
    if "m" not in parameters: parameters["m"] = 1000
    if "sampleDimCount" not in parameters: parameters["sampleDimCount"] = "variable"

def _fillDefaultParametersCC(parameters):
    if "prune" not in parameters: parameters["prune"] = "True"
    if "recomputeDimensions" not in parameters: parameters["recomputeDimensions"] = "True"
    if "dimensionsAlg" not in parameters: parameters["dimensionsAlg"] = "stdev_based2_0.04"

def _createDirs():
    if not os.path.isdir(OUTPUTDIR):
        os.makedirs(OUTPUTDIR)

def _shouldPrune(parameters):
    for parameter in parameters:
        if parameter["name"] == "prune" and parameter["value"] == "True":
            return True

    return False

def experiments(experiments):
    global w

    for expKey in experiments["subspaceCluster"]:
        file = "%s/%s%s" % (OUTPUTDIR, experiments["subspaceCluster"][expKey]["dataKey"], "_prune" if _shouldPrune(experiments["clusterConverter"]["parameters"]) else "")

        # if os.path.isfile(file):
        #     print("File exists. Skipping. (%s)" % (file))
        #     continue

        w = open(file, "w")

        rangedParametersEX, fixedParametersEX = _splitParameters(experiments["experiment"]["parameters"])
        rangedParametersCC, fixedParametersCC = _splitParameters(experiments["clusterConverter"]["parameters"])

        for evaluation in experiments["evaluation"]:

            experiment = experiments["subspaceCluster"][expKey]

            for parametersEX in _flattenParameters(rangedParametersEX, fixedParametersEX):
                _fillDefaultParametersEX(parametersEX)

                for parametersCC in _flattenParameters(rangedParametersCC, fixedParametersCC):
                    _fillDefaultParametersCC(parametersCC)

                    rangedParametersSC, fixedParametersSC = _splitParameters(experiment["parameters"])

                    if len(rangedParametersSC) != 1:
                        print ("Cannot handle ranged params of length %d" %(len(rangedParametersSC)))
                        break

                    rangedParameterSC = rangedParametersSC[0]

                    infoArr = []

                    for v in rangedParameterSC["values"]:
                        parametersSC = {rangedParameterSC["name"]: v}

                        for param in fixedParametersSC:
                            parametersSC[param["name"]] = param["value"]

                        _fillDefaultParametersSC(parametersSC)

                        infoArr.append({"test": rangedParameterSC["name"], "value": v, "data": exp(experiment, parametersCC, evaluation)})

                    drawInfoGroupedArr(experiment, infoArr, parametersEX, parametersSC, parametersCC, evaluation)

        w.close()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    _createDirs()
    experiments(json.load(open(sys.argv[1])))
