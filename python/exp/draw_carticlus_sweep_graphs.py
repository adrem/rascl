
import os, sys

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 20 })


def drawSingleHeatmap(file, data, output_dir):
    for key in data:
        if not os.path.exists(os.path.join(output_dir, key)):
            os.makedirs(os.path.join(output_dir, key))

        if key.endswith("params"):
            continue

        xlabels = [param[1] for param in data["%s_params" %(key)][0:11]]
        xmlabels = [v for i, v in enumerate(xlabels) if i % 3 == 0]

        ylabels = [data["%s_params" %(key)][i*11][0] for i in range(0, 10)]
        ymlabels = [v for i, v in enumerate(ylabels) if i % 3 == 0]

        d = [data[key][i*11:(i+1)*11] for i in range(0, 10)]

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap="gist_rainbow", vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)


        ax.set_xlabel(r"$\sigma$")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5, minor=True)
        ax.set_xticks(np.arange(len(xmlabels)) * 3 + 0.5, minor=False)
        ax.set_xticklabels(xmlabels, minor=False)

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5, minor=True)
        ax.set_yticks(np.arange(len(ymlabels)) * 3 + 0.5, minor=False)
        ax.set_yticklabels(ymlabels, minor=False)

        fig.tight_layout()
        plt.colorbar(heatmap)

        plt.savefig(os.path.join(os.path.join(output_dir, key, "2d_%s.pdf" %(file))))
        plt.close()


def readData(file):
    data = {}

    with open(file) as f:

        for line in f:
            sp = line.strip().split(";")

            pKey = "%s_params" %(sp[0])

            if sp[0] not in data: data[sp[0]] = []
            if pKey not in data: data[pKey] = []

            data[sp[0]].append(float(sp[3]) if sp[3] != "nan" else 0)
            data[pKey].append(sp[1:3])

    return data


def drawGraphs(input_dir, output_dir):
    for file in os.listdir(input_dir):
        if file.endswith(".pdf") or file.startswith("."):
            continue

        drawSingleHeatmap(file, readData(os.path.join(input_dir, file)), output_dir)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Please specify a directory with CartiClus sweep output and an output directory")
        exit(0)

    drawGraphs(sys.argv[1], sys.argv[2])