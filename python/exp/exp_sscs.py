#!/usr/bin/env python

"""
    Experiment with various subspace clustering techniques on synthetic data.

    Runs scaling experiments on synthetic data using ProClus and other methods. Three different types of synthetic data
    are used: 1) increasing data size, 2) increasing dimension size, 3) increasing noise level.
"""

import os, pickle, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import get_cluster
from measures import *

from subspace_clustering import *

if os.getcwd().endswith("exp"):
    os.chdir("..")

OUTPUTDIR = os.path.join("experiment_results", "comparisonQuality")


evaluators = [
    "object_precision_res",
    "object_recall_res",
    "dimension_precision_res",
    "dimension_recall_res",
    "sc_f1P_res",
    "sc_f1R_res",
    "e4sc_res"
]

def runProClus():
    print ("RUNNING PROCLUS")

    for file in [
        ("dbsizescale_s1500", 10, 14),
        ("dbsizescale_s2500", 10, 14),
        ("dbsizescale_s3500", 10, 14),
        ("dbsizescale_s4500", 10, 14),
        ("dbsizescale_s5500", 10, 14),
        ("dimscale_d05", 10, 4),
        ("dimscale_d10", 10, 7),
        ("dimscale_d25", 10, 17),
        ("dimscale_d50", 10, 34),
        ("dimscale_d75", 10, 50),
        ("noisescale_n10", 10, 14),
        ("noisescale_n30", 10, 14),
        ("noisescale_n50", 10, 14),
        ("noisescale_n70", 10, 14),
    ]:
        f = "%s/proclus/%s" %(OUTPUTDIR, file[0])

        if os.path.isfile(f):
            print("File exists. Skipping. (%s)" % (f))
            continue

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file[0], file[0]))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file[0], file[0]))

        scoreMap = {evaluator: [] for evaluator in evaluators}

        for run in range(1, 11):
            pickleFile = "%s/proclus/cache/%s_run%d_k%d_l%d" %(OUTPUTDIR, file[0], run, file[1], file[2])

            if os.path.isfile(pickleFile):
                cclusters = pickle.load(open(pickleFile))
            else:
                rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

                cclusters = proclus(rows, k=file[1], l=file[2])

                pickle.dump(cclusters, open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

            clusters = [get_cluster(row) for row in open(clusters_file)]

            for evaluator in evaluators:
                scoreMap[evaluator].append(MEASURES[evaluator](cclusters, clusters))

        w = open(f, "w")

        for evaluator in evaluators:
            w.write("%s %s\n" %(evaluator, " ".join(["%.5f" %(score) for score in scoreMap[evaluator]])))

        w.close()

if __name__ == '__main__':
    for a in ["proclus", "subclu", "clique", "doc", "p3c"]:
        if not os.path.exists("%s/%s" %(OUTPUTDIR, a)):
            os.makedirs("%s/%s" %(OUTPUTDIR, a))

    for a in ["proclus", "subclu", "clique", "doc", "p3c"]:
        if not os.path.exists("%s/%s/cache" % (OUTPUTDIR, a)):
            os.makedirs("%s/%s/cache" % (OUTPUTDIR, a))

    runProClus()
