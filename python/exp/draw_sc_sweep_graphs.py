
import os, sys

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 20 })

def _createDir(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)


def drawSingleHeatmap(file, data, output_dir):
    print(file)
    for key in data.keys():
        if key.endswith("params"):
            continue

        k1 = 19
        k2 = 19

        ylabels = [data["%s_params" %(key)][i*k1][0].replace("kk_", "").replace("k_", "") for i in range(0, k2)]
        ymlabels = [v for i, v in enumerate(ylabels) if i % 3 == 0]

        xlabels = [param[1].split("|")[-1].replace("s_", "") for param in data["%s_params" %(key)][0:k1]]
        xmlabels = [v for i, v in enumerate(xlabels) if i % 3 == 0]

        d = [data[key][i*k1:(i+1)*k1] for i in range(0, k2)]

        print("ToDraw: %s:" %(key), d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel(r"$\sigma$")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5, minor=True)
        ax.set_xticks(np.arange(len(xmlabels)) * 3 + 0.5, minor=False)
        ax.set_xticklabels(xmlabels, minor=False)

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5, minor=True)
        ax.set_yticks(np.arange(len(ymlabels)) * 3 + 0.5, minor=False)
        ax.set_yticklabels(ymlabels, minor=False)

        fig.tight_layout()
        plt.colorbar(heatmap)

        _createDir(os.path.join(output_dir, key))

        file_name = os.path.join(output_dir, key, "prune_2d_%s.pdf" %(file.replace("prune", "")))

        if file.endswith("prune"):
            plt.savefig(file_name)
        else:
            plt.savefig(file_name)
            print(file_name)

        plt.close()

def drawSingleHeatmap2(file, data, output_dir):
    print(file)
    for key in data.keys():
        if key.endswith("params"):
            continue

        k1 = 10
        k2 = 10

        ylabels = [data["%s_params" %(key)][i*k1][0] for i in range(0, k2)]
        xlabels = [param[1].split("|")[-1] for param in data["%s_params" %(key)][0:k1]]

        for i in range(1, k1):
            if i % 3 != 0:
                xlabels[i] = ""

        for i in range(1, k2):
            if i % 3 != 0:
                ylabels[i] = ""

        d = [data[key][i*k1:(i+1)*k1] for i in range(0, k2)]

        print("ToDraw: %s:" %(key), d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel(r"$\sigma$")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5)
        ax.set_xticklabels([xlabel.replace("s_", "") for xlabel in xlabels])

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5)
        ax.set_yticklabels([ylabel.replace("kk_", "").replace("k_", "") for ylabel in ylabels])

        fig.tight_layout()
        plt.colorbar(heatmap)

        _createDir("%s/%s" %(output_dir, key))

        file_name = os.path.join(output_dir, key, "prune_2d_%s.pdf" %(file.replace("prune", "")))

        if file.endswith("prune"):
            plt.savefig(file_name)
        else:
            plt.savefig(file_name)
            print(file_name)

        plt.close()

def drawSingleHeatmap3(file, data, output_dir):
    print(file)
    for key in data.keys():
        if key.endswith("params"):
            continue

        k1 = 7
        k2 = 9

        ylabels = [data["%s_params" %(key)][i*k1][0] for i in range(0, k2)]
        xlabels = [param[1].split("|")[-1] for param in data["%s_params" %(key)][0:k1]]

        for i in range(1, k1):
            if i % 3 != 0:
                xlabels[i] = ""

        for i in range(1, k2):
            if i % 3 != 0:
                ylabels[i] = ""

        d = [data[key][i*k1:(i+1)*k1] for i in range(0, k2)]

        print("ToDraw: %s:" %(key), d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel(r"$\sigma$")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5)
        ax.set_xticklabels([xlabel.replace("s_", "") for xlabel in xlabels])

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5)
        ax.set_yticklabels([ylabel.replace("kk_", "").replace("k_", "") for ylabel in ylabels])

        fig.tight_layout()
        plt.colorbar(heatmap)

        file_name = os.path.join(output_dir, key, "prune_2d_%s.pdf" %(file.replace("prune", "")))

        if file.endswith("prune"):
            plt.savefig(file_name)
        else:
            plt.savefig(file_name)
            print(file_name)

        plt.close()


def drawSingleHeatmap4(file, data, output_dir):
    print(file)
    for key in data.keys():
        if key.endswith("params"):
            continue

        k1 = 10
        k2 = 15

        ylabels = [data["%s_params" %(key)][i*k1][0] for i in range(0, k2)]
        xlabels = [param[1].split("|")[-1] for param in data["%s_params" %(key)][0:k1]]

        for i in range(1, k1):
            if i % 3 != 0:
                xlabels[i] = ""

        for i in range(1, k2):
            if i % 3 != 0:
                ylabels[i] = ""

        d = [data[key][i*k1:(i+1)*k1] for i in range(0, k2)]

        print("ToDraw: %s:" %(key), d)

        fig, ax = plt.subplots()
        # heatmap = ax.pcolor(d, cmap=plt.cm.RdYlBu, vmin=0, vmax=1)
        heatmap = ax.pcolor(d, cmap=plt.cm.Blues, vmin=0, vmax=1)

        ax.set_xlabel(r"$\sigma$")
        ax.set_xticks(np.arange(len(xlabels)) + 0.5)
        ax.set_xticklabels([xlabel.replace("s_", "") for xlabel in xlabels])

        ax.set_ylabel("K")
        ax.set_yticks(np.arange(len(ylabels)) + 0.5)
        ax.set_yticklabels([ylabel.replace("kk_", "").replace("k_", "") for ylabel in ylabels])

        fig.tight_layout()
        plt.colorbar(heatmap)

        _createDir("%s/%s" %(output_dir, key))

        file_name = os.path.join(output_dir, key, "prune_2d_%s.pdf" %(file.replace("prune", "")))

        if file.endswith("prune"):
            plt.savefig(file_name)
        else:
            plt.savefig(file_name)
            print(file_name)

        plt.close()


def readData(file):
    data = {}

    with open(file) as f:

        for line in f:
            sp = line.strip().split(";")

            pKey = "%s_params" %(sp[0])

            if sp[0] not in data: data[sp[0]] = []
            if pKey not in data: data[pKey] = []

            data[sp[0]].append(np.mean([float(s) for s in sp[3].split(" ")]) if not "nan" in sp[3] else 0)
            data[pKey].append(sp[1:3])

    return data


def drawGraphs(input_dir, output_dir):
    _createDir(output_dir)

    for file in sorted(os.listdir(input_dir)):
        if file.endswith(".pdf") or file.startswith(".") or os.path.isdir("%s/%s" %(input_dir, file)):
            continue

        if not file == "dbsizescale_s1500": continue

        try:
            d = readData(os.path.join(input_dir, file))
            print(d)

            if file in ["glass", "liver", "breast"]:
                drawSingleHeatmap2(file, d, output_dir)
            elif file in ["iris"]:
                drawSingleHeatmap3(file, d, output_dir)
            elif file in ["pendigits"]:
                drawSingleHeatmap4(file, d, output_dir)
            else:
                drawSingleHeatmap(file, d, output_dir)

        except Exception as e:
            print(e)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Please specify a directory with RASCL or RASCLR sweep output and an output directory")
        exit(0)

    drawGraphs(sys.argv[1], sys.argv[2])