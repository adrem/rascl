
import itertools, os, sys

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import get_cluster
from measures import *

from subspace_clustering import *

if os.getcwd().endswith("exp"):
    os.chdir("..")

OUTPUTDIR = os.path.join("experiment_results", "sweepExperiment")


def writeScores(w, mclusters, clusters, ps):
    w.write("object_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], object_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], object_f1R_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_precision;%d;%d;%.5f\n" % (ps[0], ps[1], object_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("object_recall;%d;%d;%.5f\n" % (ps[0], ps[1], object_recall_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_f1R_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_precision;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("dimension_recall;%d;%d;%.5f\n" % (ps[0], ps[1], dimension_recall_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_f1P;%d;%d;%.5f\n" % (ps[0], ps[1], sc_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_f1R;%d;%d;%.5f\n" % (ps[0], ps[1], sc_f1P_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_precision;%d;%d;%.5f\n" % (ps[0], ps[1], sc_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("sc_recall;%d;%d;%.5f\n" % (ps[0], ps[1], sc_precision_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.write("e4sc_res;%d;%d;%.5f\n" % (ps[0], ps[1], e4sc_res(mclusters, clusters) if len(mclusters) > 0 else -1))
    w.flush()


def runProClus():
    print ("RUNNING PROCLUS")

    for params in [
        # "dbsizescale_s1500",
        # "dbsizescale_s2500",
        # "dbsizescale_s3500",
        # "dbsizescale_s4500",
        # "dbsizescale_s5500",
        # "dbsizescale_s5500",
        ["dimscale_d05", range(2, 41, 2), range(3, 42, 2)],
        # "dimscale_d10",
        # "dimscale_d15",
        # "dimscale_d20",
        ["dimscale_d25", range(2, 41, 2), range(3, 42, 2)],
        # "dimscale_d50",
        ["dimscale_d75", range(2, 41, 2), range(3, 78, 4)]
        # "noisescale_n10",
        # "noisescale_n30",
        # "noisescale_n50",
        # "noisescale_n70",
    ]:

        file = params[0]
        ks = params[1]
        ls = params[2]

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/proclus/%s" %(OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = proclus(rows, k=ps[0], l=ps[1])

            gclusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, mclusters, gclusters, ps)

        w.close()

def runProClusRW():
    print ("RUNNING PROCLUS RW")

    for file in [
        # "glass",
        "pendigits",
    ]:

        ks = range(2, 41, 2)
        ls = range(3, 42, 2)

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/proclus/%s" %(OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = proclus(rows, k=ps[0], l=ps[1])

            gclusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, mclusters, gclusters, ps)

        w.close()

def runDOC():
    print ("RUNNING DOC")

    for file in [
        "dbsizescale_s1500",
        "dbsizescale_s2500",
        "dbsizescale_s3500",
        "dbsizescale_s4500",
        "dbsizescale_s5500",
        "dbsizescale_s5500",
        "dimscale_d05",
        "dimscale_d10",
        "dimscale_d15",
        "dimscale_d20",
        "dimscale_d25",
        "dimscale_d50",
        "dimscale_d75",
        "noisescale_n10",
        "noisescale_n30",
        "noisescale_n50",
        "noisescale_n70",
    ]:

        ks = [1. * v/100 for v in range(5, 25, 1)]
        ls = [1. * v/100 for v in range(5, 100, 5)]

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/doc/%s" %(OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = doc(rows, alpha=ps[0], beta=ps[1], w=70)

            gclusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, mclusters, gclusters, ps)

        w.close()

def runP3C():
    print ("RUNNING P3C")

    for file in [
        "dbsizescale_s1500",
        "dbsizescale_s2500",
        "dbsizescale_s3500",
        "dbsizescale_s4500",
        "dbsizescale_s5500",
        "dbsizescale_s5500",
        "dimscale_d05",
        "dimscale_d10",
        "dimscale_d15",
        "dimscale_d20",
        "dimscale_d25",
        "dimscale_d50",
        "dimscale_d75",
        "noisescale_n10",
        "noisescale_n30",
        "noisescale_n50",
        "noisescale_n70",
    ]:

        ks = [0.0001 * v for v in range(5, 100, 5)]
        ls = [0.00001 * v for v in range(5, 100, 5)]

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/p3c/%s" %(OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = p3c(rows, alpha=ps[0], threshold=ps[1])

            gclusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, mclusters, gclusters, ps)

        w.close()

def runSUBCLU():
    print ("RUNNING SUBCLU")

    for file in [
        "dbsizescale_s1500",
        "dbsizescale_s2500",
        "dbsizescale_s3500",
        "dbsizescale_s4500",
        "dbsizescale_s5500",
        "dbsizescale_s5500",
        "dimscale_d05",
        "dimscale_d10",
        "dimscale_d15",
        "dimscale_d20",
        "dimscale_d25",
        "dimscale_d50",
        "dimscale_d75",
        "noisescale_n10",
        "noisescale_n30",
        "noisescale_n50",
        "noisescale_n70",
    ]:

        ks = [1. * v/100 for v in range(5, 100, 5)]
        ls = range(50, 500, 50)

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/subclu/%s" %(OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = subclu(rows, epsilon=ps[0], minpts=ps[1])

            gclusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, mclusters, gclusters, ps)

        w.close()

def runCLIQUE():
    print ("RUNNING CLIQUE")

    for file in [
        "dbsizescale_s1500",
        "dbsizescale_s2500",
        "dbsizescale_s3500",
        "dbsizescale_s4500",
        "dbsizescale_s5500",
        "dbsizescale_s5500",
        "dimscale_d05",
        "dimscale_d10",
        "dimscale_d15",
        "dimscale_d20",
        "dimscale_d25",
        "dimscale_d50",
        "dimscale_d75",
        "noisescale_n10",
        "noisescale_n30",
        "noisescale_n50",
        "noisescale_n70",
    ]:

        ks = range(2, 21, 2)
        ls = [1. * v/100 for v in range(5, 100, 5)]

        data_file = os.path.join("data", "%s/%s.raw.txt" %(file, file))
        clusters_file = os.path.join("data", "%s/%s.clusters.txt" %(file, file))

        w = open("%s/clique/%s" % (OUTPUTDIR, file), "w")

        for ps in itertools.product(ks, ls):
            print(ps)

            rows = [[float(v) for v in row.split(",")] for row in open(data_file)]

            mclusters = clique(rows, xsi=ps[0], tau=ps[1])

            clusters = [get_cluster(row) for row in open(clusters_file)]

            writeScores(w, clusters, mclusters, ps)

        w.close()

if __name__ == '__main__':
    for a in ["proclus", "subclu", "clique", "doc", "p3c"]:
        if not os.path.exists("%s/%s" %(OUTPUTDIR, a)):
            os.makedirs("%s/%s" %(OUTPUTDIR, a))

    runProClus()
    # runDOC()
    # runP3C()
    # runCLIQUE()
    # runSUBCLU()

    # runProClusRW()
