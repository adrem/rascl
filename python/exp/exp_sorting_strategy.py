
import json, os.path, pickle, sys

import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 16 })

import pandas as pd

if os.getcwd().endswith("exp"):
    sys.path.insert(0, '..')
else:
    sys.path.insert(0, '.')

from exp_runner_utils import fill_default_parameters_cc, fill_default_parameters_ex, fill_default_parameters_rascl, flatten_parameters, get_cluster, split_parameters
from clusterConverter import ClusterConverter
from colors import tableau20
from measures import *
from patternSortingStrategy import *

import rascl as sc

strip_first_ellipsis = os.getcwd().endswith("exp")

if strip_first_ellipsis:
    os.chdir("..")

EXP_DIR = "experiments"
OUTPUTDIR = os.path.join("experiment_results_fig", "sortingStrategy")


MAP = {
    "sc_precision": r"$precision_{SC}$",
    "sc_recall": r"$recall_{SC}$",
    "sc_f1": r"$F1_{SC}$"
}


def exp(sc, experiment, exp_dir, parametersEX, parametersSC, parametersCC, evaluation, noOverlapping=False, draw=False, redo=True, use_cache=True):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]
    name = experiment["name"]

    print("[EXPERIMENT]:", "Datafile: %s" %(experiment["dataFile"]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersEX[param]) if isinstance(parametersEX[param], int) else parametersEX[param]) for param in sorted(parametersEX.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted(parametersSC.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersCC[param]) if isinstance(parametersCC[param], int) else parametersCC[param]) for param in sorted(parametersCC.keys())]))

    exp_dir = os.path.join(exp_dir, data_key, name)
    pickle_dir = os.path.join(exp_dir, "pickles")

    data = []

    for run in range(1, parametersEX["runCount"] + 1):

        pickle_file = os.path.join(pickle_dir, "run%d_fimmsdims_%s.pickle" %(run, "_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])])))

        for d in [exp_dir, pickle_dir]:
            if not os.path.exists(d):
                os.makedirs(d)

        clusters = [get_cluster(row) for row in open(experiment["clustersFile"])]

        if os.path.isfile(pickle_file):
            print("Pickle file found!")

            if not redo:
                return

            object = pickle.load(open(pickle_file))

            fimms = object[0]
            dims = object[1]
        else:
            print("Running RASCL")

            samples, transactions, fimms, dims = sc.sc_parameters(experiment["dataFile"], parameters=parametersSC, run=run, draw=False, draw_new_samples=False, draw_new_centers=False, use_cache=use_cache)

            if use_cache:
                pickle.dump([fimms, dims], open(pickle_file, "w"), pickle.HIGHEST_PROTOCOL)

        rows = pd.read_csv(experiment["dataFile"], delimiter=",", header=None, index_col=False)

        clusterConverter = ClusterConverter(rows, parametersCC)

        evaluatedClusters = []

        fimmsPlusDims = [(fimm, dims[i]) for i, fimm in enumerate(fimms)]

        fimmsPlusDims = sort_patterns(parametersEX["patternSorter"], fimmsPlusDims)

        for fimmPlusDim in fimmsPlusDims:
            fimm = fimmPlusDim[0]
            dims = fimmPlusDim[1]

            fimm = clusterConverter.convert(fimm, dims)

            vv = [(i, sc_f1(fimm, cluster)) for i, cluster in enumerate(clusters)]
            vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

            closest = vv[0]

            if not noOverlapping or (noOverlapping and closest[0] not in [0,1,2,3,4,5]):
                evaluatedClusters.append((fimm, closest))

        evaluator = MEASURES[evaluation]

        data.append(([(len(cluster[0].objects), evaluator(cluster[0], clusters[cluster[1][0]]), cluster[1][0]) for cluster in evaluatedClusters], [len(cluster[0].objects) for cluster in evaluatedClusters]))

    return data

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


def draw(experiment, data, parametersEX, parametersSC, parametersCC, evaluation, noOverlapping):
    data_key = experiment["dataFile"].split("/")[-1].split(".")[0]

    if not os.path.exists(os.path.join(OUTPUTDIR, data_key)):
        os.makedirs(os.path.join(OUTPUTDIR, data_key))

    for run, ddata in enumerate(data):
        plotFile = os.path.join(OUTPUTDIR, data_key, "%s_noOverlapping%s_run%d.pdf" %("_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])]), noOverlapping, run + 1))

        map = {}

        f, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3]})

        a0.grid(which="both", axis="both", linestyle="--", linewidth=0.3 )

        if "dbsizescale" in experiment["dataFile"]:
            a0.hist(ddata[1], bins=22, range=(0, 1100))
        else:
            a0.hist(ddata[1])

        if "dbsizescale" in experiment["dataFile"]:
            a0.set_ylim(0, 6000)
            a0.set_xlim(0, 1100)
            a0.set_yticks([i * 2000 for i in range(0, 4)])
            a0.set_yticks([1000 + (i * 2000) for i in range(0, 3)], minor=True)

        a0.set_ylabel("#clusters")

        for d in ddata[0]:
            if not d[2] in map: map[d[2]] = []
            map[d[2]].append(d)

        for i, key in enumerate(sorted(map.keys(), reverse=True)):
            a1.scatter([d[0] for d in map[key]], [d[1] for d in map[key]], s=40 - i*4, color=tableau20[i], alpha=0.1)

        if evaluation == "object_size":
            a1.set_ylim(0, 405)
        else:
            a1.set_ylim(0, 1.05)

        if "dbsizescale" in experiment["dataFile"]:
            a1.set_xlim(0, 1100)

        a1.grid(which="both", axis="both", linestyle="--", linewidth=0.3 )
        a1.set_xlabel("object size")
        a1.set_ylabel(MAP[evaluation])
        a1.set_yticks([i * 0.2 for i in range(0, 6)])
        a1.set_yticks([0.1 + (i * 0.2) for i in range(0, 5)], minor=True)

        plt.tight_layout(h_pad=-1)
        plt.setp(a0.get_xticklabels(), visible=False)
        plt.savefig(plotFile)
        plt.cla()
        plt.clf()


def experiments(experiments):
    for expKey in experiments["subspaceCluster"]:
        rangedParametersEX, fixedParametersEX = split_parameters(experiments["experiment"]["parameters"])
        rangedParametersCC, fixedParametersCC = split_parameters(experiments["clusterConverter"]["parameters"])

        for evaluation in experiments["evaluation"]:

            experiment = experiments["subspaceCluster"][expKey]

            for parametersEX in flatten_parameters(rangedParametersEX, fixedParametersEX):
                fill_default_parameters_ex(parametersEX)

                for parametersCC in flatten_parameters(rangedParametersCC, fixedParametersCC):
                    fill_default_parameters_cc(parametersCC)

                    rangedParametersSC, fixedParametersSC = split_parameters(experiment["parameters"])

                    if len(rangedParametersSC) != 0:
                        print ("Cannot handle ranged params of length %d" %(len(rangedParametersSC)))
                        break

                    parametersSC = {}

                    for param in fixedParametersSC:
                        parametersSC[param["name"]] = param["value"]

                    fill_default_parameters_rascl(parametersSC)

                    for noOverlapping in [True, False]:

                        data = exp(sc, experiment, EXP_DIR, parametersEX, parametersSC, parametersCC, evaluation, noOverlapping=noOverlapping)

                        draw(experiment, data, parametersEX, parametersSC, parametersCC, evaluation, noOverlapping=noOverlapping)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    if strip_first_ellipsis:
        experiments(json.load(open(sys.argv[1][3:])))
    else:
        experiments(json.load(open(sys.argv[1])))

