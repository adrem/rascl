
import json, math, os.path, pickle, sys

import numpy as np

from collections import Counter
from matplotlib import pyplot as plt

from clustering import *
from dataStore import *
from dataUtils import *
from dimensionStrategy import *
from measures import *

KEY_FIMMS = "fimms"

VALUE_SC1 = "sc1"
VALUE_SC2 = "sc2"
VALUE_SC3 = "sc3"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

prune = True

def indicator_object_count(cluster, dims):
    return len(cluster.objects)

def indicator_dimension_count(cluster, dims):
    return len(dims)

def indicator_distinct_dimension_count(cluster, dims):
    return len(set(["_".join(["%d" %(v) for v in sorted(d)]) for d in dims]))

def indicator_distinct_dimension_count_deviation(cluster, dims):
    m = {}

    for d in ["_".join(["%d" %(v) for v in sorted(d)]) for d in dims]:
        if d not in m: m[d] = 0
        m[d] += 1

    s = np.std(m.values())

    return s

INDICATORS = [
    ("object_count", indicator_object_count),
    # ("dimension_count", indicator_dimension_count),
    # ("distinct_dimension_count", indicator_distinct_dimension_count),
    # ("distinct_dimension_count_deviation", indicator_distinct_dimension_count_deviation)
]

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v) for v in sp[1].split(" ")]))

def smallOverlap(newCluster, existingClusters, maxOverlap=0.25):
    for existingCluster in existingClusters:
        if precision(newCluster[0], existingCluster[0]) > maxOverlap:
            return False

    return True

def cluster_dimension_precision(cluster, clusters):
    print(cluster.dimensions)
    for c in clusters:
        print(c.dimensions, len(c.objects), c.objects)
    return [dimension_precision(c, cluster) for c in clusters]

def pruneFimm(fimm, dims, dps):
    allstd = np.std(dps, 0)
    allavg = np.mean(dps, 0)

    # for i in range(0, len(dps[0])):
    #     print i, allstd[i], allavg[i]

    dimsDps = [[dp[dim] for dim in dims]for dp in dps]

    stdev = np.std(dimsDps, 0)
    avg = np.mean(dimsDps, 0)

    newFimm = []
    newDps = []

    # print dims
    # print "stdev", stdev
    # print "avg", avg
    #
    # for dim in dims:
    #     print "\t", dim, np.mean([dp[dim] for dp in dps])

    for i, f in enumerate(fimm):
        add = True

        for j in range(0, len(dims)):
            add &= (np.abs(avg[j] - dps[i][dims[j]]) <= stdev[j] * 1.)

        if add:
            newFimm.append(f)
            newDps.append(dps[i])

    return newFimm, newDps

def exp(sc, experiment, parameters, draw=False, redo=True):
    method = experiment["method"]
    dataKey = experiment["dataKey"]
    name = experiment["name"]
    topk = parameters["topk"] if "topk" in parameters else 10

    print("Running %s experiment:" %(method))
    print("Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print(", ".join(["%s=%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/fimms_%s.pickle" %(pickleDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted([key for key in parameters.keys() if key != "topk"])]))
    plotFile = "%s/fimms_%s" %(figDir, "_".join(["%s%d" %(param, parameters[param]) for param in sorted(parameters.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running %s!" %(method))

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parameters, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    indicators = {i: {} for i, cluster in enumerate(clusters)}
    for i, cluster in enumerate(clusters):
        for indicator in INDICATORS:
            indicators[i][indicator[0]] = []

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    nrows = []

    minMax = [[float("inf"), float("-inf")] for i in range(0, len(rows[0]))]

    for row in rows:
        for i in range(0, len(row)):
            minMax[i][0] = min(minMax[i][0], row[i])
            minMax[i][1] = max(minMax[i][1], row[i])

    for row in rows:
        nrows.append([(1. * v - minMax[i][0]) / (minMax[i][1] - minMax[i][0]) for i, v in enumerate(row)])

    for indicator in INDICATORS:

        evaluatedClusters = []

        for ix, fimm in enumerate(fimms):
            draw = len(fimm) > 20

            ndps = [nrows[int(f)] for f in fimm]

            if prune:
                ddims = infer_dimensions(ALG_STDEV2_0_04, fimm, dims[ix], ndps)
                newFimm, newDps = pruneFimm(fimm, ddims, ndps)

                fimm = SubspaceCluster(set(newFimm), set(infer_dimensions(ALG_STDEV2_0_04, newFimm, dims[ix], newDps)))
            else:
                fimm = SubspaceCluster(set(fimm), set(infer_dimensions(ALG_STDEV2_0_04, fimm, dims[ix], ndps)))

            score = indicator[1](fimm, dims[ix])

            vv = [(i, precision(fimm, cluster)) for i, cluster in enumerate(clusters)]
            vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

            closest = vv[0]

            evaluatedClusters.append((fimm, score, closest))

            if not draw:
                continue

            ddddimmms = fimm.dimensions

            d = {i: [] for i in range(0, len(rows[0]))}
            for i in ddddimmms:
                d["_%d" % i if i > 10 else "_0%d" %i] = []
            d['_-1'] = []
            d['cluster'] = []

            dc = {i: [] for i in range(0, len(rows[0]))}
            for i in ddddimmms:
                dc["_%d" % i if i > 10 else "_0%d" %i] = []
            dc['_-1'] = []
            dc['cluster'] = []

            for i, row in enumerate(rows):
                for dim in ddddimmms:
                    if i in fimm.objects:
                        dc["_%d" % dim if dim > 10 else "_0%d" %dim].append(row[dim])
                    else:
                        d["_%d" % dim if dim > 10 else "_0%d" %dim].append(row[dim])
                for dim in range(0, len(rows[0])):
                    if i in fimm.objects:
                        dc[dim].append(row[dim])
                    else:
                        d[dim].append(row[dim])

                if i in fimm.objects:
                    dc["_-1"].append(0)
                    dc['cluster'].append('yes')
                else:
                    d["_-1"].append(0)
                    d['cluster'].append('no')

            import pandas as pd
            from pandas.plotting import parallel_coordinates

            parallel_coordinates(pd.DataFrame(data=d), 'cluster', color=['b'])
            parallel_coordinates(pd.DataFrame(data=dc), 'cluster', color=['r'])

            plt.show()

        evaluatedClusters = sorted(evaluatedClusters, key=lambda tuple: tuple[1], reverse=True)

        smallOverlapClusters = []

        for evaluatedCluster in evaluatedClusters:
            if smallOverlap(evaluatedCluster, smallOverlapClusters):
                smallOverlapClusters.append(evaluatedCluster)

            if len(smallOverlapClusters) == topk:
                break

        for evaluatedCluster in smallOverlapClusters:
            indicators[evaluatedCluster[2][0]][indicator[0]].append(evaluatedCluster[0])

        for c in range(0, len(clusters)):
            indicators[c][indicator[0]] = [cluster_dimension_precision(clusters[c], indicators[c][indicator[0]])]

    return indicators

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def drawInfoGroupedArr(experiment, infoArr, key, topk):
    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped" % (expDir)

    plotFile = "%s/test_%s_dimension_precision_topk%d_indicator_%s_smalloverlap.pdf" %(figGroupedDir, infoArr[0]["test"], topk, key)

    for ic in range(0, len(infoArr[0]["data"])):
        x = [i["value"] for i in infoArr]

        y = []
        for ix, i in enumerate(x):
            print(infoArr)
            y.append(infoArr[ix]["data"][ic][key])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y)))*2.0 + ic * 0.3, widths=0.25)
        p = plt.plot([], label="%d" %(ic))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()

def _getParameters(experiment):
    rangedParameters = []
    fixedParameters = []

    for parameter in experiment["parameters"]:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def experiments(experiments, topk):
    for key in experiments:
        experiment = experiments[key]

        if experiment["method"] == VALUE_SC1:
            import sc1 as sc
        elif experiment["method"] == VALUE_SC2:
            import sc2_ckdtree as sc
        elif experiment["method"] == VALUE_SC3:
            import sc3 as sc
        else:
            print("Abort. Invalid method!")
            return

        sc.FIXEDDIMENSIONS = experiment["fixedDimensions"].lower() == "true"

        rangedParameters, fixedParameters = _getParameters(experiment)

        if len(rangedParameters) != 1:
            print("Abort. Can only handle one range parameter!")
            return

        rangedParam = rangedParameters[0]

        infoArr = []

        for v in range(rangedParam["range"][0],
                       rangedParam["range"][1],
                       rangedParam["step"]):

            parameters = {}

            parameters[rangedParam["name"]] = v
            parameters["topk"] = topk

            for param in fixedParameters:
                parameters[param["name"]] = param["value"]

            infoArr.append({"test": rangedParam["name"], "value": v, "data": exp(sc, experiment, parameters)})

        for indicator in INDICATORS:
            drawInfoGroupedArr(experiment, infoArr, indicator[0], topk)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    for topk in range(10, 51, 10):
        experiments(json.load(open(sys.argv[1])), topk)
