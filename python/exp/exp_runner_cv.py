
import itertools, json, math, os.path, pickle, sys

from matplotlib import pyplot as plt

from clusterConverter import ClusterConverter
from dataStore import *
from dataUtils import *
from dimensionStrategy import *
from measures import *
from patternSortingStrategy import *

import sc2_ckdtree_v2 as sc

KEY_FIMMS = "fimms"

COLORS = ["b", "g", "r", "c", "m", "y", "k", "w"]

prune = True

def drawClustersCounts(clustersCounts, plotFile, draw=True, pct=False):
    f, axarr = plt.subplots()

    ind = np.arange(len(clustersCounts[0]))

    width = 0.30

    for i, clusterCounts in enumerate(clustersCounts):
        axarr.bar(ind, clusterCounts, width, color=COLORS[i])
        ind = ind + width

    axarr.set_xlabel("purity")
    axarr.set_ylabel("#")
    axarr.set_title(plotFile)

    if pct:
        axarr.set_ylim([0.0, 0.5])
    axarr.set_xlim([0, 11])

    plt.savefig(plotFile)

    if draw:
        plt.show()

def getCluster(row):
    sp = row.rstrip("\n").split("|")

    return SubspaceCluster(set([int(v) for v in sp[0].split(" ")]), set([int(v)-1 for v in sp[1].split(" ")]))

def smallOverlap(newCluster, existingClusters, maxOverlap=0.25):
    for existingCluster in existingClusters:
        if precision(newCluster[0], existingCluster[0]) > maxOverlap:
            return False

    return True

def cluster_dimension_precision(cluster, clusters):
    print(cluster.dimensions)
    for c in clusters:
        print(c.dimensions, len(c.objects), c.objects)
    return [dimension_precision(c, cluster) for c in clusters]

def drawFimm(rows, fimm):
    ddddimmms = fimm.dimensions

    d = {i: [] for i in range(0, len(rows[0]))}
    for i in ddddimmms:
        d["_%d" % i if i > 10 else "_0%d" % i] = []
    d['_-1'] = []
    d['cluster'] = []

    dc = {i: [] for i in range(0, len(rows[0]))}
    for i in ddddimmms:
        dc["_%d" % i if i > 10 else "_0%d" % i] = []
    dc['_-1'] = []
    dc['cluster'] = []

    for i, row in enumerate(rows):
        for dim in ddddimmms:
            if i in fimm.objects:
                dc["_%d" % dim if dim > 10 else "_0%d" % dim].append(row[dim])
            else:
                d["_%d" % dim if dim > 10 else "_0%d" % dim].append(row[dim])
        for dim in range(0, len(rows[0])):
            if i in fimm.objects:
                dc[dim].append(row[dim])
            else:
                d[dim].append(row[dim])

        if i in fimm.objects:
            dc["_-1"].append(0)
            dc['cluster'].append('yes')
        else:
            d["_-1"].append(0)
            d['cluster'].append('no')

    import pandas as pd
    from pandas.plotting import parallel_coordinates

    parallel_coordinates(pd.DataFrame(data=d), 'cluster', color=['b'])
    parallel_coordinates(pd.DataFrame(data=dc), 'cluster', color=['r'])

    plt.show()

def exp(sc, experiment, parametersEX, parametersSC, parametersCC, evaluation, run=1, draw=False, redo=True):
    dataKey = experiment["dataKey"]
    name = experiment["name"]

    topk = parametersEX["topk"] if "topk" in parametersEX else 10

    print("[EXPERIMENT]:", "Datafile: %s" %(DATA[dataKey][KEY_RAW_FILE]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersEX[param]) if isinstance(parametersEX[param], int) else parametersEX[param]) for param in sorted(parametersEX.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted(parametersSC.keys())]))
    print("[EXPERIMENT]:", ", ".join(["%s=%s" % (param, "%d" %(parametersCC[param]) if isinstance(parametersCC[param], int) else parametersCC[param]) for param in sorted(parametersCC.keys())]))

    expDir = "experiments/%s/%s" %(dataKey, name)
    pickleDir = "%s/pickles" %(expDir)
    figDir = "%s/fig" %(expDir)
    figGroupedDir = "%s/figGrouped" %(expDir)

    pickleFile = "%s/run%d_fimms_%s.pickle" %(pickleDir, run, "_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted([key for key in parametersSC.keys() if key != "topk"])]))
    plotFile = "%s/run%d_fimms_%s" %(figDir, run, "_".join(["%s%s" % (param, "%d" %(parametersSC[param]) if isinstance(parametersSC[param], int) else parametersSC[param]) for param in sorted(parametersSC.keys())]))

    for d in [expDir, pickleDir, figDir, figGroupedDir]:
        if not os.path.exists(d):
            os.makedirs(d)

    clusters = [getCluster(row) for row in open(DATA[dataKey][KEY_CLUSTERS_FILE])]

    if os.path.isfile(pickleFile):
        print("Pickle file found!")

        if not redo:
            return

        object = pickle.load(open(pickleFile))

        samples = object[0]
        transactions = object[1]
        fimms = object[2]
        dims = object[3]
    else:
        print("Running SC2_CDKTEE_V2")

        samples, transactions, fimms, dims = sc.scc(dataKey, parameters=parametersSC, run=run, draw=False, drawNewSamples=False)

        pickle.dump([samples, transactions, fimms, dims], open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    clusterConverter = ClusterConverter(rows, parametersCC)

    evaluatedClusters = []

    fimmsPlusDims = [(fimm, dims[i]) for i, fimm in enumerate(fimms)]

    fimmsPlusDims = sort_patterns(parametersEX["patternSorter"], fimmsPlusDims)

    for fimmPlusDim in fimmsPlusDims:
        fimm = fimmPlusDim[0]
        dims = fimmPlusDim[1]

        # print len(fimm), len(dims)

        draw = len(fimm) > 50
        draw = False

        fimm = clusterConverter.convert(fimm, dims)

        vv = [(i, e4sc_precision(fimm, cluster)) for i, cluster in enumerate(clusters)]
        vv = sorted(vv, key=lambda tuple: tuple[1], reverse=True)

        closest = vv[0]

        evaluatedClusters.append((fimm, closest))

        if draw:
            drawFimm(rows, fimm)

    topKClusters = []

    for evaluatedCluster in evaluatedClusters:
        if smallOverlap(evaluatedCluster, topKClusters, maxOverlap=parametersEX["overlap"]):
            topKClusters.append(evaluatedCluster)

            # if raw_input() == "y":
            #     fimm = evaluatedCluster[0]
            #
            #     print precision(fimm, clusters[evaluatedCluster[1][0]])
            #     print recall(fimm, clusters[evaluatedCluster[1][0]])
            #     print evaluatedCluster[1][0]
            #
            #     ddddimmms = fimm.dimensions
            #     ddddimmms = clusters[evaluatedCluster[1][0]].dimensions
            #
            #     d = {i: [] for i in range(0, len(rows[0]))}
            #     for i in ddddimmms:
            #         d["_%d" % i] = []
            #     d['_-1'] = []
            #     d['cluster'] = []
            #
            #     dc = {i: [] for i in range(0, len(rows[0]))}
            #     for i in ddddimmms:
            #         dc["_%d" % i] = []
            #     dc['_-1'] = []
            #     dc['cluster'] = []
            #
            #     for i, row in enumerate(rows):
            #         for dim in ddddimmms:
            #             if i in fimm.objects:
            #                 dc["_%d" % dim].append(row[dim])
            #             else:
            #                 d["_%d" % dim].append(row[dim])
            #         for dim in range(0, len(rows[0])):
            #             if i in fimm.objects:
            #                 dc[dim].append(row[dim])
            #             else:
            #                 d[dim].append(row[dim])
            #
            #         if i in fimm.objects:
            #             dc["_-1"].append(0)
            #             dc['cluster'].append('yes')
            #         else:
            #             d["_-1"].append(0)
            #             d['cluster'].append('no')
            #
            #     import pandas as pd
            #     from pandas.plotting import parallel_coordinates
            #
            #     parallel_coordinates(pd.DataFrame(data=d), 'cluster', color=['b'])
            #     parallel_coordinates(pd.DataFrame(data=dc), 'cluster', color=['r'])
            #
            #     plt.show()


        if len(topKClusters) == topk:
            break

    topKClustersGrouped = {i: [] for i in range(0, len(clusters))}

    for cluster in topKClusters:
        topKClustersGrouped[cluster[1][0]].append(cluster[0])

    data = {i: [] for i, cluster in enumerate(clusters)}
    data["all"] = []

    evaluator = MEASURES[evaluation]

    for key in topKClustersGrouped:
        for cluster in topKClustersGrouped[key]:
            eval = evaluator(cluster, clusters[key])
            data[key].append(eval)
            data["all"].append(eval)

    return data

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

import numpy
w = None

def drawInfoGroupedArr(experiment, infoArr, parametersEX, parametersSC, parametersCC, evaluation):
    global w

    for i in range(0, len(infoArr)):
        w.write("test_%s%s_%s_topk%d_patternSorter%s_overlap%.2f_prune%s_recomputeDimensions%s_dimensionsAlg%s.pdf" % (infoArr[i]["test"], "%d" %(infoArr[i]["value"]) if isinstance(infoArr[i]["value"], int) else infoArr[i]["value"], evaluation, parametersEX["topk"], parametersEX["patternSorter"], parametersEX["overlap"], str(parametersCC["prune"]), str(parametersCC["recomputeDimensions"]), parametersCC["dimensionsAlg"]))
        w.write(" ")
        w.write(" ".join(["%.2f" %(numpy.average(arr["all"])) for arr in infoArr[i]["data"]]))
        w.write("\n")

    expDir = "experiments/%s/%s" % (experiment["dataKey"], experiment["name"])
    figGroupedDir = "%s/figGrouped" % (expDir)

    plotFile = "%s/test_%s_%s_topk%d_patternSorter%s_overlap%.2f_prune%s_recomputeDimensions%s_dimensionsAlg%s.pdf" %(figGroupedDir, infoArr[0]["test"], evaluation, parametersEX["topk"], parametersEX["patternSorter"], parametersEX["overlap"], str(parametersCC["prune"]), str(parametersCC["recomputeDimensions"]), parametersCC["dimensionsAlg"])

    x = [i["value"] for i in infoArr]
    print(infoArr)
    print(x)
    for ic, key in enumerate(sorted(infoArr[0]["data"][0].keys())):
        y = []
        for ix, i in enumerate(x):
            y.append([numpy.average(arr[key]) if key in arr else None for arr in infoArr[ix]["data"]])

        bp = plt.boxplot(y, positions=np.array(xrange(len(y))) * 2.0 + 1. * ic / (len(y)+1) + 0.1, widths=0.9/(len(y)+1))
        p = plt.plot([], label="%s" %("%d" %key if isinstance(key, int) else key))
        set_box_color(bp, p[0].get_color())

    plt.xticks(xrange(0, len(x) * 2, 2), x)
    plt.xlim(-1, len(x) * 2 + 1)
    if evaluation == "object_size":
        plt.ylim(0, 405)
    else:
        plt.ylim(0, 1.05)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=7, mode="expand", borderaxespad=0.)
    plt.savefig(plotFile)
    plt.cla()
    plt.clf()

def _splitParameters(parameters):
    rangedParameters = []
    fixedParameters = []

    for parameter in parameters:
        if parameter["type"] == "range":
            rangedParameters.append(parameter)
            parameter["values"] = range(parameter["range"]["start"], parameter["range"]["end"], parameter["range"]["step"])
        elif parameter["type"] == "values":
            rangedParameters.append(parameter)
        elif parameter["type"] == "fixed":
            fixedParameters.append(parameter)
        else:
            print("Can not handle parameter of type '%s'" %(parameter["type"]))

    return rangedParameters, fixedParameters

def _flattenParameters(rangedParameters, fixedParameters):
    parametersList = [[(rangedParameter["name"], value) for value in rangedParameter["values"]] for rangedParameter in rangedParameters]
    parametersList.extend([[(fixedParameter["name"], fixedParameter["value"])] for fixedParameter in fixedParameters])

    for parameters in itertools.product(*parametersList):
        yield {parameter[0]: parameter[1] for parameter in parameters}

def _fillDefaultParametersEX(parameters):
    if "topk" not in parameters: parameters["topk"] = 10
    if "patternSorter" not in parameters: parameters["patternSorter"] = "object_count"
    if "overlap" not in parameters: parameters["overlap"] = 0.25

def _fillDefaultParametersSC(parameters):
    if "n" not in parameters: parameters["n"] = 1000
    if "k" not in parameters: parameters["k"] = 200
    if "kk" not in parameters: parameters["kk"] = 5
    if "s" not in parameters: parameters["s"] = 100
    if "m" not in parameters: parameters["m"] = 1000
    if "sampleDimCount" not in parameters: parameters["sampleDimCount"] = "variable"

def _fillDefaultParametersCC(parameters):
    if "prune" not in parameters: parameters["prune"] = "True"
    if "recomputeDimensions" not in parameters: parameters["recomputeDimensions"] = "True"
    if "dimensionsAlg" not in parameters: parameters["dimensionsAlg"] = "stdev_based2_0.04"

def experiments(experiments):
    global w
    w = open(experiments["subspaceCluster"]["experiment_sc2_v2_vary_sampleDimCount"]["dataKey"], "a")

    rangedParametersEX, fixedParametersEX = _splitParameters(experiments["experiment"]["parameters"])
    rangedParametersCC, fixedParametersCC = _splitParameters(experiments["clusterConverter"]["parameters"])

    for evaluation in experiments["evaluation"]:

        for key in experiments["subspaceCluster"]:
            experiment = experiments["subspaceCluster"][key]

            for parametersEX in _flattenParameters(rangedParametersEX, fixedParametersEX):
                _fillDefaultParametersEX(parametersEX)

                for parametersCC in _flattenParameters(rangedParametersCC, fixedParametersCC):
                    _fillDefaultParametersCC(parametersCC)

                    rangedParametersSC, fixedParametersSC = _splitParameters(experiments["subspaceCluster"][key]["parameters"])

                    if len(rangedParametersSC) != 1:
                        print ("Cannot handle ranged params of length %d" %(len(rangedParametersSC)))
                        break

                    rangedParameterSC = rangedParametersSC[0]

                    infoArr = []

                    for v in rangedParameterSC["values"]:
                        parametersSC = {rangedParameterSC["name"]: v}

                        for param in fixedParametersSC:
                            parametersSC[param["name"]] = param["value"]

                        _fillDefaultParametersSC(parametersSC)

                        data = [exp(sc, experiment, parametersEX, parametersSC, parametersCC, evaluation, run=i) for i in range(1, 11)]

                        infoArr.append({"test": rangedParameterSC["name"], "value": v, "data": data})

                    drawInfoGroupedArr(experiment, infoArr, parametersEX, parametersSC, parametersCC, evaluation)

    w.close()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("Please add an experiment configuration file")
        sys.exit(-1)

    experiments(json.load(open(sys.argv[1])))
