
import os, tempfile

from subprocess import call

from clustering import SubspaceCluster

ELKI_JAR = "bin/elki-bundle-0.7.6-SNAPSHOT.jar"

def parseClusters(file):
    scs = []

    name = None
    clusterSize = -1

    with open(file, "r") as r:
        for row in r:
            if clusterSize == 0:
                if name == "Noise":
                    print("Discarding noise cluster")
                elif name != None:
                    scs.append(SubspaceCluster(set(objects), set(dimensions)))
                    clusterSize = -1

            if row.startswith("# Cluster:"):
                name = row.strip().split(": ")[1]
                objects = []
            elif row.startswith("# Subspace: Dimensions:"):
                if row.strip() == "# Subspace: Dimensions:]":
                    dimensions = []
                else:
                    dimensions = [int(v) for v in row.split("[")[1].split("]")[0].split(", ")]
            elif row.startswith("# Cluster size:"):
                clusterSize = int(row.strip().split(":")[1])
            elif row.startswith("ID"):
                objects.append(int(row.split("=")[1].split(" ")[0]))
                clusterSize -= 1
        if clusterSize == 0:
            if name == "Noise":
                print("Discarding noise cluster")
            elif name != None:
                scs.append(SubspaceCluster(set(objects), set(dimensions)))

    return scs

def clique(rows, xsi, tau, prune=False):
    if not isinstance(xsi, int):
        raise ValueError("Parameter xsi should be an integral number")
    if tau >= 1:
        raise ValueError("Parameter tau should be a real number less than 1")

    print("Running CLIQUE [xsi=%d, tau=%.5f, prune=%s]" % (xsi, tau, str(prune)))

    rawFile = tempfile.NamedTemporaryFile().name

    with open(rawFile, "w") as f:
        for row in rows:
            f.write("%s\n" % (" ".join(["%.5f" % (v) for v in row])))

    clustersFile = tempfile.NamedTemporaryFile().name

    with open(clustersFile, "w") as o:
        if prune:
            call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
                  "-algorithm", "clustering.subspace.CLIQUE", "-clique.xsi", "%d" %(xsi), "-clique.tau", "%.5f" %(tau),
                  "-clique.prune"], stdout=o)
        else:
            call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
                  "-algorithm", "clustering.subspace.CLIQUE", "-clique.xsi", "%d" % (xsi), "-clique.tau", "%.5f" %(tau)],
                 stdout=o)

    clusters = parseClusters(clustersFile)

    os.remove(rawFile)
    os.remove(clustersFile)

    return clusters

def doc(rows, alpha=0.2, beta=0.8, w=0.05):
    print("Running DOC [alpha=%.5f, beta=%.5f, w=%.5f]" % (alpha, beta, w))

    rawFile = tempfile.NamedTemporaryFile().name

    with open(rawFile, "w") as f:
        for row in rows:
            f.write("%s\n" % (" ".join(["%.5f" % (v) for v in row])))

    clustersFile = tempfile.NamedTemporaryFile().name

    with open(clustersFile, "w") as o:
        call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
              "-algorithm", "clustering.subspace.DOC", "-doc.alpha", "%.5f" %(alpha), "-doc.beta", "%.5f" %(beta),
              "-doc.w", "%.5f" %(w)], stdout=o)

    clusters = parseClusters(clustersFile)

    os.remove(rawFile)
    os.remove(clustersFile)

    return clusters

def p3c(rows, alpha=0.001, threshold=0.0001, em_maxiter=20, em_delta=0.00001, minSize=1):
    print("Running P3C [alpha=%.5f, threshold=%.5f, em_maxiter=%d, em_delta=%.5f, minSize=%d]" % (alpha, threshold, em_maxiter, em_delta, minSize))

    rawFile = tempfile.NamedTemporaryFile().name

    with open(rawFile, "w") as f:
        for row in rows:
            f.write("%s\n" % (" ".join(["%.5f" % (v) for v in row])))

    clustersFile = tempfile.NamedTemporaryFile().name

    with open(clustersFile, "w") as o:
        call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
              "-algorithm", "clustering.subspace.P3C", "-p3c.alpha", "%.5f" %(alpha), "-p3c.threshold", "%.5f" %(threshold),
              "-p3c.em.maxiter", "%d" %(em_maxiter), "-p3c.em.delta", "%.5f" %(em_delta), "-p3c.minsize", "%d" %(minSize)], stdout=o)

    clusters = parseClusters(clustersFile)

    os.remove(rawFile)
    os.remove(clustersFile)

    return clusters

def proclus(rows, k, l, ki=30, mi=10):
    if not isinstance(k, int):
        raise ValueError("Parameter k should be an integral number")
    if not isinstance(l, int):
        raise ValueError("Parameter l should be an integral number")

    print("Running PROCLUS [k=%d, l=%d, ki=%d, mi=%d]" % (k, l, ki, mi))

    rawFile = tempfile.NamedTemporaryFile().name

    with open(rawFile, "w") as f:
        for row in rows:
            f.write("%s\n" % (" ".join(["%.5f" % (v) for v in row])))

    clustersFile = tempfile.NamedTemporaryFile().name

    with open(clustersFile, "w") as o:
        call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
              "-algorithm", "clustering.subspace.PROCLUS", "-projectedclustering.k", "%d" %(k), "-projectedclustering.l", "%d" %(l),
              "-projectedclustering.k_i", "%d" % (ki), "-proclus.mi", "%d" %(mi)], stdout=o)

    clusters = parseClusters(clustersFile)

    os.remove(rawFile)
    os.remove(clustersFile)

    return clusters

SUBCLU_ONEDIMENSIONAL_DISTANCEFUNCTION = "OnedimensionalDistanceFunction"
SUBCLU_SUBSPACE_EUCLIDIAN_DISTANCEFUNCTION = "SubspaceEuclideanDistanceFunction"
SUBCLU_SUBSPACE_LPNORM_DISTANCEFUNCTION = "SubspaceLPNormDistanceFunction"
SUBCLU_SUBSPACE_MANHATTAN_DISTANCEFUNCTION = "SubspaceManhattanDistanceFunction"
SUBCLU_SUBSPACE_MAXIMUM_DISTANCEFUNCTION = "SubspaceMaximumDistanceFunction"

def subclu(rows, epsilon, minpts, distanceFunction=SUBCLU_SUBSPACE_EUCLIDIAN_DISTANCEFUNCTION, dims=1, mindim=1):
    if not isinstance(minpts, int):
        raise ValueError("Parameter minpts should be an integral number")
    if not isinstance(mindim, int):
        raise ValueError("Parameter mindim should be an integral number")
    if not (distanceFunction == SUBCLU_ONEDIMENSIONAL_DISTANCEFUNCTION or
            distanceFunction == SUBCLU_SUBSPACE_EUCLIDIAN_DISTANCEFUNCTION or
            distanceFunction == SUBCLU_SUBSPACE_LPNORM_DISTANCEFUNCTION or
            distanceFunction == SUBCLU_SUBSPACE_MANHATTAN_DISTANCEFUNCTION or
            distanceFunction == SUBCLU_SUBSPACE_MAXIMUM_DISTANCEFUNCTION):
        raise ValueError("Distance function must be one of:", SUBCLU_ONEDIMENSIONAL_DISTANCEFUNCTION,
                         SUBCLU_SUBSPACE_EUCLIDIAN_DISTANCEFUNCTION, SUBCLU_SUBSPACE_LPNORM_DISTANCEFUNCTION,
                         SUBCLU_SUBSPACE_MANHATTAN_DISTANCEFUNCTION, SUBCLU_SUBSPACE_MAXIMUM_DISTANCEFUNCTION)

    print("Running SUBCLU [distanceFunction=%s, dims=%d, epsilon=%.5f, minpts=%d, mindim=%d]" % (distanceFunction, dims, epsilon, minpts, mindim))

    rawFile = tempfile.NamedTemporaryFile().name

    with open(rawFile, "w") as f:
        for row in rows:
            f.write("%s\n" % (" ".join(["%.5f" % (v) for v in row])))

    clustersFile = tempfile.NamedTemporaryFile().name

    with open(clustersFile, "w") as o:
        call(["java", "-cp", ELKI_JAR, "de.lmu.ifi.dbs.elki.application.KDDCLIApplication", "-dbc.in", rawFile,
              "-algorithm", "clustering.subspace.SUBCLU", "-subclu.distancefunction", distanceFunction,
              "-distance.dims", "%d" %(dims), "-subclu.epsilon", "%.5f" %(epsilon), "-subclu.minpts", "%d" % (minpts),
              "-subclu.mindim", "%d" %(mindim)], stdout=o)

    clusters = parseClusters(clustersFile)

    os.remove(rawFile)
    os.remove(clustersFile)

    return clusters
