
import math, unittest

from collections import Counter

import dimensionSampler

ROWS1 = [[i, i] for i in range(1, 101)]

ROWS2 = [[i, 100 - i] for i in range(1, 101)]

ROWS3 = [[i, i] for i in range(1, 101)] + [[i, 100 + i] for i in range(1, 21)] + [[i, 120 + i] for i in range(1, 21)] + [[i, 140 + i] for i in range(1, 21)]

ROWS4 = [[i, i, 161 - i] for i in range(1, 101)] + [[i, 100 + i, 61 - i] for i in range(1, 21)] + [[i, 120 + i, 41 - i] for i in range(1, 21)] + [[i, 140 + i, 21 - i] for i in range(1, 21)]


class TestDimensionSamplerFactory(unittest.TestCase):

    def testVariable(self):
        self.assertTrue(isinstance(dimensionSampler.dimensionSampler(ROWS1, 2, "variable"), dimensionSampler.UniformDimensionOfLengthSampler))

    def testVarious(self):
        dimSampler = dimensionSampler.dimensionSampler(ROWS1, 2, "uniform_1")

        self.assertTrue(isinstance(dimSampler, dimensionSampler.UniformDimensionSampler))
        self.assertTrue(dimSampler._sampleDimCount, 1)


        dimSampler = dimensionSampler.dimensionSampler(ROWS1, 2, "uniform_1")

        self.assertTrue(isinstance(dimSampler, dimensionSampler.UniformDimensionSampler))
        self.assertTrue(dimSampler._sampleDimCount, 2)


        dimSampler = dimensionSampler.dimensionSampler(ROWS1, 2, "weighted1_1")

        self.assertTrue(isinstance(dimSampler, dimensionSampler.Weighted1DimensionSampler))
        self.assertTrue(dimSampler._sampleDimCount, 1)


        dimSampler = dimensionSampler.dimensionSampler(ROWS1, 2, "weighted2_2")

        self.assertTrue(isinstance(dimSampler, dimensionSampler.Weighted2DimensionSampler))
        self.assertTrue(dimSampler._sampleDimCount, 2)


class TestComputeWeights(unittest.TestCase):

    def testUniform(self):
        dimSampler = dimensionSampler.dimensionSampler(ROWS1, 2, "uniform_2")

        self.assertEqual(dimSampler._weights, [0.5, 0.5])


        dimSampler = dimensionSampler.dimensionSampler(ROWS2, 2, "uniform_2")

        self.assertEqual(dimSampler._weights, [0.5, 0.5])


    def testNonUniform(self):
        dimSampler = dimensionSampler.dimensionSampler(ROWS3, 2, "weighted1_2")

        self.assertEqual(dimSampler._weights, [2./3., 1./3.])


        dimSampler = dimensionSampler.dimensionSampler(ROWS4, 3, "weighted1_3")

        self.assertEqual(dimSampler._weights, [2./4., 1./4., 1./4.])


class TestUniformDimensionSampler(unittest.TestCase):

    def testConstructor(self):
        dimSampler = dimensionSampler.UniformDimensionSampler(2)

        self.assertEqual(dimSampler._dimensionCount, 2)


    def testSampling(self):
        dimSampler = dimensionSampler.UniformDimensionSampler(2)

        for i in range(0, 10000):
            self.assertLessEqual(len(dimSampler.sample()), max([int(math.sqrt(2)), 2]))


class TestWeightedDimensionSampler(unittest.TestCase):

    def testConstructor(self):
        dimSampler = dimensionSampler.WeightedDimensionSampler(ROWS1, 2, 2)

        self.assertEqual(dimSampler._dimensionCount, 2)
        self.assertEqual(dimSampler._sampleDimCount, 2)
        self.assertEqual(dimSampler._weights, [0.5, 0.5])


    def testSampling(self):
        dimSampler = dimensionSampler.WeightedDimensionSampler(ROWS1, 2, 2)

        dimensions = []

        for i in range(0, 10000):
            sample = dimSampler.sample()
            dimensions.extend(sample)
            self.assertEqual(len(sample), 2)

        c = Counter(dimensions)

        self.assertAlmostEqual(1. * c[0] / 20000., 0.5, delta=0.05)
        self.assertAlmostEqual(1. * c[1] / 20000., 0.5, delta=0.05)


        dimSampler = dimensionSampler.WeightedDimensionSampler(ROWS3, 2, 2)

        dimensions = []

        for i in range(0, 10000):
            sample = dimSampler.sample()
            dimensions.extend(sample)
            self.assertEqual(len(sample), 2)

        c = Counter(dimensions)

        self.assertAlmostEqual(1. * c[0] / 20000., 2./3., delta=0.05)
        self.assertAlmostEqual(1. * c[1] / 20000., 1./3., delta=0.05)


        dimSampler = dimensionSampler.WeightedDimensionSampler(ROWS4, 3, 2)

        dimensions = []

        for i in range(0, 10000):
            sample = dimSampler.sample()
            dimensions.extend(sample)
            self.assertEqual(len(sample), 2)

        c = Counter(dimensions)

        self.assertAlmostEqual(1. * c[0] / 20000., 2./4., delta=0.05)
        self.assertAlmostEqual(1. * c[1] / 20000., 1./4., delta=0.05)
        self.assertAlmostEqual(1. * c[2] / 20000., 1./4., delta=0.05)

if __name__ == '__main__':
    unittest.main()
