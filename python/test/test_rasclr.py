
import os, shutil, unittest

import pandas as pd

from collections import Counter
from numpy import std

import rasclr

if os.getcwd().endswith("test"):
    os.chdir("..")

OUTPUT_DIR = os.path.join("output", "rasclr")

TRANSACTIONS = pd.DataFrame([
    [10.6888437031, 10.5159088059],
    [9.84114316166, 9.51783350059],
    [10.0225494427, 9.8098682749],
    [10.5675971781, 9.60662545216],
    [9.9531939083, 10.1667640789],
    [10.8162257704, 10.0093737116],
    [9.5636756888, 10.5116084083],
    [10.2367379934, 9.50101268272],
    [9.62029513864, 10.4596634965],
    [10.7976765759, 10.3679678638],
    [9.94428543091, 9.20140241614],
    [9.86834367091, 10.2217739469],
    [9.95401955311, 10.7306198555],
    [9.52098462078, 10.610055654],
    [10.0973986077, 9.02808340033],
    [10.4394093728, 9.79764708445],
    [10.6496899543, 10.3363064025],
    [9.00228563863, 9.98715573293],
    [10.735205551, 9.48782175377],
    [9.65040872549, 10.7409424642]
])

SAMPLES = [
    ([0,1,2,3,4], [0,0]),
    ([2,3,4,5,6], [0,1]),
    ([4,5,6,7,8], [1,1]),
    ([6,7,8,9,10], [1,0]),
    ([8,9,10,11,12], [0,1])
]

class TestGenerateSamples(unittest.TestCase):

    def samplesCheck(self, samples, n, k, d):
        self.assertEqual(len(samples), n)

        for sample in samples:
            self.assertEqual(len(sample[0]), k)
            self.assertEqual(len(sample[1]), d)

    def testGenerateNewSamples(self):
        n = 10
        k = 5
        d = 2
        sampleDimCount = "uniform|%d" %(d)

        samples = rasclr.generate_new_samples(TRANSACTIONS, n, k, sampleDimCount)

        self.samplesCheck(samples, n, k ,d)


    def testUniformDataPoints(self):
        n = 10000
        k = 5
        d = 2
        sampleDimCount = "uniform|%d" %(d)

        samples = rasclr.generate_new_samples(TRANSACTIONS, n, k, sampleDimCount)

        c = Counter([dp for sample in samples for dp in sample[0]])

        counts = [c[i] for i in range(0, TRANSACTIONS.shape[0])]

        self.assertLess(std(counts), 100, "Standard deviation should be small")


    def testGenerateSamples(self):
        dataKey = "testGenerateSamples"
        n = 10
        k = 5
        d = 2
        sampleDimCount = "uniform|%d" %(d)
        run = 1

        outputDir = os.path.join(OUTPUT_DIR, dataKey)

        if os.path.isdir(outputDir):
            shutil.rmtree(outputDir)
        os.makedirs(outputDir)

        samples = rasclr.generate_samples(dataKey, TRANSACTIONS, n, k, sampleDimCount, run=run, use_cache=True)

        self.samplesCheck(samples, n, k ,d)

        shutil.rmtree(outputDir)


class TestGenerateTransactions(unittest.TestCase):

    def transactionsCheck(self, transactions):
        self.assertEqual(len(transactions), len(SAMPLES) * len(SAMPLES[0][0]))

        for i in range(0, len(transactions), len(SAMPLES[0][0])):
            all = []

            for j in range(0, len(SAMPLES[0][0])):
                all.extend(transactions[i + j])

            allC = Counter(all)

            for key in allC:
                self.assertEqual(allC[key], 1)


    def testGenerateNewTransactions(self):
        transactions = rasclr.generate_new_transactions(SAMPLES, TRANSACTIONS, draw_new_centers=False)

        self.transactionsCheck(transactions)


    def testGenerateTransactions(self):
        dataKey = "testGenerateTransactions"
        n = 5
        k = 5
        d = 2
        sampleDimCount = "uniform|%d" %(d)
        run = 1

        outputDir = os.path.join(OUTPUT_DIR, dataKey)

        if os.path.isdir(outputDir):
            shutil.rmtree(outputDir)
        os.makedirs(outputDir)

        transactions = rasclr.generate_transactions(dataKey, TRANSACTIONS, SAMPLES, n, k, sampleDimCount, run=run, draw_new_centers=False, use_cache=True)

        self.transactionsCheck(transactions)

        shutil.rmtree(outputDir)

if __name__ == '__main__':
    unittest.main()
