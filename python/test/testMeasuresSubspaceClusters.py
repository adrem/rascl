#!/bin/python

import unittest

from measures import *
from clustering import *

EMPTY_SET = set([])
SET1 = set([1, 2, 3])
SET2 = set([1, 2, 3, 4, 5])
SET3 = set([3, 4, 5])
SET4 = set([4, 5, 6])

EMPTY_SC = SubspaceCluster(set([]), set([]))
SC1 = SubspaceCluster(SET1, SET1)
SC2 = SubspaceCluster(SET2, SET2)
SC3 = SubspaceCluster(SET3, SET3)
SC4 = SubspaceCluster(SET4, SET4)

EMPTY_SCS = [EMPTY_SC]
SCS1 = [SC1, SC2, SC3]
SCS2 = [SC2, SC3, SC4]
SCS3 = [SC3, SC4]
SCS4 = [SC4]



class TestSubspaceClusterRecall(unittest.TestCase):

    def testNoSubspaceCluster(self):
        self.assertEqual(-1.   , sc_recall(EMPTY_SET, EMPTY_SET))
        self.assertEqual(-1.   , sc_recall(SET1, SET1))
        self.assertEqual(-1.   , sc_recall(SET2, SET2))
        self.assertEqual(-1.   , sc_recall(SET3, SET3))
        self.assertEqual(-1.   , sc_recall(SET4, SET4))

    def testEmpty(self):
        self.assertEqual(1.    , sc_recall(EMPTY_SC, EMPTY_SC))

    def testEmptyResult(self):
        self.assertEqual(0.    , sc_recall(EMPTY_SC, SC1))

    def testEmptyGround(self):
        self.assertEqual(1.    , sc_recall(SC1, EMPTY_SC))

    def testEqual(self):
        self.assertEqual(1.    , sc_recall(SC1, SC1))

    def testMix(self):
        self.assertEqual(9./25., sc_recall(SC1, SC2))
        self.assertEqual(1./9. , sc_recall(SC1, SC3))
        self.assertEqual(0.    , sc_recall(SC1, SC4))

        self.assertEqual(1.    , sc_recall(SC2, SC1))
        self.assertEqual(1./9. , sc_recall(SC3, SC1))
        self.assertEqual(0.    , sc_recall(SC4, SC1))

        self.assertEqual(1.    , sc_recall(SC2, SC3))
        self.assertEqual(4./9. , sc_recall(SC2, SC4))

        self.assertEqual(9./25., sc_recall(SC3, SC2))
        self.assertEqual(4./25., sc_recall(SC4, SC2))

        self.assertEqual(4./9. , sc_recall(SC3, SC4))

        self.assertEqual(4./9. , sc_recall(SC4, SC3))



class TestSubspaceClusterPrecision(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1.    , sc_precision(EMPTY_SC, EMPTY_SC))

    def testEmptyResult(self):
        self.assertEqual(1.    , sc_precision(EMPTY_SC, SC1))

    def testEmptyGround(self):
        self.assertEqual(0.    , sc_precision(SC1, EMPTY_SC))

    def testEqual(self):
        self.assertEqual(1.    , sc_precision(SC1, SC1))

    def testMix(self):
        self.assertEqual(1.    , sc_precision(SC1, SC2))
        self.assertEqual(1./9. , sc_precision(SC1, SC3))
        self.assertEqual(0.    , sc_precision(SC1, SC4))

        self.assertEqual(9./25., sc_precision(SC2, SC1))
        self.assertEqual(1./9. , sc_precision(SC3, SC1))
        self.assertEqual(0.    , sc_precision(SC4, SC1))

        self.assertEqual(9./25., sc_precision(SC2, SC3))
        self.assertEqual(4./25., sc_precision(SC2, SC4))

        self.assertEqual(1.   , sc_precision(SC3, SC2))
        self.assertEqual(4./9., sc_precision(SC4, SC2))

        self.assertEqual(4./9., sc_precision(SC3, SC4))

        self.assertEqual(4./9., sc_precision(SC4, SC3))



class TestSubspaceClusterF1(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1.    , sc_f1(EMPTY_SC, EMPTY_SC))

    def testEmptyResult(self):
        self.assertEqual(0.    , sc_f1(EMPTY_SC, SC1))

    def testEmptyGround(self):
        self.assertEqual(0.    , sc_f1(SC1, EMPTY_SC))

    def testEqual(self):
        self.assertEqual(1.    , sc_f1(SC1, SC1))

    def testMix(self):
        self.assertEqual(2 * 9./25. * 1. / (9./25. + 1.)      , sc_f1(SC1, SC2))
        self.assertEqual(1./9.                                , sc_f1(SC1, SC3))
        self.assertEqual(0.                                   , sc_f1(SC1, SC4))

        self.assertEqual(2 * 9./25. * 1. / (9./25. + 1.)      , sc_f1(SC2, SC1))
        self.assertEqual(1./9.                                , sc_f1(SC3, SC1))
        self.assertEqual(0.                                   , sc_f1(SC4, SC1))

        self.assertEqual(2 * 1. * 9./25. / (1. + 9./25.)      , sc_f1(SC2, SC3))
        self.assertEqual(2 * 4./9. * 4./25. / (4./9. + 4./25.), sc_f1(SC2, SC4))

        self.assertEqual(2 * 1. * 9./25. / (1. + 9./25.)      , sc_f1(SC3, SC2))
        self.assertEqual(2 * 4./9. * 4./25. / (4./9. + 4./25.), sc_f1(SC4, SC2))

        self.assertEqual(4./9.                               , sc_f1(SC3, SC4))

        self.assertEqual(4./9.                               , sc_f1(SC4, SC3))



class TestSubspaceClusterRecallRes(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1., sc_recall_res(EMPTY_SET, EMPTY_SET))

    def testEmptyResult(self):
        self.assertEqual(0., sc_recall_res(EMPTY_SET, SCS1))
        self.assertEqual(0., sc_recall_res(EMPTY_SET, SCS2))
        self.assertEqual(0., sc_recall_res(EMPTY_SET, SCS3))
        self.assertEqual(0., sc_recall_res(EMPTY_SET, SCS4))

    def testSingleResultEmptySC(self):
        self.assertEqual(0., sc_recall_res(EMPTY_SCS, SCS1))
        self.assertEqual(0., sc_recall_res(EMPTY_SCS, SCS2))
        self.assertEqual(0., sc_recall_res(EMPTY_SCS, SCS3))
        self.assertEqual(0., sc_recall_res(EMPTY_SCS, SCS4))

    def testEmptyGround(self):
        self.assertEqual(1., sc_recall_res(SCS1, EMPTY_SET))
        self.assertEqual(1., sc_recall_res(SCS2, EMPTY_SET))
        self.assertEqual(1., sc_recall_res(SCS3, EMPTY_SET))
        self.assertEqual(1., sc_recall_res(SCS4, EMPTY_SET))

    def testSingleGroundEmptySc(self):
        self.assertEqual(1., sc_recall_res(SCS1, EMPTY_SCS))
        self.assertEqual(1., sc_recall_res(SCS2, EMPTY_SCS))
        self.assertEqual(1., sc_recall_res(SCS3, EMPTY_SCS))
        self.assertEqual(1., sc_recall_res(SCS4, EMPTY_SCS))

    def testMix(self):
        self.assertEqual((1./3.) * (1. + 1. + 4./9.), sc_recall_res(SCS1, SCS2))
        self.assertEqual((1./2.) * (1. + 4./9.), sc_recall_res(SCS1, SCS3))
        self.assertEqual((1./1.) * (4./9.), sc_recall_res(SCS1, SCS4))

        self.assertEqual((1./3.) * (1. + 1. + 1.), sc_recall_res(SCS2, SCS1))
        self.assertEqual((1./3.) * (1./9. + 9./25. + 1.), sc_recall_res(SCS3, SCS1))
        self.assertEqual((1./3.) * (0. + 4./25. + 4./9.), sc_recall_res(SCS4, SCS1))

        self.assertEqual(1., sc_recall_res(SCS2, SCS3))
        self.assertEqual(1., sc_recall_res(SCS2, SCS4))

        self.assertEqual((1./3.) * (9./25. + 1. + 1.), sc_recall_res(SCS3, SCS2))
        self.assertEqual((1./3.) * (4./25. + 4./9. + 1.), sc_recall_res(SCS4, SCS2))

        self.assertEqual(1., sc_recall_res(SCS3, SCS4))

        self.assertEqual((1./2.) * (4./9. + 1.), sc_recall_res(SCS4, SCS3))



class TestSubspaceClusterPrecisionRes(unittest.TestCase):
    def testEmpty(self):
        self.assertEqual(1., sc_precision_res(EMPTY_SET, EMPTY_SET))

    def testEmptyResult(self):
        self.assertEqual(1., sc_precision_res(EMPTY_SET, SCS1))
        self.assertEqual(1., sc_precision_res(EMPTY_SET, SCS2))
        self.assertEqual(1., sc_precision_res(EMPTY_SET, SCS3))
        self.assertEqual(1., sc_precision_res(EMPTY_SET, SCS4))

    def testSingleResultEmptySC(self):
        self.assertEqual(1., sc_precision_res(EMPTY_SCS, SCS1))
        self.assertEqual(1., sc_precision_res(EMPTY_SCS, SCS2))
        self.assertEqual(1., sc_precision_res(EMPTY_SCS, SCS3))
        self.assertEqual(1., sc_precision_res(EMPTY_SCS, SCS4))

    def testEmptyGround(self):
        self.assertEqual(0., sc_precision_res(SCS1, EMPTY_SET))
        self.assertEqual(0., sc_precision_res(SCS2, EMPTY_SET))
        self.assertEqual(0., sc_precision_res(SCS3, EMPTY_SET))
        self.assertEqual(0., sc_precision_res(SCS4, EMPTY_SET))

    def testSingleGroundEmptySc(self):
        self.assertEqual(0., sc_precision_res(SCS1, EMPTY_SCS))
        self.assertEqual(0., sc_precision_res(SCS2, EMPTY_SCS))
        self.assertEqual(0., sc_precision_res(SCS3, EMPTY_SCS))
        self.assertEqual(0., sc_precision_res(SCS4, EMPTY_SCS))

    def testMix(self):
        self.assertEqual((1. / 3.) * (1. + 1. + 1.), sc_precision_res(SCS1, SCS2))
        self.assertEqual((1. / 3.) * (1. / 9. + 9. / 25. + 1.), sc_precision_res(SCS1, SCS3))
        self.assertEqual((1. / 3.) * (0. + 4. / 25. + 4. / 9.), sc_precision_res(SCS1, SCS4))

        self.assertEqual((1. / 3.) * (1. + 1. + 4. / 9.), sc_precision_res(SCS2, SCS1))
        self.assertEqual((1. / 2.) * (1. + 4. / 9.), sc_precision_res(SCS3, SCS1))
        self.assertEqual((1. / 1.) * (4. / 9.), sc_precision_res(SCS4, SCS1))

        self.assertEqual((1. / 3.) * (9. / 25. + 1. + 1.), sc_precision_res(SCS2, SCS3))
        self.assertEqual((1. / 3.) * (4. / 25. + 4. / 9. + 1.), sc_precision_res(SCS2, SCS4))

        self.assertEqual(1., sc_precision_res(SCS3, SCS2))
        self.assertEqual(1., sc_precision_res(SCS4, SCS2))

        self.assertEqual((1. / 2.) * (4. / 9. + 1.), sc_precision_res(SCS3, SCS4))

        self.assertEqual(1., sc_precision_res(SCS4, SCS3))



class TestSubspaceClusterF1RRes(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1., sc_f1R_res(EMPTY_SET, EMPTY_SET))

    def testEmptyResult(self):
        self.assertEqual(0., sc_f1R_res(EMPTY_SET, SCS1))
        self.assertEqual(0., sc_f1R_res(EMPTY_SET, SCS2))
        self.assertEqual(0., sc_f1R_res(EMPTY_SET, SCS3))
        self.assertEqual(0., sc_f1R_res(EMPTY_SET, SCS4))

    def testSingleResultEmptySC(self):
        self.assertEqual(0., sc_f1R_res(EMPTY_SCS, SCS1))
        self.assertEqual(0., sc_f1R_res(EMPTY_SCS, SCS2))
        self.assertEqual(0., sc_f1R_res(EMPTY_SCS, SCS3))
        self.assertEqual(0., sc_f1R_res(EMPTY_SCS, SCS4))

    def testEmptyGround(self):
        self.assertEqual(1., sc_f1R_res(SCS1, EMPTY_SET))
        self.assertEqual(1., sc_f1R_res(SCS2, EMPTY_SET))
        self.assertEqual(1., sc_f1R_res(SCS3, EMPTY_SET))
        self.assertEqual(1., sc_f1R_res(SCS4, EMPTY_SET))

    def testSingleGroundEmptySc(self):
        self.assertEqual(0., sc_f1R_res(SCS1, EMPTY_SCS))
        self.assertEqual(0., sc_f1R_res(SCS2, EMPTY_SCS))
        self.assertEqual(0., sc_f1R_res(SCS3, EMPTY_SCS))
        self.assertEqual(0., sc_f1R_res(SCS4, EMPTY_SCS))

    def testMix(self):
        self.assertEqual((1./3.) * (1. + 1. + (4./9.)), sc_f1R_res(SCS1, SCS2))
        self.assertEqual((1./2.) * (1. + 4./9.), sc_f1R_res(SCS1, SCS3))
        self.assertEqual(4./9., sc_f1R_res(SCS1, SCS4))

        self.assertEqual((1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.), sc_f1R_res(SCS2, SCS1))
        self.assertEqual((1./3.) * (1./9. + ((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1.), sc_f1R_res(SCS3, SCS1))
        self.assertEqual((1./3.) * (0. + ((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9.), sc_f1R_res(SCS4, SCS1))

        self.assertEqual(1., sc_f1R_res(SCS2, SCS3))
        self.assertEqual(1., sc_f1R_res(SCS2, SCS4))

        self.assertEqual((1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.), sc_f1R_res(SCS3, SCS2))
        self.assertEqual((1./3.) * (((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9. + 1.), sc_f1R_res(SCS4, SCS2))

        self.assertEqual(1., sc_f1R_res(SCS3, SCS4))

        self.assertEqual((1./2.) * (4./9. + 1.), sc_f1R_res(SCS4, SCS3))



class TestSubspaceClusterF1PRes(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1., sc_f1P_res(EMPTY_SET, EMPTY_SET))

    def testEmptyResult(self):
        self.assertEqual(1., sc_f1P_res(EMPTY_SET, SCS1))
        self.assertEqual(1., sc_f1P_res(EMPTY_SET, SCS2))
        self.assertEqual(1., sc_f1P_res(EMPTY_SET, SCS3))
        self.assertEqual(1., sc_f1P_res(EMPTY_SET, SCS4))

    def testSingleResultEmptySC(self):
        self.assertEqual(0., sc_f1P_res(EMPTY_SCS, SCS1))
        self.assertEqual(0., sc_f1P_res(EMPTY_SCS, SCS2))
        self.assertEqual(0., sc_f1P_res(EMPTY_SCS, SCS3))
        self.assertEqual(0., sc_f1P_res(EMPTY_SCS, SCS4))

    def testEmptyGround(self):
        self.assertEqual(0., sc_f1P_res(SCS1, EMPTY_SET))
        self.assertEqual(0., sc_f1P_res(SCS2, EMPTY_SET))
        self.assertEqual(0., sc_f1P_res(SCS3, EMPTY_SET))
        self.assertEqual(0., sc_f1P_res(SCS4, EMPTY_SET))

    def testSingleGroundEmptySc(self):
        self.assertEqual(0., sc_f1P_res(SCS1, EMPTY_SCS))
        self.assertEqual(0., sc_f1P_res(SCS2, EMPTY_SCS))
        self.assertEqual(0., sc_f1P_res(SCS3, EMPTY_SCS))
        self.assertEqual(0., sc_f1P_res(SCS4, EMPTY_SCS))

    def testMix(self):
        self.assertEqual((1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.), sc_f1P_res(SCS1, SCS2))
        self.assertEqual((1./3.) * (1./9. + ((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1.), sc_f1P_res(SCS1, SCS3))
        self.assertEqual((1./3.) * (0. + ((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9.), sc_f1P_res(SCS1, SCS4))

        self.assertEqual((1./3.) * (1. + 1. + (4./9.)), sc_f1P_res(SCS2, SCS1))
        self.assertEqual((1./2.) * (1. + 4./9.), sc_f1P_res(SCS3, SCS1))
        self.assertEqual(4./9., sc_f1P_res(SCS4, SCS1))

        self.assertEqual((1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.), sc_f1P_res(SCS2, SCS3))
        self.assertEqual((1./3.) * (((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9. + 1.), sc_f1P_res(SCS2, SCS4))

        self.assertEqual(1., sc_f1P_res(SCS3, SCS2))
        self.assertEqual(1., sc_f1P_res(SCS4, SCS2))

        self.assertEqual((1./2.) * (4./9. + 1.), sc_f1P_res(SCS3, SCS4))

        self.assertEqual(1., sc_f1P_res(SCS4, SCS3))



class TestSubspaceClusterE4SCRes(unittest.TestCase):

    def testEmpty(self):
        self.assertEqual(1., e4sc_res(EMPTY_SET, EMPTY_SET))

    def testEmptyResult(self):
        self.assertEqual(0., e4sc_res(EMPTY_SET, SCS1))
        self.assertEqual(0., e4sc_res(EMPTY_SET, SCS2))
        self.assertEqual(0., e4sc_res(EMPTY_SET, SCS3))
        self.assertEqual(0., e4sc_res(EMPTY_SET, SCS4))

    def testSingleResultEmptySC(self):
        self.assertEqual(0., e4sc_res(EMPTY_SCS, SCS1))
        self.assertEqual(0., e4sc_res(EMPTY_SCS, SCS2))
        self.assertEqual(0., e4sc_res(EMPTY_SCS, SCS3))
        self.assertEqual(0., e4sc_res(EMPTY_SCS, SCS4))

    def testEmptyGround(self):
        self.assertEqual(0., e4sc_res(SCS1, EMPTY_SET))
        self.assertEqual(0., e4sc_res(SCS2, EMPTY_SET))
        self.assertEqual(0., e4sc_res(SCS3, EMPTY_SET))
        self.assertEqual(0., e4sc_res(SCS4, EMPTY_SET))

    def testSingleGroundEmptySc(self):
        self.assertEqual(0., e4sc_res(SCS1, EMPTY_SCS))
        self.assertEqual(0., e4sc_res(SCS2, EMPTY_SCS))
        self.assertEqual(0., e4sc_res(SCS3, EMPTY_SCS))
        self.assertEqual(0., e4sc_res(SCS4, EMPTY_SCS))

    def testMix(self):
        rec = (1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.)
        pre = (1./3.) * (1. + 1. + (4./9.))
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS1, SCS2))
        self.assertEqual(e4sc, e4sc_res(SCS2, SCS1))

        rec = (1./3.) * (1./9. + ((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1.)
        pre = (1./2.) * (1. + 4./9.)
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS1, SCS3))
        self.assertEqual(e4sc, e4sc_res(SCS3, SCS1))

        rec = (1./3.) * (0. + ((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9.)
        pre = 4./9.
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS1, SCS4))
        self.assertEqual(e4sc, e4sc_res(SCS4, SCS1))

        rec = (1./3.) * (((2. * 1. * (9./25.)) / (1. + 9./25.)) + 1. + 1.)
        pre = 1.
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS2, SCS3))
        self.assertEqual(e4sc, e4sc_res(SCS3, SCS2))

        rec = (1./3.) * (((2. * (4./9.) * (4./25.)) / (4./9. + 4./25.)) + 4./9. + 1.)
        pre = 1.
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS2, SCS4))
        self.assertEqual(e4sc, e4sc_res(SCS4, SCS2))

        rec = (1./2.) * (4./9. + 1.)
        pre = 1.
        e4sc = (2. * rec * pre) / (rec + pre)
        self.assertEqual(e4sc, e4sc_res(SCS3, SCS4))
        self.assertEqual(e4sc, e4sc_res(SCS4, SCS3))

if __name__ == "__main__":
    unittest.main()
