#!/usr/bin/env python

"""
    Wrapper class for running Random Tile Samping.

    Original implementation of RTS is provided by S. Moens and is written in Java. This module provides a simple
    interface to run the algorithm and read the output as itemsets.
"""

import os, sys, tempfile

from subprocess import call

RTS_JAR = "bin/rts-2.0.0-jar-with-dependencies.jar"

'''
Samples a collection of n large tiles
'''
def rts(transactions, n=100):
    print("Sampling TILES [n=%d]" % (n))

    transactionsFile = tempfile.NamedTemporaryFile().name

    with open(transactionsFile, "w") as f:
        for transaction in transactions:
            if type(transaction) is list:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction])))
            else:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction[0]])))

    tilesFile = tempfile.NamedTemporaryFile().name

    call(["java", "-jar", RTS_JAR, "-o%s" % (tilesFile), transactionsFile, "%d" % (n)])

    fimms = [[int(item) for item in line.rstrip().split(" ")] for line in open(tilesFile) if line.rstrip() != ""]

    os.remove(transactionsFile)
    os.remove(tilesFile)

    return fimms


# CL #########################################################

def _usage():
    print("""
COMMAND
    $ python rmis.py <-f file> (-n num_res)

OPTIONS
    - file: space separated transactions file with integers as items

    - num_res: number of results (default: 100)
""", file=sys.stderr)


if __name__ == "__main__":
    import getopt

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:n:", ['help'])
    except getopt.GetoptError:
        _usage()
        sys.exit(2)

    file = None
    num_res = 100

    for o, a in opts:
        if o in ('-h', '--help'):
            _usage()
            sys.exit()
        elif o == '-f':
            file = a
        elif o == '-n':
            num_res = int(a)

    if file == None:
        print("""Please specify an input file using -f""", file=sys.stderr)
        _usage()
        sys.exit(2)

    if num_res == None:
        print("""Please specify an number of results using -n""", file=sys.stderr)
        _usage()
        sys.exit(2)

    transactions = [[int(v) for v in row.strip().split(" ")] for row in open(file)]

    fimms = rts(transactions, n=num_res)

    for fimm in fimms:
        print(" ".join(["%d" % (i) for i in fimm]))
