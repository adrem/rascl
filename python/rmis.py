#!/usr/bin/env python

"""
    Wrapper class for running Randomised Maximal Itemsets.

    Original implementation of RMIS is provided by S. Moens and is written in Java. This module provides a simple
    interface to run the algorithm and read the output as itemsets.
"""

import os, sys, tempfile

from subprocess import call

RMIS_JAR = "bin/rmis-3.0.0-SNAPSHOT-jar-with-dependencies.jar"

'''
Samples a collection of n maximal frequent itemsets from the transactions that satisfy the threshold s
'''
def rmis(transactions, s, n=100, q=1, p=1, w=1):
    print("Sampling itemsets using RMIS [s=%d, n=%d, q=%d, p=%d, w=%d]" % (s, n, q, p, w))

    transactionsFile = tempfile.NamedTemporaryFile().name

    with open(transactionsFile, "w") as f:
        for transaction in transactions:
            if type(transaction) is list:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction])))
            else:
                f.write("%s\n" % (" ".join(["%d" % (v) for v in transaction[0]])))

    fimmFile = tempfile.NamedTemporaryFile().name

    call(["java", "-jar", RMIS_JAR, "-q%d" %(q), "-p%d" %(p), "-w%d" %(w), "-o%s" % (fimmFile), transactionsFile, "%d" % (s), "%d" % (n)])

    fimms = [[int(item) for item in line.rstrip().split(" ")] for line in open(fimmFile) if line.rstrip() != ""]

    os.remove(transactionsFile)
    os.remove(fimmFile)

    return fimms

# CL #########################################################

def _usage():
    print("""
COMMAND
    $ python rmis.py <-f file> <-s min_sup> (-n num_res) (-q qual_func) (-p prun_func) (-w weights)

OPTIONS
    - file: space separated transactions file with integers as items
    
    - min_sup: minimum support value used for maximal frequent itemset sampling

    - num_res: number of results (default: 100)

    - qual_func: specifies the quality measure (default: Support)
        1: Support
        2: Average Subset Support
        
    - prun_func: specifies the approximation measure (default: Support)
        1: Support (ItemSet Measures)
        2: Negative Support (ItemSet Measures)
        104: Lift (Symmetric Rule Measures)
        203: Confidence (Asymmetric Rule Measures)
    
    - weights: specifies the weighting scheme (default: none)
        1: none
        2: additive
        3: multiplicative
        4: adaptive
""", file=sys.stderr)


if __name__ == "__main__":
    import getopt

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:s:n:q:p:w:", ['help'])
    except getopt.GetoptError:
        _usage()
        sys.exit(2)

    file = None
    min_sup = None
    num_res = 100
    qual_func = 1
    prun_func = 1
    weights = 1

    for o, a in opts:
        if o in ('-h', '--help'):
            _usage()
            sys.exit()
        elif o == '-f':
            file = a
        elif o == '-s':
            min_sup = int(a)
        elif o == '-n':
            num_res = int(a)
        elif o == '-q':
            qual_func = int(a)
        elif o == '-p':
            prun_func = int(a)
        elif o == '-w':
            weights = int(a)

    if file == None:
        print("""Please specify an input file using -f""", file=sys.stderr)
        _usage()
        sys.exit(2)

    if min_sup == None:
        print("""Please specify an minimum support using -s""", file=sys.stderr)
        _usage()
        sys.exit(2)

    transactions = [[int(v) for v in row.strip().split(" ")] for row in open(file)]

    fimms = rmis(transactions, s=min_sup, n=num_res, q=qual_func, p=prun_func, w=weights)

    for fimm in fimms:
        print(" ".join(["%d" %(i) for i in fimm]))
