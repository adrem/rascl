
class SubspaceCluster:

    def __init__(self, objects, dimensions):
        self.objects = objects
        self.dimensions = dimensions

    def __repr__(self):
        return "SC[#o%d;#d%d;%s;%s]" %(len(self.objects), len(self.dimensions), str(self.objects), str(self.dimensions))

    def __str__(self):
        return "SC[#o%d;#d%d;%s;%s]" %(len(self.objects), len(self.dimensions), str(self.objects), str(self.dimensions))

def print_clusters(scs):
    print("Cluster count: %d" % (len(scs)))
    print()

    for i, sc in enumerate(scs):
        print("Cluster%d: %d %d" % (i + 1, len(sc.objects), len(sc.dimensions)))
        print("Objects: %s" % (" ".join(["%d" % (o) for o in sc.objects])))
        print("Dimensions: %s" % (" ".join(["%d" % (d) for d in sc.dimensions])))
        print()
