"""
    Implements utility and drawing functions for RaSCl and RaSClR.
"""

import random

from matplotlib import pyplot as plt

import pandas as pd

from pandas.plotting import andrews_curves, parallel_coordinates

import rmis, rts

from dimensionSampler import dimensionSampler
from measures import sc_precision


def _split_dataframes(rows, fimm):
    notFimm = [i for i in range(0, rows.shape[0]) if i not in fimm]

    d = pd.DataFrame(rows.iloc[notFimm,:])
    d = d.reset_index(drop=True)
    d = d.join(pd.DataFrame(data=["no" for o in notFimm], columns=["cluster"]))

    dc = pd.DataFrame(rows.iloc[fimm,:])
    dc = dc.reset_index(drop=True)
    dc = dc.join(pd.DataFrame(data=["yes" for o in fimm], columns=["cluster"]))

    return d, dc


def draw_andrews_curves(rows, fimm, ax):
    d, dc = _split_dataframes(rows, fimm)

    andrews_curves(d, 'cluster', color=['b'], ax=ax)
    andrews_curves(dc, 'cluster', color=['r'], ax=ax)


def draw_centers(rows, samples, centers, dimensions, ax):
    x = rows[dimensions[0]]
    y = rows[dimensions[1]]

    ax.scatter(x, y, c="b")

    x = rows.iloc[samples, dimensions[0]]
    y = rows.iloc[samples, dimensions[1]]

    ax.scatter(x, y, c="g")

    x = [s[0] for s in centers]
    y = [s[1] for s in centers] if len(dimensions) > 1 else x

    ax.scatter(x, y, c="r")

    ax.set_xlabel("Dim %d" %(dimensions[0] + 1))
    ax.set_ylabel("Dim %d" %(dimensions[1] + 1))


def draw_fis(rows, fis):
    for ix, fi in enumerate(fis):
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))

        draw_andrews_curves(rows, fi, ax1)
        draw_parallel_coordinates(rows, fi, ax2)

        plt.show()

        if ix == 4:
            break


def draw_labeled(data, labels, dimensions, ax):
    labeled = {label:[] for label in labels}

    for i in range(0, len(data)):
        labeled[labels[i]].append(data[i])

    for i, label in enumerate(sorted(labeled.keys())):
        x = [row[0] for row in labeled[label]]
        y = [row[1] for row in labeled[label]] if len(data[0]) > 1 else x

        ax.scatter(x, y)

        ax.set_xlabel("Dim %d" %(dimensions[0] + 1))
        ax.set_ylabel("Dim %d" %(dimensions[1] + 1))


def draw_parallel_coordinates(rows, fimm, ax):
    d, dc = _split_dataframes(rows, fimm)

    parallel_coordinates(d, 'cluster', color=['b'], ax=ax)
    parallel_coordinates(dc, 'cluster', color=['r'], ax=ax)


def draw_samples(rows, samples):
    for i, sample in enumerate(samples):
        fig, ax = plt.subplots(1, 1, figsize=(5,5))

        x = rows[0]
        y = rows[1]

        ax.scatter(x, y, c="b")

        x = rows.iloc[sample[0], 0]
        y = rows.iloc[sample[0], 1]

        ax.scatter(x, y, c="r")

        ax.set_xlabel("Dim 1")
        ax.set_ylabel("Dim 2")

        plt.show()

        if i == 4:
            break


def expand_fimm_using_drop(items, fimm, drop=0.05):
    suppSet = set([])

    for i, ii in enumerate(fimm):
        if i == 0:
            suppSet |= items[ii]
        else:
            suppSet &= items[ii]

    ffimm = [i for i in fimm]

    for item in items:
        if not item in fimm:
            ssuppSet = set(suppSet)
            ssuppSet &= items[item]

            if 1 - (1.*len(ssuppSet)/len(suppSet)) <= drop:
                ffimm.append(item)

    return ffimm


'''
Generates n samples.

A sample contains a set of k data points and a list of dimensions.
The number of dimensions is between 1 and sqrt(dimensions).
'''
def generate_new_samples(rows, n, k, sample_dim_count):
    rowCount = len(rows)

    samples = []

    dim_sampler = dimensionSampler(rows, sample_dim_count)

    # create n samples
    for i in range(0, n):
        # sample K data points
        dataPoints = random.sample(range(0, rowCount), k)
        dimensions = dim_sampler.sample()

        samples.append([dataPoints, dimensions])

    return samples


def get_fis(transactions, s, m):
    if s.startswith("rmis"):
        params = s.split("|")[1:]
        supp = int(params[-1])

        q = 1
        p = 1
        w = 1

        for i in range(0, len(params) - 1):
            param = params[i].split("-")
            if param[0] == "q": q = int(param[1])
            if param[0] == "p": p = int(param[1])
            if param[0] == "w": w = int(param[1])

        return rmis.rmis(transactions, s=supp, n=m, q=q, p=p, w=w)
    elif s.startswith("rts"):
        return rts.rts(transactions, n=m)

    return rmis.rmis(transactions, s=s, n=m)


def _small_overlap(newCluster, existingClusters, maxOverlap=0.25):
    for existingCluster in existingClusters:
        if sc_precision(newCluster, existingCluster) > maxOverlap:
            return False

    return True


def top_k_clusters(scs, top_k=10, max_overlap=0.25):
    top_k_clusters = []

    for sc in scs:
        if _small_overlap(sc, top_k_clusters, maxOverlap=max_overlap):
            if len(sc.objects) > 10:
                top_k_clusters.append(sc)

        if len(top_k_clusters) == top_k:
            break

    return top_k_clusters


def transactions_to_items(transactions):
    items = {}

    for i, transaction in enumerate(transactions):
        if type(transaction) is list:
            for item in transaction:
                if not item in items: items[item] = set()
                items[item].add(i)
        else:
            for item in transaction[0]:
                if not item in items: items[item] = set()
                items[item].add(i)

    return items
