# Creates object comparison graphs between SC_{R} and SC_{KMEANS} for various measures

echo "Create comparison graphs 'dbsizescale'"
python ../../draw_graphs_comparison_objects.py dbsizescale.conf

echo "Create comparison graphs 'dimscale'"
python ../../draw_graphs_comparison_objects.py dimscale.conf

echo "Create comparison graphs 'noisescale'"
python ../../draw_graphs_comparison_objects.py noisescale.conf