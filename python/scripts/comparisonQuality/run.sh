# Runs experiments for some fixed settings

# RASCLR

echo "Running RASCLR experiments for dbsizescale (10, 100)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_dbsizescale_10_100.json

echo "Running RASCLR experiments for dbsizescale (20, 200)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_dbsizescale_20_200.json

echo "Running RASCLR experiments for dimscale (10, 100)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_dimscale_10_100.json

echo "Running RASCLR experiments for dimscale (20, 200)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_dimscale_20_200.json

echo "Running RASCLR experiments for noisescale (10, 100)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_noisescale_10_100.json

echo "Running RASCLR experiments for noisescale (20, 200)"
python exp/exp_runner_rasclr.py scripts/comparisonQuality/rasclr_noisescale_20_200.json


# RASCL

echo "Running RASCL experiments for dbsizescale (10, 100)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_dbsizescale_10_100.json

echo "Running RASCL experiments for dbsizescale (20, 200)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_dbsizescale_20_200.json

echo "Running RASCL experiments for dimscale (10, 100)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_dimscale_10_100.json

echo "Running RASCL experiments for dimscale (20, 200)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_dimscale_20_200.json

echo "Running RASCL experiments for noisescale (10, 100)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_noisescale_10_100.json

echo "Running RASCL experiments for noisescale (20, 200)"
python exp/exp_runner_rascl.py scripts/comparisonQuality/rascl_noisescale_20_200.json


# CartiClus

python exp/exp_carticlus.py


# ProClus

python exp/exp_sscs.py


# Creates object comparison graphs between RASCLR, RASCL, CartiClus and ProClus for various measures

echo "Create comparison graphs 'dbsizescale'"
python exp/draw_graphs_comparison.py scripts/comparisonQuality/dbsizescale.conf

echo "Create comparison graphs 'dimscale'"
python exp/draw_graphs_comparison.py scripts/comparisonQuality/dimscale.conf

echo "Create comparison graphs 'noisescale'"
python exp/draw_graphs_comparison.py scripts/comparisonQuality/noisescale.conf
