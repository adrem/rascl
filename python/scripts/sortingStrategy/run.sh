echo "Create graphs 'dbsizescale'"
python exp/exp_sorting_strategy.py scripts/sortingStrategy/rascl_dbsizescale.json

echo "Create graphs 'dimscale'"
python exp/exp_sorting_strategy.py scripts/sortingStrategy/rascl_dimscale.json

echo "Create graphs 'noisescale'"
python exp/exp_sorting_strategy.py scripts/sortingStrategy/rascl_noisescale.json
