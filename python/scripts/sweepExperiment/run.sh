# Runs experiments for some fixed settings

# RASCLR

echo "Running RASCLR sweep experiments for dbsizescale"
python exp/exp_runner_rasclr_sweep.py scripts/sweepExperiment/rasclr_dbsizescale.json

echo "Running RASCLR sweep experiments for dimscale"
python exp/exp_runner_rasclr_sweep.py scripts/sweepExperiment/rasclr_dimscale.json

echo "Running RASCLR sweep experiments for noisescale"
python exp/exp_runner_rasclr_sweep.py scripts/sweepExperiment/rasclr_noisescale.json


# RASCL

echo "Running RASCL sweep experiments for dbsizescale"
python exp/exp_runner_rascl_sweep.py scripts/sweepExperiment/rascl_dbsizescale.json

echo "Running RASCL sweep experiments for dimscale"
python exp/exp_runner_rascl_sweep.py scripts/sweepExperiment/rascl_dimscale.json

echo "Running RASCL sweep experiments for noisescale"
python exp/exp_runner_rascl_sweep.py scripts/sweepExperiment/rascl_noisescale.json


# CartiClus

python exp/exp_carticlus_sweep.py


# ProClus

python exp/exp_sscs_sweep.py


# Creates heatmaps for RASCLR, RASCL, CartiClus and ProClus for various measures

echo "Create sweep graphs RASCLR"
python exp/draw_sc_sweep_graphs.py experiment_results/sweepExperiment/rasclr experiment_results_fig/sweepExperiment/rasclr

echo "Create sweep graphs RASCL"
python exp/draw_sc_sweep_graphs.py experiment_results/sweepExperiment/rascl experiment_results_fig/sweepExperiment/rascl

echo "Create sweep graphs CartiClus"
python exp/draw_carticlus_sweep_graphs.py experiment_results/sweepExperiment/carticlus experiment_results_fig/sweepExperiment/carticlus

echo "Create sweep graphs ProClus"
python exp/draw_sscs_sweep_graphs.py experiment_results/sweepExperiment/proclus experiment_results_fig/sweepExperiments/proclus

