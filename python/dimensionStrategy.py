
import random, numpy

from scipy import spatial

from collections import Counter

ALG_VOTING = "voting"
ALG_ALPHA = "alpha"
ALG_ALPHA2 = "alpha2"
ALG_STDEV = "stdev"
ALG_STDEV2 = "stdev2"
ALG_DENSITY = "density"


def infer_dimensions(d, fi, dims, dps=[]):
    if d == ALG_VOTING:
        return voting(dims)
    elif d.startswith(ALG_ALPHA):
        return alpha_occurrence(dims, float(d.split("|")[1]))
    elif d.startswith(ALG_ALPHA2):
        return alpha2_occurrence(dims, float(d.split("|")[1]))
    elif d.startswith(ALG_STDEV):
        return stdev_based(dps, float(d.split("|")[1]))
    elif d.startswith(ALG_STDEV2):
        return stdev_based2(dps, float(d.split("|")[1]))
    elif d.startswith(ALG_DENSITY):
        return density_based(dps, float(d.split("|")[1]))
    return []


def voting(dims):
    if len(dims) == 0:
        return []

    counts = Counter(["_".join("%d" % (d) for d in sorted(dd)) for dd in dims])

    maxValue = counts.most_common()[0][1]

    maxDims = [[int(d) for d in mc[0].split("_")] for mc in counts.most_common() if mc[1] == maxValue]

    if len(maxDims) == 1:
        return maxDims[0]

    return maxDims[random.randint(0, len(maxDims) - 1)]


def alpha_occurrence(dims, alpha=0.5):
    if len(dims) == 0:
        return []

    counts = Counter([item for sublist in dims for item in sublist])

    minValue = len(dims) * alpha

    return [d for d in counts.keys() if counts[d] >= minValue]


def alpha2_occurrence(dims, alpha=1):
    if len(dims) == 0:
        return []

    counts = Counter([item for sublist in dims for item in sublist])

    return [d for d in counts.keys() if counts[d] >= alpha]


def stdev_based(dps, beta):
    stdev = numpy.std(dps, 0)
    # print stdev
    return [i for i, v in enumerate(stdev) if v <= beta]


def stdev_based2(dps, beta):
    if len(dps) == 0:
        return []

    p25 = numpy.percentile(dps, 25, 0)
    p75 = numpy.percentile(dps, 75, 0)

    stdev = []

    for i in range(0, len(dps[0])):
        dpp = [dp[i] for dp in dps if dp[i] >= p25[i] and dp[i] <= p75[i]]
        stdev.append(numpy.std(dpp))

    # print "stdev_based2", stdev
    return [i for i, v in enumerate(stdev) if v <= beta]


def density_based(dps, beta):
    stdev = numpy.std(dps, 0)
    stdev /= len(dps)
    return [i for i, v in enumerate(stdev) if v <= beta]


# Tests

def votingTest():
    if voting([]) != []:
        print("[voting] Empty list test failed!!")
        exit()

    if voting([[0, 1]]) != [0,1]:
        print("[voting] Single list test failed")
        exit()

    if voting([[0,1], [0,1], [0], [2,3], [2], [3], [4]]) != [0,1]:
        print("[voting] Multi list test failed")
        exit()

    for i in range(0, 50):
        maxDim = voting([[0,1], [0,1], [0], [2,3], [2,3], [2], [3], [4]])

        if maxDim != [2,3] and maxDim != [0,1]:
            print("[voting] Multi list same occurrence test failed")
            exit()

def alphaOccurrenceTest():
    if alpha_occurrence([], 0.25) != []:
        print("[alpha|0.25] Empty list test failed!!")
        exit()

    if alpha_occurrence([[0,1]], 0.25) != [0,1]:
        print("[alpha|0.25] Single list test failed")
        exit()

    if alpha_occurrence([[0,1]], 0.25) != [0,1]:
        print("[alpha|0.25] Single list test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]], 0.25) != [0,1]:
        print("[alpha|0.25] Multi list1 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]], 0.25) != [0,1]:
        print("[alpha|0.25] Multi list2 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [2,3], [2,3], [3], [4], [4,5], [6,7]], 0.25) != [0,1,3]:
        print("[alpha|0.25] Multi list3 test failed")
        exit()


    if alpha_occurrence([], 0.50) != []:
        print("[alpha|0.50] Empty list test failed!!")
        exit()

    if alpha_occurrence([[0, 1]], 0.50) != [0, 1]:
        print("[alpha|0.50] Single list test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]], 0.50) != [0,1]:
        print("[alpha|0.50] Multi list1 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]], 0.50) != [1]:
        print("[alpha|0.50] Multi list2 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [2, 3], [2,3], [3], [4], [4,5], [6,7]], 0.50) != []:
        print("[alpha|0.50] Multi list3 test failed")
        exit()


    if alpha_occurrence([], 0.75) != []:
        print("[alpha|0.75] Empty list test failed!!")
        exit()

    if alpha_occurrence([[0, 1]], 0.75) != [0, 1]:
        print("[alpha|0.75] Single list test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0,1], [0,2,3], [0,2], [0,1,3], [0,4], [0,4,5], [1,6,7], [8]], 0.75) != [0]:
        print("[alpha|0.75] Multi list1 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [1,2,3], [2], [3], [4], [1,4,5], [6,7]], 0.75) != []:
        print("[alpha|0.75] Multi list2 test failed")
        exit()

    if alpha_occurrence([[0,1], [0,1], [0], [1], [2, 3], [2,3], [3], [4], [4,5], [6,7]], 0.75) != []:
        print("[alpha|0.75] Multi list3 test failed")
        exit()

if False:
    print("Testing dimension strategies")
    votingTest()
    alphaOccurrenceTest()