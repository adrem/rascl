
import pickle, shutil

import numpy as np
from scipy.spatial import cKDTree
from scipy.stats import chisquare

from matplotlib import pyplot as plt
from sklearn import *

from clustering import SubspaceCluster
from dataStore import *
from dataUtils import *
from dimensionStrategy import *
from sampling import *

from rascl_utils import draw_andrews_curves, draw_centers, draw_labeled, draw_parallel_coordinates, draw_samples, expand_fimm_using_drop, generate_new_samples, get_fis, transactions_to_items

OUTPUT_DIR = "output/sc_general"

VALUE_VARIABLE = "variable"

N = 1000
K = 100
KK = 10
S = 100
M = 10

def drawSamples(rows, samples):
    for i, sample in enumerate(samples):
        print("dims: ", sample[1])
        d1 = sample[1][0]
        d2 = sample[1][1] if len(sample[1]) > 1 else d1

        x = [row[d1] for row in rows]
        y = [row[d2] for row in rows]

        plt.scatter(x, y, c="b")

        x = [s[d1] for s in sample[0]]
        y = [s[d2] for s in sample[0]]

        plt.scatter(x, y, c="r")

        plt.show()

        if i == 5:
            break


def drawCenters(rows, samples, centers, dimensions):
    x = [row[dimensions[0]] for row in rows]
    y = [row[dimensions[1]] for row in rows] if len(dimensions) > 1 else x

    plt.scatter(x, y, c="b")

    x = [s[dimensions[0]] for s in samples]
    y = [s[dimensions[1]] for s in samples] if len(dimensions) > 1 else x

    plt.scatter(x, y, c="g")

    x = [s[0] for s in centers]
    y = [s[1] for s in centers] if len(dimensions) > 1 else x

    plt.scatter(x, y, c="r")

    plt.show()

colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

def drawLabeled(data, labels):
    labeled = {label:[] for label in labels}

    for i in range(0, len(data)):
        labeled[labels[i]].append(data[i])

    for i, label in enumerate(sorted(labeled.keys())):
        x = [row[0] for row in labeled[label]]
        y = [row[1] for row in labeled[label]] if len(data[0]) > 1 else x

        plt.scatter(x, y)

    plt.show()

def drawRows(rows, fimm, dimCounts):
    dims = [(dim, dimCounts[dim]) for dim in dimCounts]
    dims = sorted(dims, key=lambda dim: dim[1], reverse=True)

    for i in range(1, len(dims)+1):
        if i >= len(dims):
            continue

        x = [row[dims[0][0]] for row in rows]
        y = [row[dims[i if len(dims) > 1 else 0][0]] for row in rows]

        plt.scatter(x, y, c="b")

        x = [rows[item][dims[0][0]] for item in fimm]
        y = [rows[item][dims[i if len(dims) > 1 else 0][0]] for item in fimm]

        plt.scatter(x, y, c="r")

        plt.show()

        break


'''
Cleans up the temporary files and creates a new tmp directory
'''
def cleanupFiles(dataKey):
    if os.path.isdir("tmp"):
        shutil.rmtree("tmp")

    outputDir = "%s/%s" %(OUTPUT_DIR, dataKey)
    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)


'''
Generates n samples.

A sample contains a set of k data points and a list of dimensions.
The number of dimensions is between 1 and sqrt(dimensions).
'''
def generateSamples(dataKey, rows, parameters, run=1, printNewSamples=False, drawNewSamples=False, useCache=False):
    print ("Getting samples [N=%d, K=%d]" %(parameters["n"], parameters["k"]))

    pickleFile = "%s/%s/run%d_samples_n%d_k%d_sampleDimCount%s.pickle" % (OUTPUT_DIR, dataKey, run, parameters["n"], parameters["k"], "%d" %(parameters["sampleDimCount"]) if isinstance(parameters["sampleDimCount"], int) else parameters["sampleDimCount"])

    if useCache and os.path.isfile(pickleFile):
        print ("Pickle file exists, reading samples from pickle")
        samples = pickle.load(open(pickleFile))
    else:
        rowCount = len(rows)
        dimensionCount = len(rows[0])

        samples = []

        if parameters["sampleDimCount"] != VALUE_VARIABLE:
            weights = []

            for j in range(0, len(rows[0])):
                weights.append(chisquare(numpy.histogram([row[j] for row in rows])[0])[0])

            weights = [(1. if weight < 10 else 5.) for weight in weights]

        theSum = sum(weights)
        weights = [weight / theSum for weight in weights]

        #create n samples
        for i in range(0, parameters["n"]):
            #sample K data points
            dataPoints = set(random.sample(range(0, rowCount), parameters["k"]))

            if parameters["sampleDimCount"] == VALUE_VARIABLE:
                # sample 1 to sqrt(dimensionCount) dimensions
                dimensions = uniformSubsetWithMaxLength(range(0, dimensionCount), max([int(math.sqrt(dimensionCount)), 2]))
            else:
                dimensions = weightedSubsetWithLength(range(0, dimensionCount), weights, parameters["sampleDimCount"])

            samples.append(([rows[j] for j in [dp for dp in dataPoints]], dimensions, dataPoints))

        if useCache:
            pickle.dump(samples, open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    if printNewSamples:
        for sample in samples:
            for s in sample[0]:
                print (s)
            print ("\tdimensions %s" %(str(sample[1])))
            print ("\tpoints %s" %(str(sample[2])))

    if drawNewSamples:
        drawSamples(rows, samples)

    return samples

def getClusterer(parameters):
    if parameters["clusterer"] == "kmeans":
        return cluster.KMeans(n_clusters=parameters["clusterer_n_clusters"], init="k-means++", n_init=1)
    elif parameters["clusterer"] == "agglomerative":
        return cluster.AgglomerativeClustering(n_clusters=parameters["clusterer_n_clusters"])
    elif parameters["clusterer"] == "birch":
        return cluster.Birch(n_clusters=parameters["clusterer_n_clusters"])
    elif parameters["clusterer"] == "dbscan":
        return cluster.DBSCAN()
    elif parameters["clusterer"] == "optics":
        return cluster.OPTICS()
    elif parameters["clusterer"] == "spectral":
        return cluster.SpectralClustering(n_clusters=parameters["clusterer_n_clusters"])

    return cluster.KMeans(n_clusters=10, init="k-means++", n_init=1)

def getClassifier(parameters):
    if parameters["classifier"] == "kneighbors":
        return neighbors.KNeighborsClassifier(parameters["classifier_k"])
    elif parameters["classifier"] == "randomforest":
        return ensemble.RandomForestClassifier()
    elif parameters["classifier"] == "decisiontree":
        return tree.DecisionTreeClassifier()

    return neighbors.KNeighborsClassifier(1)

'''
Generates transactions from the samples, by assigning original datapoints to the closest
data point in the sample given the dimensions, producing n*k transactions. Where n is the
number of samples and k is the number of data points per sample
'''
def generateTransactions(dataKey, rows, samples, parameters, n=N, k=K, sampleDimCount=VALUE_VARIABLE, kk=KK, run=1, drawNewCenters=True, useCache=True):
    print ("Generating transactions [kk=%d]" %(kk))

    clustererString = "clusterer%s%s" %(parameters["clusterer"], "_%s" %(parameters["n_clusters"]) if "n_clusters" in parameters else "")
    classifierString = "classifier%s%s" %(parameters["classifier"], "_%s" %(parameters["classifier_k"]) if "classifier_k" in parameters else "")

    pickleFile = "%s/%s/run%d_transactions_n%d_k%d_kk_%d_sampleDimCount%s_%s_%s.pickle" %(OUTPUT_DIR, dataKey, run, n, k, kk, sampleDimCount, clustererString, classifierString)

    if useCache and os.path.isfile(pickleFile):
        print ("Pickle file exists, reading transactions from pickle")
        transactions = pickle.load(open(pickleFile))
    else:
        transactions = []

        for j, sample in enumerate(samples):
            data = [reduceDimensions(dataPoint, sample[1]) for dataPoint in sample[0]]

            clusterer = getClusterer(parameters)
            dLabels = clusterer.fit_predict(data)

            if drawNewCenters:
                drawLabeled(data, dLabels)

            labels = set(dLabels)

            if -1 in labels:
                labels.remove(-1)

            ts = [[[], sample[1]] for label in labels]

            classifier = getClassifier(parameters)
            classifier.fit(data, dLabels)

            reducedDataPoints = [reduceDimensions(row, sample[1]) for row in rows]

            rdpLabels = classifier.predict(reducedDataPoints)

            if drawNewCenters:
                drawLabeled(reducedDataPoints, rdpLabels)

            for i, label in enumerate(rdpLabels):
                ts[label][0].append(i)

            transactions.extend(ts)

        if useCache:
            pickle.dump(transactions, open(pickleFile, "w"), pickle.HIGHEST_PROTOCOL)

    w = open("tmp_transactions.txt", "w")

    for transaction in transactions:
        w.write(" ".join(["%d" %(item) for item in transaction[0]]))
        w.write("\n")

    w.close()

    return transactions

def reduceDimensions(point, dimensions):
    return np.array([point[dimension] for dimension in dimensions])

def scc(dataKey, parameters={}, run=1, distanceComputer=None, draw=False, drawNewSamples=False, drawNewCenters=False, useCache=True):
    cleanupFiles(dataKey)

    print ("DISTANCECOMPUTER CAN NOT BE USED!!")

    pars = {}

    pars["n"] = parameters["n"] if "n" in parameters else N
    pars["k"] = parameters["k"] if "k" in parameters else K
    pars["kk"] = parameters["kk"] if "kk" in parameters else KK
    pars["s"] = parameters["s"] if "s" in parameters else S
    pars["m"] = parameters["m"] if "m" in parameters else M
    pars["d"] = parameters["d"] if "d" in parameters else None
    pars["sampleDimCount"] = parameters["sampleDimCount"] if "sampleDimCount" in parameters else VALUE_VARIABLE

    pars["clusterer"] = parameters["clusterer"] if "clusterer" in parameters else "kmeans"
    pars["clusterer_n_clusters"] = parameters["clusterer_n_clusters"] if "clusterer_n_clusters" in parameters else 10
    pars["classifier"] = parameters["classifier"] if "classifier" in parameters else "kneighbors"
    pars["classifier_k"] = parameters["classifier_k"] if "classifier_k" in parameters else 1

    print (pars)

    return sc(dataKey, parameters=pars, run=run, draw=draw, drawNewSamples=drawNewSamples, drawNewCenters=drawNewCenters, useCache=useCache)

def drawParallelCoordinates(rows, fimm, dimCounts):
    dims = [(dim, dimCounts[dim]) for dim in dimCounts]
    dims = sorted(dims, key=lambda dim: dim[1], reverse=True)

    d = {i: [] for i in range(0, len(rows[0]))}
    d['cluster'] = []

    dc = {i: [] for i in range(0, len(rows[0]))}
    dc['cluster'] = []

    for i, row in enumerate(rows):
        for dim in range(0, len(rows[0])):
            if i in fimm:
                dc[dim].append(row[dim])
            else:
                d[dim].append(row[dim])

        if i in fimm:
            dc['cluster'].append('yes')
        else:
            d['cluster'].append('no')

    import pandas as pd
    from pandas.plotting import parallel_coordinates

    parallel_coordinates(pd.DataFrame(data=d), 'cluster', color=['b'])
    parallel_coordinates(pd.DataFrame(data=dc), 'cluster', color=['r'])

    plt.show()

def sc(dataKey, parameters, run=1, d=None, draw=True, drawNewSamples=False, drawNewCenters=False, useCache=True):
    rows = getTransactions(DATA[dataKey][KEY_RAW_FILE])

    print ("rC: %d, dC: %d, %s, run: %d" % (len(rows), len(rows[0]), ", ".join(["%s: %s" %(key, str(parameters[key])) for key in sorted(parameters.keys())]), run))

    samples = generateSamples(dataKey, rows, parameters, run=run, drawNewSamples=drawNewSamples, useCache=useCache)

    transactions = generateTransactions(dataKey, rows, samples, parameters, parameters["n"], parameters["k"], parameters["sampleDimCount"], parameters["kk"], run=run, drawNewCenters=drawNewCenters, useCache=useCache)

    items = transactions_to_items(transactions)

    fimms = get_fis(transactions, s=parameters["s"], n=parameters["m"])

    dims = []

    fimms2 = []

    for fimm in fimms:
        if len(fimm) < 30:
            continue
        print(fimm)

        intersect = reduce(set.intersection, map(set, [items[item] for item in fimm]))

        dimensions = [transactions[tid][1] for tid in intersect]

        print(dimensions)

        dims.append(dimensions)

        if d != None:
            fimms2.append(SubspaceCluster(fimms, infer_dimensions(d, fimm, dimensions, [rows[int(ix)] for ix in fimm])))

        if draw:
            dimCounts = Counter([ddim for dim in dimensions for ddim in dim])
            print(dimCounts)
            print([key+1 for key in sorted(dimCounts.keys()) if dimCounts[key] >= 200])
            print([key+1 for key in sorted(dimCounts.keys()) if dimCounts[key] >= 50])

            drawRows(rows, fimm, dimCounts)
            drawParallelCoordinates(rows, fimm, dimCounts)

    if d != None:
       return samples, transactions, fimms2, dims

    return samples, transactions, fimms, dims

'''
Main function
'''
def main():
    dataKey = "dbsizescale_s1500"

    params = {
        "n" : 10000,
        "k" : 100,
        "sampleDimCount" : 2,
        "kk" : 10,
        "s" : 1000,
        "m" : 100,
        "d" : None,
        "clusterer" : "optics",
        # "classifier" : "randomforest"
    }

    run = 1
    distanceComputer = None
    draw = True
    drawNewSamples = True
    drawNewCenters = True
    useCache = True

    cleanupFiles(dataKey)

    scc(dataKey, params, run, distanceComputer, draw, drawNewSamples, drawNewCenters, useCache)


if __name__ == '__main__':
    print ("RUNNING SC GENERAL")

    main()