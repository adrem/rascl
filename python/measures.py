
from clustering import SubspaceCluster

def object_recall(cR, cG):
    if isinstance(cR, SubspaceCluster):
        cR = cR.objects
    if isinstance(cG, SubspaceCluster):
        cG = cG.objects
    if len(cG) == 0:
        return 1

    return 1. * len(cR & cG) / len(cG)

def object_precision(cR, cG):
    return object_recall(cG, cR)

def object_f1(cR, cG):
    rec = object_recall(cR, cG)
    pre = object_precision(cR, cG)

    if rec + pre == 0:
        return 0

    return (2. * rec * pre) / (rec + pre)

def object_recall_res(cRs, cGs):
    if len(cGs) == 0:
        return 1

    if len(cRs) == 0:
        return 0

    return (1. / len(cGs)) * (sum([max([object_recall(cR, cG) for cR in cRs]) for cG in cGs]))

def object_precision_res(cRs, cGs):
    return object_recall_res(cGs, cRs)

def object_f1clus_res(p, q):
    if len(p) == 0:
        return 1

    if len(q) == 0:
        return 0

    return (1. / len(p)) * (sum([max([object_f1(pp, qq) for qq in q]) for pp in p]))

def object_f1R_res(cRs, cGs):
    return object_f1clus_res(cGs, cRs)

def object_f1P_res(cRs, cGs):
    return object_f1clus_res(cRs, cGs)

def jaccard_distance(cR, cG):
    if isinstance(cR, SubspaceCluster):
        cR = cR.objects
    if isinstance(cG, SubspaceCluster):
        cG = cG.objects

    uSize = len(cR | cG)

    if uSize == 0:
        return 0

    return 1. * len(cR & cG) / len(cR | cG)

def sc_recall(cR, cG):
    if not isinstance(cR, SubspaceCluster) or not isinstance(cG, SubspaceCluster):
        return -1

    if len(cG.objects) == 0 or len(cG.dimensions) == 0:
        return 1

    return (1. * len(cR.objects & cG.objects) * len(cR.dimensions & cG.dimensions)) / (len(cG.objects) * len(cG.dimensions))

def sc_precision(cR, cG):
    return sc_recall(cG, cR)

def sc_f1(cR, cG):
    rec = sc_recall(cR, cG)
    pre = sc_precision(cR, cG)

    if rec + pre == 0:
        return 0

    return (2. * rec * pre) / (rec + pre)

def sc_recall_res(cRs, cGs):
    if len(cGs) == 0:
        return 1

    if len(cRs) == 0:
        return 0

    return (1. / len(cGs)) * (sum([max([sc_recall(cR, cG) for cR in cRs]) for cG in cGs]))

def sc_precision_res(cRs, cGs):
    return sc_recall_res(cGs, cRs)

def sc_f1R_res(cRs, cGs):
    if len(cGs) == 0:
        return 1

    if len(cRs) == 0:
        return 0

    return (1. / len(cGs)) * (sum([max([sc_f1(cG, cR) for cR in cRs]) for cG in cGs]))

def sc_f1P_res(cRs, cGs):
    if len(cRs) == 0:
        return 1

    if len(cGs) == 0:
        return 0

    return (1. / len(cRs)) * (sum([max([sc_f1(cR, cG) for cG in cGs]) for cR in cRs]))

def dimension_recall(cR, cG):
    if isinstance(cR, SubspaceCluster):
        cR = cR.dimensions
    if isinstance(cG, SubspaceCluster):
        cG = cG.dimensions

    if len(cG) == 0:
        return 1

    return 1. * len(cR & cG) / len(cG)

def dimension_precision(cR, cG):
    return dimension_recall(cG, cR)

def dimension_f1(cR, cG):
    rec = dimension_recall(cR, cG)
    pre = dimension_precision(cR, cG)

    if rec + pre == 0:
        return 0

    return (2. * rec * pre) / (rec + pre)

def dimension_recall_res(cRs, cGs):
    if len(cGs) == 0:
        return 1

    if len(cRs) == 0:
        return 0

    return (1. / len(cGs)) * (sum([max([dimension_recall(cR, cG) for cR in cRs]) for cG in cGs]))

def dimension_precision_res(cRs, cGs):
    return dimension_recall_res(cGs, cRs)

def dimension_f1clus_res(p, q):
    if len(p) == 0:
        return 1

    if len(q) == 0:
        return 0

    return (1. / len(p)) * (sum([max([dimension_f1(pp, qq) for qq in q]) for pp in p]))

def dimension_f1R_res(cRs, cGs):
    return dimension_f1clus_res(cGs, cRs)

def dimension_f1P_res(cRs, cGs):
    return dimension_f1clus_res(cRs, cGs)

def object_size(cR, cG):
    if isinstance(cR, SubspaceCluster):
        cR = cR.objects

    return len(cR)

def object_size_res(cRs, cGs):
    return 1. / len(cRs) * (sum([object_size(t[0], t[1]) for t in zip(cRs, cGs)]))

def e4sc_res(cRs, cGs):
    rec = sc_f1R_res(cRs, cGs)
    pre = sc_f1P_res(cRs, cGs)

    if rec + pre == 0:
        return 0

    return (2. * rec * pre) / (rec + pre)

MEASURES = {
    "object_precision": object_precision,
    "object_recall": object_recall,
    "object_f1": object_f1,
    "dimension_precision": dimension_precision,
    "dimension_recall": dimension_recall,
    "dimension_f1": dimension_f1,
    "sc_precision": sc_precision,
    "sc_recall": sc_recall,
    "sc_f1": sc_f1,
    "object_size": object_size,
    "object_precision_res": object_precision_res,
    "object_recall_res": object_recall_res,
    "object_f1P_res": object_f1P_res,
    "object_f1R_res": object_f1R_res,
    "dimension_precision_res": dimension_precision_res,
    "dimension_recall_res": dimension_recall_res,
    "dimension_f1P_res": dimension_f1P_res,
    "dimension_f1R_res": dimension_f1R_res,
    "sc_precision_res": sc_precision_res,
    "sc_recall_res": sc_recall_res,
    "sc_f1P_res": sc_f1P_res,
    "sc_f1R_res": sc_f1R_res,
    "object_size_res": object_size_res,
    "e4sc_res": e4sc_res
}
