RASCL - Randomised Subspace Clusters
====================================

This project is an implementation of the RASCL algorithms which is introduced in
the paper:

"A Sampling-based Approach for Discovering Subspace Clusters", _by_ Sandy Moens,
Boris Cule and Bart Goethals, in review

Implementation
--------------

We implemented RASCL and RASCLR using python and use a Java based library for
randomly sampling maximal itemsets (https://gitlab.com/adrem/rmis).

RASCL implements the ideas from the paper, using *n* database samples
(containing *k* data points and 2 dimensions) and then using KMeans to discover
*kk* cluster centroids. Using the centroids, a transaction database is
constructed which is then sampled for maximal frequent itemsets. The latter
can be translated back to a subspace cluster.

RASCLR is similar to RASCL, except that it sets *k* equal to *kk*, essentially
skipping the clustering step. Therefore, in this implementation we drop *kk*
as a parameter and randomly sample *k* data points which are treated as the
centroids for creating the transaction database.

Usage
-----

Both RASCL and RASCLR can be used from the command line using `./rascl.py` or
`./rasclr.py` resp. Both algorithms accept comma-separated numeric data files.

Example usage:

`./rascl.py -f <path_to_iris_dataset> -n 1000 -k 100 -d "weighted|2" -K 3 -s
"rmis|30" -m 100 -r 10 -e "drop|0.05"`

`./rasclr.py -f <path_to_iris_dataset> -n 1000 -k 5 -d "weighted|2" -s
"rmis|30" -m 100 -r 10 -e "drop|0.05"`

All parameters and their options are presented when running the algorithms
without parameters or by running `./rascl.py -h`.

Experiments
-----------

All experiments that are used in the paper can be found in the folder "exp"
and json configuration files are found in "scripts".

Experiment runners expect the datasets to be in a folder "data" to be next
to the python folder. Experiments expect each dataset to have 2 data files:
- raw file: contains the numeric, comma-separated data
- clusters file: contains cluster assignments as defined by
`<space-separated object ids>|<space-separated dimension ids>`. Object ids
start at 0, while dimension ids start at 1.
Example clusters file:

```
0 1 2 3 4|1 2 3
3 4 5 6 7|3 4 5
```

We added scripts to more easily run the experiments and get the figures from the
paper. Each experiment has a corresponding configuration directory under
'scripts'. Within each directory resides a `run.py` that should be run from
the python directory to reconstruct the results from the paper. Experiment
output goes in to 'experiment_results' while the corresponding figures will be
placed under 'experiment_results_fig'.

<b>NOTE:</b> In order to run experiments on real world data, one has to also
create 'clusters' files for these files. This can be done using the original
class labels in case of classification datasets. For the experiments one can
use all dimensions. Keep in mind that by doing so, one can not use dimension
aware measures such as e4sc for evaluation.

Synthetic data
--------------

For reproducibility, we made the synthetic scaling datasets available:
http://macos.uantwerpen.be/~smoens/data/synthetic_sc_data.zip

<b>NOTE:</b> Note that we are not the owners or creators of the data, but
only make this available because the original source no longer exists.

<b>NOTE:</b> The datasets require processing of the data to be compatible
with the experiments.
